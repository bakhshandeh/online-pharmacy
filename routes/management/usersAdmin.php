<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Manegment usersAdmin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register usersAdmin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web,auth" middleware group. Now create something great!
|

 * $adminNamespace = 'App\Http\Controllers\Manegment\UsersAdmin\';
 *middleware(['web','auth'])
 * prefix('App\Providers\RouteServiceProvider\     $prefixRoutusersAdmin')
 *

*/
Route::name('Manegment.UsersAdmin.')->middleware('can:viewing the admin page is allowed')->group(function () {

    Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::prefix('notification')->name('notification.')->group(function () {
        Route::get('/send-mail', 'Notification\NotificationController@mail')->name('form.mail');
        Route::post('/send-mail', 'Notification\NotificationController@sendMail')->name('send.mail');
        Route::get('/send-sms', 'Notification\NotificationController@sms')->name('form.sms');
        Route::post('/send-sms', 'Notification\NotificationController@sendSms')->name('send.sms');
    });
    Route::prefix('users')->name('users.')->group(function () {
        Route::get('/', 'User\UserController@index')->name('index');
        Route::get('/{user}/edit', 'User\UserController@edit')->name('edit');
        Route::PUT('/{user}', 'User\UserController@update')->name('update');
//        Route::delete('/{user}','User\UserController@destroy')->name('destroy');

        Route::prefix('rolesPermissions')->name('roles.permissions.')->group(function (){
            Route::get('/{user}/edit', 'User\UserController@editRolesPermissionsUser')->name('edit');
            Route::PUT('/{user}', 'User\UserController@updateRolesPermissionsUser')->name('update');
        });
    });
    Route::prefix('roles')->name('roles.')->group(function () {
        Route::get('/', 'Role\RoleController@index')->name('index');
        Route::get('/create', 'Role\RoleController@create')->name('create');
        Route::post('/', 'Role\RoleController@store')->name('store');
        Route::get('/{role}/edit', 'Role\RoleController@edit')->name('edit');
        Route::PUT('/{role}', 'Role\RoleController@update')->name('update');
        Route::delete('/{role}','Role\RoleController@destroy')->name('destroy');

    });

    Route::prefix('permissions')->name('permissions.')->group(function () {
        Route::get('/', 'Permission\PermissionController@index')->name('index');
        Route::get('/create', 'Permission\PermissionController@create')->name('create');
        Route::post('/', 'Permission\PermissionController@store')->name('store');
    });

    Route::prefix('products')->name('products.')->group(function () {
        Route::get('/', 'Products\ProductController@index')->name('index');
        Route::get('/create', 'Products\ProductController@create')->name('create');
        Route::post('/', 'Products\ProductController@store')->name('store');
        Route::get('/{product}/edit', 'Products\ProductController@edit')->name('edit');
        Route::PUT('/{product}', 'Products\ProductController@update')->name('update');
        Route::delete('/{product}', 'Products\ProductController@destroy')->name('destroy');

        Route::prefix('add-inventory')->name('add-inventory.')->group(function () {
            Route::get('/create', 'Products\ProductController@addInventoryCreate')->name('create');
            Route::post('/', 'Products\ProductController@addInventoryStore')->name('store');
        });

        Route::prefix('files')->name('files.')->group(function () {
            Route::get('/', 'Products\FileController@index')->name('index');
            Route::get('/create', 'Products\FileController@create')->name('create');
            Route::post('/', 'Products\FileController@store')->name('store');
            Route::get('/{file}/edit', 'Products\FileController@edit')->name('edit');
            Route::get('/{file}/download', 'Products\FileController@show')->name('show');
            Route::PUT('/{file}', 'Products\FileController@update')->name('update');
            Route::delete('/{file}', 'Products\FileController@destroy')->name('destroy');

        });

        Route::prefix('properties')->name('properties.')->group(function () {
            Route::get('/', 'Products\PropertyController@index')->name('index');
            Route::get('/create', 'Products\PropertyController@create')->name('create');
            Route::post('/', 'Products\PropertyController@store')->name('store');
            Route::get('/{property}/edit', 'Products\PropertyController@edit')->name('edit');
            Route::PUT('/{property}', 'Products\PropertyController@update')->name('update');
            Route::delete('/{property}', 'Products\PropertyController@destroy')->name('destroy');

        });

        Route::prefix('types')->name('types.')->group(function () {
            Route::get('/', 'Products\TypeController@index')->name('index');
            Route::get('/create', 'Products\TypeController@create')->name('create');
            Route::post('/', 'Products\TypeController@store')->name('store');
            Route::get('/{type}/edit', 'Products\TypeController@edit')->name('edit');
            Route::PUT('/{type}', 'Products\TypeController@update')->name('update');
            Route::delete('/{type}', 'Products\TypeController@destroy')->name('destroy');

        });
        Route::prefix('categories')->name('categories.')->group(function () {
            Route::get('/', 'Products\CategoryController@index')->name('index');
            Route::get('/create', 'Products\CategoryController@create')->name('create');
            Route::post('/', 'Products\CategoryController@store')->name('store');
            Route::get('/{category}/edit', 'Products\CategoryController@edit')->name('edit');
            Route::PUT('/{category}', 'Products\CategoryController@update')->name('update');
            Route::delete('/{category}', 'Products\CategoryController@destroy')->name('destroy');

        });

        Route::prefix('manufacturers')->name('manufacturers.')->group(function () {
            Route::get('/', 'Products\Manufacturers\ManufacturerController@index')->name('index');
            Route::get('/create', 'Products\Manufacturers\ManufacturerController@create')->name('create');
            Route::post('/', 'Products\Manufacturers\ManufacturerController@store')->name('store');
            Route::get('/{manufacturer}/edit', 'Products\Manufacturers\ManufacturerController@edit')->name('edit');
            Route::PUT('/{manufacturer}', 'Products\Manufacturers\ManufacturerController@update')->name('update');
            Route::delete('/{manufacturer}', 'Products\Manufacturers\ManufacturerController@destroy')->name('destroy');

            Route::prefix('files')->name('files.')->group(function () {
                Route::get('/', 'Products\Manufacturers\FileController@index')->name('index');
                Route::get('/create', 'Products\Manufacturers\FileController@create')->name('create');
                Route::post('/', 'Products\Manufacturers\FileController@store')->name('store');
                Route::get('/{file}/edit', 'Products\Manufacturers\FileController@edit')->name('edit');
                Route::PUT('/{file}', 'Products\Manufacturers\FileController@update')->name('update');
                Route::delete('/{file}', 'Products\Manufacturers\FileController@destroy')->name('destroy');

            });
        });
        Route::prefix('sellers')->name('sellers.')->group(function () {
            Route::get('/', 'Products\Sellers\SellerController@index')->name('index');
            Route::get('/create', 'Products\Sellers\SellerController@create')->name('create');
            Route::post('/', 'Products\Sellers\SellerController@store')->name('store');
            Route::get('/{seller}/edit', 'Products\Sellers\SellerController@edit')->name('edit');
            Route::PUT('/{seller}', 'Products\Sellers\SellerController@update')->name('update');
            Route::delete('/{seller}', 'Products\Sellers\SellerController@destroy')->name('destroy');

            Route::prefix('files')->name('files.')->group(function () {
                Route::get('/', 'Products\Sellers\FileController@index')->name('index');
                Route::get('/create', 'Products\Sellers\FileController@create')->name('create');
                Route::post('/', 'Products\Sellers\FileController@store')->name('store');
                Route::get('/{file}/edit', 'Products\Sellers\FileController@edit')->name('edit');
                Route::PUT('/{file}', 'Products\Sellers\FileController@update')->name('update');
                Route::delete('/{file}', 'Products\Sellers\FileController@destroy')->name('destroy');

            });
        });
    });

    Route::prefix('prescriptions')->name('prescriptions.')->group(function () {
        Route::get('/setPrice/{prescription}', 'Prescriptions\PrescriptionController@setPrice')->name('set.price');
        Route::post('/setPrice/{prescription}', 'Prescriptions\PrescriptionController@storePrice')->name('store.price');
        Route::post('/{prescription}/addProduct/{product}', 'Prescriptions\PrescriptionController@addProduct')->name('add.product');

        Route::get('/', 'Prescriptions\PrescriptionController@index')->name('index');
        Route::get('/create', 'Prescriptions\PrescriptionController@create')->name('create');
        Route::post('/', 'Prescriptions\PrescriptionController@store')->name('store');
        Route::get('/{prescription}/edit', 'Prescriptions\PrescriptionController@edit')->name('edit');
        Route::PUT('/{prescription}', 'Prescriptions\PrescriptionController@update')->name('update');
        Route::delete('/{prescription}', 'Prescriptions\PrescriptionController@destroy')->name('destroy');

    });

    Route::prefix('accounting')->name('accounting.')->group(function () {
        Route::prefix('orders')->name('orders.')->group(function () {
            Route::get('/', 'Accounting\OrderController@index')->name('index');
            Route::get('/create', 'Accounting\OrderController@create')->name('create');
            Route::post('/', 'Accounting\OrderController@store')->name('store');
            Route::get('/{order}/edit', 'Accounting\OrderController@edit')->name('edit');
            Route::PUT('/{order}', 'Accounting\OrderController@update')->name('update');
            Route::delete('/{order}', 'Accounting\OrderController@destroy')->name('destroy');

        });
        Route::prefix('payments')->name('payments.')->group(function () {
            Route::get('/', 'Accounting\PaymentController@index')->name('index');
            Route::get('/create', 'Accounting\PaymentController@create')->name('create');
            Route::post('/', 'Accounting\PaymentController@store')->name('store');
            Route::get('/{payment}/edit', 'Accounting\PaymentController@edit')->name('edit');
            Route::PUT('/{payment}', 'Accounting\PaymentController@update')->name('update');
            Route::delete('/{payment}', 'Accounting\PaymentController@destroy')->name('destroy');

        });

    });

});

