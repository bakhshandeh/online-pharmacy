<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Manegment users Routes
|--------------------------------------------------------------------------
|
| Here is where you can register usersAdmin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web,auth" middleware group. Now create something great!
|

 * $adminNamespace = 'App\Http\Controllers\Manegment\Users\';
 *middleware(['web','auth'])
 *

*/

Route::name('manegment.users.')->group(function (){
    Route::get('/dashboard','Dashboard\DashboardController@index')->name('dashboard');

    Route::prefix('users')->name('users.')->group(function (){


        Route::prefix('address')->name('address.')->group(function (){
//        Route::get('/', 'Users\Address\AddressController@index')->name('index');
            Route::get('/create', 'Users\Address\AddressController@create')->name('create');
            Route::post('/', 'Users\Address\AddressController@store')->name('store');
//        Route::get('/{address}/edit', 'Users\Address\AddressController@edit')->name('edit');
//        Route::PUT('/{address}', 'Users\Address\AddressController@update')->name('update');
//        Route::delete('/{address}', 'Users\Address\AddressController@destroy')->name('destroy');
        });
    });

    Route::prefix('prescriptions')->name('prescriptions.')->group(function (){
        Route::get('/listSetPrice', 'Prescriptions\PrescriptionController@listSetPrice')->name('list.set.price');
        Route::get('/cancel/{prescription}', 'Prescriptions\PrescriptionController@cancel')->name('cancel');

//        Route::get('/', 'Prescriptions\PrescriptionController@index')->name('index');
        Route::get('/create', 'Prescriptions\PrescriptionController@create')->name('create');
        Route::post('/', 'Prescriptions\PrescriptionController@store')->name('store');
//        Route::get('/{address}/edit', 'Prescriptions\PrescriptionController@edit')->name('edit');
//        Route::PUT('/{address}', 'Prescriptions\PrescriptionController@update')->name('update');
//        Route::delete('/{address}', 'Prescriptions\PrescriptionController@destroy')->name('destroy');
    });


});


