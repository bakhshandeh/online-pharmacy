<?php

use App\Repositories\Models\Role;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect()->route('products.index',1);

});
//Route::get('user/dashboard/test','Manegment\Users\Dashboard\DashboardController@index')->name('dashboard');


Auth::routes();

Route::get('/home', function (){
    return redirect()->route('products.index',3);
})->name('home');
//Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('products')->name('products.')->group(function (){
    Route::get('/category/{category}','Web\Products\ProductsController@index')->name('index');
});

Route::prefix('basket')->name('basket.')->group(function (){
    Route::get('/add/product/{product}/{prescription}/{seller}','Web\Basket\BasketController@add')->name('add');
    Route::get('/','Web\Basket\BasketController@index')->name('index');
    Route::post('/update/{product}/{seller}','Web\Basket\BasketController@update')->name('update');
    Route::get('/checkout','Web\Basket\BasketController@checkOutForm')->name('checkout.form');
    Route::post('/checkout','Web\Basket\BasketController@checkOut')->name('checkout');
});

Route::prefix('accounting')->name('accounting.')->group(function (){
    Route::prefix('payment')->name('payment.')->group(function (){
        Route::post('{gateway}/callback','Web\Accounting\PaymentController@verify')->name('verify');
    });
});

//Route::get('/getCity','Manegment\CityQuarterController@getCity')->name('get.city');

Route::get('basket/clear', function (){
    resolve(\App\Support\Storage\Contracts\StorageInterface::class)->clear();
});

