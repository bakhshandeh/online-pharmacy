@if($prescription->files()->where('type', 'image')->first())
    <a href="">
        <img src="{{ asset(''.Storage::url($prescription->files()
                                                                    ->where('type', 'image')->first()->type.
                                                                      DIRECTORY_SEPARATOR .$prescription->files()
                                                                      ->where('type', 'image')->first()->name).'')}}"
             alt="">
    </a>
@endif

<div class="col-md-12">
    <fieldset class="form-group">
        <div class="table-responsive">
            {{--                    <table class="table table-striped dataex-html5-selectors">--}}
            {{--                    <table class="table table-striped table-bordered complex-headers">--}}
            <table class="table nowrap scroll-horizontal-vertical">
                <thead>
                    <tr>
                        <th>
                            نام داارو
                        </th>
                        <th>
                            نام فارسی داارو
                        </th>
                        <th>
                            نام عمومی داارو
                        </th>
                        <th>
                            دوز/حجم دارو
                        </th>
                        <th>
                            تولید کننده
                        </th>
                        <th>
                            موجودی
                        </th>
                        <th>
                            قیمت
                        </th>
                        <th>
                            عملیات
                        </th>
                    </tr>
                </thead>
                <tbody>
                @forelse($productsCategorId1 as $product)
                    @if(!is_null($product->sellers()->where('seller_id', auth()->user()->seller_id)->first()))
                        <tr>
                            <td>{{$product->name ?? ''}}</td>
                            <td>{{$product->persian_name ?? ''}}</td>
                            <td>{{$product->generic_name ?? ''}}</td>
                            <td>{{$product->amount ?? ''}}</td>
                            <td>{{$product->manufacturer ?? ''}}</td>
                            <td>{{$product->sum_number ?? ''}}</td>
                            <td>{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price ?? ''}}</td>
                            <td>
                                <form name="form1"
                                      action="{{route('Manegment.UsersAdmin.prescriptions.add.product', ['prescription' => $prescription->id, 'product' => $product->id])}}"
                                      method="post" class="form-inline input-number">
                                    @csrf
                                    <input type="hidden" name="seller_id" value="{{auth()->user()->seller_id}}">
                                    <input type="hidden" name="price"
                                           value="{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price}}">
                                    <select name="quantity" id="quantity" class="form-control input-sm mr-sm-2">
                                        @for($i = 0; $i <= $product->sum_number ; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-sm"> افزودن</button>
                                </form>
                            </td>
                        </tr>
                    @endif
                @empty
                @endforelse
                @forelse($productsCategorId2 as $product)
                    @if(!is_null($product->sellers()->where('seller_id', auth()->user()->seller_id)->first()))
                        <tr>
                            <td>{{$product->name ?? ''}}</td>
                            <td>{{$product->persian_name ?? ''}}</td>
                            <td>{{$product->generic_name ?? ''}}</td>
                            <td>{{$product->amount ?? ''}}</td>
                            <td>{{$product->manufacturer ?? ''}}</td>
                            <td>{{$product->sum_number ?? ''}}</td>
                            <td>{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price ?? ''}}</td>
                            <td>
                                <form name="form1"
                                      action="{{route('Manegment.UsersAdmin.prescriptions.add.product', ['prescription' => $prescription->id, 'product' => $product->id])}}"
                                      method="post" class="form-inline input-number">
                                    @csrf
                                    <input type="hidden" name="seller_id" value="{{auth()->user()->seller_id}}">
                                    <input type="hidden" name="price"
                                           value="{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price}}">
                                    <select name="quantity" id="quantity" class="form-control input-sm mr-sm-2">
                                        @for($i = 0; $i <= $product->sum_number ; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-sm"> افزودن</button>
                                </form>
                            </td>
                        </tr>
                    @endif
                @empty
                @endforelse
                </tbody>
                <tfoot>
                <tr>
                    <th>
                        نام داارو
                    </th>
                    <th>
                        نام فارسی داارو
                    </th>
                    <th>
                        نام عمومی داارو
                    </th>
                    <th>
                        دوز/حجم دارو
                    </th>
                    <th>
                        تولید کننده
                    </th>
                    <th>
                        موجودی
                    </th>
                    <th>
                        قیمت
                    </th>
                    <th>
                        عملیات
                    </th>

                </tr>
                </tfoot>
            </table>
        </div>

    </fieldset>
</div>
