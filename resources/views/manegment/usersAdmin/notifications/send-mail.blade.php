@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Vendor Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/manegment/dashbord/users/vendors/css/forms/select/select2.min.css')}}">
    <!-- END: Vendor CSS-->
@endpush

@section('title', 'اطلاع رسانی')
@section('title_page', 'اطلاع رسانی با ارسال ایمیل')

@section('content')
    <!-- Basic Select2 start -->
    <section class="basic-select2">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ارسال ایمیل</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                        <form method="post" action="{{route('Manegment.UsersAdmin.notification.send.mail')}}">
                                            @csrf
                                            <fieldset class="form-group col-md-12">
                                                <label class="control-label" for="user"><h5> انتخاب کاربر:</h5></label>
                                                <div class="form-group">
                                                <select name="user" id="user" data-placeholder="انتخاب کاربر..." class="select2-theme form-control @error('user') is-invalid @enderror" >
                                                    <optgroup label="تمامی کاربران">
                                                        @forelse($users as $user)
                                                            <option
                                                                name="{{$user->first_name ?? ''}}{{"-"}}{{$user->last_name ?? ''}}"
                                                                value="{{$user->id ?? ''}}"
                                                                {{old('user') == $user->id ? 'selected' : ''}}

                                                            >
                                                                {{$user->first_name ?? ''}}{{" "}}{{$user->last_name ?? ''}}

                                                            </option>
                                                        @empty
                                                        @endforelse
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div>
                                                    @error('user')
                                                    <span role="alert" >
                                                        <strong class="text-danger">{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>

                                            </fieldset>
                                            <fieldset class="form-group col-md-12">
                                                <label class="control-label" for="mail_type"><h5> انتخاب نوع ایمیل:</h5></label>
                                                <div class="form-group">
                                                <select name="mail_type" id="mail_type" data-placeholder="انتخاب نوع ایمیل..." class="select2-theme form-control @error('mail_type') is-invalid @enderror" >
                                                    <optgroup label="تمامی کاربران">
                                                        @forelse($mailTypes as $key => $mailType)
                                                            <option name="{{$key ?? ''}}"
                                                                    value="{{$key ?? ''}}"
                                                            >
                                                                {{$mailType ?? ''}}
                                                            </option>
                                                        @empty
                                                        @endforelse
                                                    </optgroup>
                                                </select>
                                                </div>
                                                <div>
                                                    @error('mail_type')
                                                    <span role="alert" >
                                                        <strong class="text-danger">{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </fieldset>

                                            <div class="col-md-12 col-sm-12 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">ارسال</button>
                                            </div>
                                        </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Select2 end -->


@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/select/form-select2.js')}}"></script>
    <!-- END: Page JS-->

@endpush

@push('before-body-end')

@endpush
