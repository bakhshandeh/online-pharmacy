@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'کاربران')
@section('title_page', 'لیست کاربران')

@section('content')

    <!-- Column selectors with Export Options and print table -->
    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">لیست تمامی کاربران به همراه نقش آنها</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                {{--                                <table class="table table-striped dataex-html5-selectors">--}}
                                <table class="table table-striped table-bordered complex-headers">
                                    <thead>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <th>
                                            نام خانوادگی
                                        </th>
                                        <th>
                                            تلفن همراه
                                        </th>
                                        <th>
                                            ایمیل
                                        </th>
                                        <th>
                                            نقش ها
                                        </th>
                                        <th>
                                            عملیات
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($users as $user)
                                        <tr>
                                            <td>{{$user->first_name}}</td>
                                            <td>{{$user->last_name}}</td>
                                            <td>{{$user->phone_number}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>*
                                                @forelse($user->roles as $role)
                                                    {{$role->persian_name}}*
                                                @empty
                                                    *
                                                @endforelse
                                            </td>
                                            <td>

                                                <a href="{{route('Manegment.UsersAdmin.users.roles.permissions.edit', ['user' => $user->id])}}"><i class="bx bx-edit-alt"></i> ویرایش دسترسی</a>

{{--                                                <form class="inline" style="display: inline !important;" method="post" action="{{route('Manegment.UsersAdmin.users.destroy', ['user' => $user->id])}}" >--}}
{{--                                                    @csrf--}}
{{--                                                    @method('DELETE')--}}
{{--                                                    <button type="submit" style="background: none;color: inherit;border: none;padding: 0;font: inherit;cursor: pointer;outline: inherit; display: inline;"><i class="bx bx-trash-alt" style="color: red"></i></button>--}}
{{--                                                </form>--}}

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>موردی یافت نشد.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <th>
                                            نام خانوادگی
                                        </th>
                                        <th>
                                            تلفن همراه
                                        </th>
                                        <th>
                                            ایمیل
                                        </th>
                                        <th>
                                            نقش ها
                                        </th>
                                        <th>
                                            عملیات
                                        </th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->

@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    {{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>--}}
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/datatables/datatable.js')}}"></script>
    <!-- END: Page JS-->
@endpush

@push('before-body-end')
    {{--    <script>--}}
    {{--        $.ajaxSetup({--}}
    {{--            headers: {--}}
    {{--                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
    {{--            }--}}
    {{--        });--}}
    {{--        $(document).on('click', 'a.jquery-postback', function(e) {--}}
    {{--            e.preventDefault(); // does not go through with the link.--}}

    {{--            var $this = $(this);--}}

    {{--            $.post({--}}
    {{--                type: $this.data('method'),--}}
    {{--                url: $this.attr('href')--}}
    {{--            }).done(function (data) {--}}
    {{--                alert(data);--}}
    {{--                console.log(data);--}}
    {{--            });--}}
    {{--        });--}}
    {{--    </script>--}}

@endpush
