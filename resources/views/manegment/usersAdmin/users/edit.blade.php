@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'کاربران')
@section('title_page', 'لیست کاربران')

@section('content')
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ویرایش سطح دسترسی کاربر</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('Manegment.UsersAdmin.users.roles.permissions.update', ['user' => $user->id])}}" >
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="col-6 col-md-6">
                                        <div class="form-group">
                                            <h2>
                                                <span>نقش های داده شده به کاربر:</span><span>{{$user->name}}</span>
                                            </h2>
                                        </div>
                                        <div class="form-group">
                                            @forelse($roles as $role)
                                                <div class="checkbox checkbox-primary checkbox-glow">
                                                    <input id="{{'role'.$role->id}}" type="checkbox" name="roles[]"
                                                           value="{{$role->name}}"
                                                    @isset($user){{ $user->roles->contains($role)? 'checked': ''  }}>@endisset
                                                    <label for="{{'role'.$role->id}}">
                                                        {{$role->persian_name}}
                                                    </label>
                                                </div>
                                            @empty
                                            @endforelse
                                        </div>
                                    </div>

                                    <div class="col-6 col-md-6 ">
                                        <div class="form-group">
                                            <h2>
                                                <span>دسترسی های داده شده به کاربر:</span><span>{{$user->name}}</span>
                                            </h2>
                                        </div>
                                        <div class="form-group inline">
                                            @forelse($permissions as $permission)
                                                <div class="checkbox checkbox-primary checkbox-glow">
                                                    <input id="{{'permission'.$permission->id}}" type="checkbox"
                                                           name="permissions[]" value="{{$permission->name}}"
                                                        {{--                                            @isset($user){{ $user->permissions->contains('id', $permission->id)? 'checked': ''  }}@endisset>--}}
                                                    @isset($user){{ $user->permissions->contains($permission)? 'checked': ''  }}@endisset>
                                                    <label for="{{'permission'.$permission->id}}">
                                                        {{$permission->persian_name}}
                                                    </label>
                                                </div>
                                            @empty
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 d-flex justify-content-end">
                            <button type="submit" class="btn btn-primary mr-1 mb-1">بروزرسانی</button>
                            <a href="{{route('Manegment.UsersAdmin.users.index')}}"
                               class="btn btn-danger mr-1 mb-1">انصراف</a>
                        </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
@push('scripts')
    <!-- BEGIN: Page JS-->

    <!-- END: Page JS-->
@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->

@endpush
