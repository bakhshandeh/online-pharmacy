<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="name">نام محصول به انگلیسی</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror"
                       name="name" id="name" value="{{ $product->name ?? old('name') }}"
                       placeholder="نام محصول به انگلیسی را وارد کنید"
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="persian_name">نام محصول به فارسی</label>
                <input type="text" class="form-control @error('persian_name') is-invalid @enderror"
                       name="persian_name" id="persian_name"
                       value="{{ $product->persian_name ?? old('persian_name') }}"
                       placeholder="نام محصول به فارسی را وارد کنید"
                @error('persian_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="generic_name">نام عمومی محصول </label>
                <input type="text" class="form-control @error('generic_name') is-invalid @enderror"
                       name="generic_name" id="generic_name"
                       value="{{ $product->generic_name ?? old('generic_name') }}"
                       placeholder="نام عمومی محصول را وارد کنید"
                @error('generic_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="amount">مقدار حل شده/وزن</label>
                <input type="text" class="form-control @error('amount') is-invalid @enderror"
                       name="amount" id="amount"
                       value="{{ $product->amount ?? old('amount') }}"
                       placeholder="مقدار/وزن محصول را وارد کنید"
                @error('amount')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="number">تعداد</label>--}}
{{--                <input type="text" class="form-control @error('number') is-invalid @enderror"--}}
{{--                       name="number" id="number"--}}
{{--                       value="{{ $product->number ?? old('number') }}"--}}
{{--                       placeholder="تعداد محصول را وارد کنید"--}}
{{--                @error('number')--}}
{{--                <span class="invalid-feedback" role="alert">--}}
{{--                    <strong class="text-danger">{{ $message }}</strong>--}}
{{--                </span>--}}
{{--                @enderror--}}
{{--            </fieldset>--}}
{{--        </div>--}}

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="manufacturers_id">شرکت تولید کننده</label>
                <div class="form-group">
                    <select name="manufacturers_id[]" multiple id="manufacturers_id" data-placeholder="تولید کننده"
                            class="select2-theme form-control @error('manufacturers_id') is-invalid @enderror">
                        <optgroup label="شرکت های تولید کننده">
                            @forelse($manufacturers as $manufacturer)
                                <option
                                    name="{{$manufacturer->name ?? ''}}"
                                    value="{{$manufacturer->id ?? ''}}"
                                    {{old('manufacturer') == $manufacturer->id ? 'selected' : ''}}
                                >
                                    {{$manufacturer->persian_name ?? ''}}{{'('}}{{$manufacturer->name ?? ''}}{{')'}}

                                </option>
                            @empty
                            @endforelse
                        </optgroup>
                    </select>
                </div>
                <div>
                    @error('manufacturers_id')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <br>
                <div class="checkbox checkbox-primary checkbox-glow">
                    <input id="sell_with_prescription" type="checkbox" name="sell_with_prescription"
                           class="@error('sell_with_prescription') is-invalid @enderror"
                           value="1" {{old('sell_with_prescription') ? 'checked' : ''}}
                    >
                    <label for="sell_with_prescription">
                        فروش فقط بانسخه
                    </label>
                </div>
            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="category_id">دسته بندی محصول</label>
                <div class="form-group">
                    <select name="categories_id[]" multiple id="categories_id" data-placeholder="دسته بندی"
                            class="select2-theme form-control @error('categories_id') is-invalid @enderror">
                        <optgroup label="دسته بندی محصول">
                            @forelse($categories as $category)
                                <option
                                    name="{{$category->name ?? ''}}"
                                    value="{{$category->id ?? ''}}"
                                    {{old('categories_id') == $category->id ? 'selected' : ''}}
                                >
                                    {{$category->name ?? ''}}

                                </option>
                            @empty
                            @endforelse
                        </optgroup>
                    </select>
                </div>
                <div>
                    @error('categories_id')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="types_id">نوع محصول</label>
                <div class="form-group">
                    <select name="types_id[]" multiple id="types_id" data-placeholder="نوع"
                            class="select2-theme form-control @error('types_id') is-invalid @enderror">
                        <optgroup label="  نوع محصول ">
                            @forelse($types as $type)
                                <option
                                    name="{{$type->name ?? ''}}"
                                    value="{{$type->id ?? ''}}"
                                    {{old('types') == $type->id ? 'selected' : ''}}
                                >
                                    {{$type->name ?? ''}}

                                </option>
                            @empty
                            @endforelse
                        </optgroup>
                    </select>
                </div>
                <div>
                    @error('types_id')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </fieldset>

        </div>

{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="sellers_id">داروخانه فروشنده</label>--}}
{{--                <div class="form-group">--}}
{{--                    <select name="sellers_id[]" multiple id="sellers_id" data-placeholder="فروشنده"--}}
{{--                            class="select2-theme form-control @error('sellers_id') is-invalid @enderror">--}}
{{--                        <optgroup label="داروخانه های فروشنده">--}}
{{--                            @forelse($sellers as $seller)--}}
{{--                                <option--}}
{{--                                    name="{{$seller->name ?? ''}}"--}}
{{--                                    value="{{$seller->id ?? ''}}"--}}
{{--                                    {{old('seller') == $seller->id ? 'selected' : ''}}--}}
{{--                                >--}}
{{--                                    {{$seller->persian_name ?? ''}}{{'('}}{{$seller->name ?? ''}}{{')'}}--}}

{{--                                </option>--}}
{{--                            @empty--}}
{{--                            @endforelse--}}
{{--                        </optgroup>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    @error('sellers_id')--}}
{{--                    <span role="alert">--}}
{{--                        <strong class="text-danger">{{ $message }}</strong>--}}
{{--                    </span>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--            </fieldset>--}}

{{--        </div>--}}


        <div class="col-md-12" name="form-repeater">
            <hr>
            <div id="add_form_properties" class="col-md-12 form-group">
                <div id="form_propertie" class="form-group col-md-12 ">
                    <div class="row justify-content-between col-md-12">
                        <div class="col-md-6 col-sm-12 form-group">
                            <fieldset class="form-group">
                                <label for="property_id"> ویژگی محصول</label>
                                <div class="form-group">
                                    <select name="properties_id[]"
                                            data-placeholder="ویژگی محصول..."
                                            class="select2-theme form-control @error('properties_id') is-invalid @enderror">
                                        <optgroup label="ویژگی های محصول">
                                            @forelse($properties as $property)
                                                <option
                                                    name="{{$property->name ?? ''}}"
                                                    value="{{$property->id ?? ''}}"
                                                    {{old('property') == $property->id ? 'selected' : ''}}
                                                >
                                                    {{$property->name ?? ''}}

                                                </option>
                                            @empty
                                            @endforelse
                                        </optgroup>
                                    </select>
                                </div>
                                <div>
                                    @error('properties_id')
                                    <span role="alert">
                                            <strong class="text-danger">{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-6 col-sm-12 form-group">
                            <fieldset class="form-group">
                                <label for="property_values">مقدار ویژگی محصول </label>
                                <input type="text" class="form-control @error('property_values') is-invalid @enderror"
                                       name="property_values[]"
                                       placeholder="مقدار ویژگی محصول را وارد کنید">
                                @error('property_values')
                                <span class="invalid-feedback" role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col p-0">
                    <button id="add_properties" class="btn btn-primary" type="button"><i class="bx bx-plus"></i>
                        افزودن
                    </button>
                </div>
            </div>
            <hr>
        </div>


        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="description"> توضیحات </label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        <textarea class="form-control char-textarea" name="description" id="description" rows="3"
                                  placeholder="توضیحات "></textarea>
                    </fieldset>
                    {{--                    <small class="counter-value float-right"><span class="char-count">0</span> / 200 </small>--}}
                </div>
                <div>
                    @error('description')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
            <hr>
        </div>

        <div class="col-md-6">
            <div class="card-header">
                <h4 class="card-title">افزودن فایل های محصول</h4>
            </div>
            <fieldset class="form-group col-md-12">
                <input type="file" name="file[]" multiple id="customFile" class="custom-file-input" accept="image/jpeg, image/jpg" >
                <label class="custom-file-label" for="customFile">فایل خود را انتخاب کنید</label>
                @error('file')
                <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                @enderror
            </fieldset>
{{--            <fieldset class="form-group">--}}
{{--                <div class="checkbox checkbox-primary checkbox-glow ">--}}
{{--                    <input id="is_private" type="checkbox" name="is_private"--}}
{{--                           class="@error('is_private') is-invalid @enderror">--}}
{{--                    <label for="is_private">--}}
{{--                        به صورت خصوصی آپلود شود--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--            </fieldset>--}}

        </div>


    </div>
</div>

