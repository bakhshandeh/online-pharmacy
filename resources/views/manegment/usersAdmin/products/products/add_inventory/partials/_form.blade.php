<div class="card-body">
    <div class="row">
            <div class="col-md-12">
                <fieldset class="form-group">
                    <label for="products_id">انتخاب محصول</label>
                    <div class="form-group">
                        <select name="products_id"  id="products_id" data-placeholder=""
                                class="select2-theme form-control @error('products_id') is-invalid @enderror">
                            <optgroup label="محصولات">
                                @forelse($products as $product)
                                    <option
                                        name="{{$product->name ?? ''}}"
                                        value="{{$product->id ?? ''}}"
                                        {{old('seller') == $product->id ? 'selected' : ''}}
                                    >
                                        {{' کد محصول: '}} {{ $product->id ?? '' }}{{' --  نام: '}}{{$product->name ?? ''}}{{' --  نام فارسی: '}}{{$product->persian_name ?? ''}}{{' --  نام عمومی: '}}{{$product->generic_name ?? ''}}{{' --  تولید کنندگان: '}}{{$product->manufacturer ?? ''}}

                                    </option>
                                @empty
                                @endforelse
                            </optgroup>
                        </select>
                    </div>
                    <div>
                        @error('products_id')
                        <span role="alert">
                                        <strong class="text-danger">{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                </fieldset>

            </div>

            <div class="col-md-3">
                <fieldset class="form-group">
                    <label for="number">تعداد</label>
                    <input type="number" class="form-control @error('number') is-invalid @enderror"
                           name="number" id="number"
                           value="{{ old('number') }}"
                           placeholder="تعداد محصول را وارد کنید"
                    @error('number')
                    <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                    @enderror
                </fieldset>
            </div>

            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="seller_price">قیمت فروش:</label>
                    <input type="number" class="form-control @error('seller_price') is-invalid @enderror"
                           name="seller_price" id="seller_price" value="{{ old('seller_price') }}"
                           placeholder="قیمت فروش را وارد کنید"
                    @error('seller_price')
                    <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                    @enderror
                </fieldset>
            </div>

            <div class="col-md-4">
                <fieldset class="form-group">
                    <label for="seller_purchase_price">قیمت خرید فروشنده:</label>
                    <input type="number" class="form-control @error('seller_purchase_price') is-invalid @enderror"
                           name="seller_purchase_price" id="seller_purchase_price"
                           value="{{ old('seller_purchase_price') }}"
                           placeholder="قیمت خرید فروشنده را وارد کنید"
                    @error('seller_purchase_price')
                    <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $message }}</strong>
                        </span>
                    @enderror
                </fieldset>
            </div>


        {{--        <div class="col-md-6">--}}
        {{--            <fieldset class="form-group">--}}
        {{--                <label for="sellers_id">داروخانه فروشنده</label>--}}
        {{--                <div class="form-group">--}}
        {{--                    <select name="sellers_id[]" multiple id="sellers_id" data-placeholder="فروشنده"--}}
        {{--                            class="select2-theme form-control @error('sellers_id') is-invalid @enderror">--}}
        {{--                        <optgroup label="داروخانه های فروشنده">--}}
        {{--                            @forelse($sellers as $seller)--}}
        {{--                                <option--}}
        {{--                                    name="{{$seller->name ?? ''}}"--}}
        {{--                                    value="{{$seller->id ?? ''}}"--}}
        {{--                                    {{old('seller') == $seller->id ? 'selected' : ''}}--}}
        {{--                                >--}}
        {{--                                    {{$seller->persian_name ?? ''}}{{'('}}{{$seller->name ?? ''}}{{')'}}--}}

        {{--                                </option>--}}
        {{--                            @empty--}}
        {{--                            @endforelse--}}
        {{--                        </optgroup>--}}
        {{--                    </select>--}}
        {{--                </div>--}}
        {{--                <div>--}}
        {{--                    @error('sellers_id')--}}
        {{--                    <span role="alert">--}}
        {{--                        <strong class="text-danger">{{ $message }}</strong>--}}
        {{--                    </span>--}}
        {{--                    @enderror--}}
        {{--                </div>--}}

        {{--            </fieldset>--}}

        {{--        </div>--}}


    </div>
</div>

