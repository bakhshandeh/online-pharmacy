@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'فایل های محصولات')
@section('title_page', 'لیست فایل های محصولات')

@section('content')
    <!-- Column selectors with Export Options and print table -->
    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">لیست تمامی فایل های محصولات</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                {{--                                <table class="table table-striped dataex-html5-selectors">--}}
                                <table class="table table-striped table-bordered complex-headers">
                                    <thead>
                                    <tr>
                                        <th>
                                            نام فایل
                                        </th>
                                        <th>
                                            نوع فایل
                                        </th>
                                        <th>
                                            حجم فایل{{' (MB) '}}
                                        </th>
                                        <th>
                                            زمان فایل
                                        </th>
                                        <th>
                                            سطح دسترسی
                                        </th>
                                        <th data-hide="phone,tablet">
                                            عملیات
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($files as $file)
                                        <tr>
                                            <td>{{$file->name}}</td>
                                            <td>{{$file->type}}</td>
                                            <td>{{number_format($file->size / (1024 * 1024) ,2)}}</td>
                                            @if(is_null($file->time))
                                                <td>ندارد</td>
                                            @else
                                                <td>{{$file->time}}</td>
                                            @endif

                                            @if($file->is_private)
                                                <td>خصوصی</td>
                                            @else
                                                <td>عمومی</td>
                                            @endif

                                            <td>

                                                <a href="{{route('Manegment.UsersAdmin.products.files.show', ['file' => $file->id])}}"><i class="bx bx-download"></i></a>
                                                <form class="inline" style="display: inline !important;" method="post" action="{{route('Manegment.UsersAdmin.products.files.destroy', ['file' => $file->id])}}" >
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" style="background: none;color: inherit;border: none;padding: 0;font: inherit;cursor: pointer;outline: inherit; display: inline;"><i class="bx bx-trash-alt" style="color: red"></i></button>
                                                </form>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>موردی یافت نشد.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>
                                            نام فایل
                                        </th>
                                        <th>
                                            نوع فایل
                                        </th>
                                        <th>
                                            حجم فایل{{' (MB) '}}
                                        </th>
                                        <th>
                                            زمان فایل
                                        </th>
                                        <th>
                                            سطح دسترسی
                                        </th>
                                        <th data-hide="phone,tablet">
                                            عملیات
                                        </th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->


@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    {{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>--}}
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/datatables/datatable.js')}}"></script>
    <!-- END: Page JS-->
@endpush

@push('before-body-end')


@endpush
