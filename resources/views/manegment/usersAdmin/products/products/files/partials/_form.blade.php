<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <fieldset class="form-group">
                <input type="file" name="file[]" multiple id="customFile" class="custom-file-input" >
                <label class="custom-file-label" for="customFile">فایل خود را انتخاب کنید</label>
                @foreach ($errors->all() as $error)
                <span role="alert">
                    <strong class="text-danger">{!! $errors->first() !!}</strong><br>
                </span>
                @endforeach

            </fieldset>
            <fieldset class="form-group">
                <div class="checkbox checkbox-primary checkbox-glow ">
                    <input id="is_private" type="checkbox" name="is_private"
                           class="@error('is_private') is-invalid @enderror">
                    <label for="is_private">
                        به صورت خصوصی آپلود شود
                    </label>
                </div>
            </fieldset>
            <fieldset class="form-group">
                    <button type="submit" class="btn btn-primary pull-left justify-content-end">آپلود</button>
            </fieldset>

        </div>
    </div>
</div>
