@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/manegment/dashbord/users/vendors/css/forms/select/select2.min.css')}}">

    <!-- END: Page CSS-->
@endpush

@section('title', 'محصولات')
@section('title_page', 'افزودن محصول')

@section('content')
    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">افزودن محصول جدید</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('Manegment.UsersAdmin.products.store')}}" enctype="multipart/form-data">
                            @csrf
                            @include('manegment.usersAdmin.products.products.partials._form')
                            <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <!-- END: Page Vendor JS-->
    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/select/form-select2.js')}}"></script>

    <!-- END: Page JS-->

@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->
{{--    <script>--}}
{{--        $( "#add_properties" ).click(function() {--}}
{{--            $( "<div class=\"form-group col-md-12 \">\n"+$(form_propertie).html()+"</div>").appendTo( "#add_form_properties");--}}

{{--        });--}}
{{--    </script>--}}

    <script>
        $( "#add_properties" ).click(function() {
            $( "<div class=\"form-group col-md-12 \">\n" +
                "                    <div class=\"row justify-content-between col-md-12\">\n" +
                "                        <div class=\"col-md-6 col-sm-12 form-group\">\n" +
                "                            <fieldset class=\"form-group\">\n" +
                "                                <label for=\"property_id\"> ویژگی محصول</label>\n" +
                "                                <div class=\"form-group\">\n" +
                "                                    <select name=\"properties_id[]\"\n" +
                "                                            data-placeholder=\"ویژگی محصول...\"\n" +
                "                                            class=\"select2-theme form-control @error('properties_id') is-invalid @enderror\">\n" +
                "                                        <optgroup label=\"ویژگی های محصول\">\n" +
                "                                            @forelse($properties as $property)\n" +
                "                                                <option\n" +
                "                                                    name=\"{{$property->name ?? ''}}\"\n" +
                "                                                    {{old('property') == $property->id ? 'selected' : ''}}\n" +
                "                                                >\n" +
                "                                                    {{$property->name ?? ''}}\n" +
                "\n" +
                "                                                </option>\n" +
                "                                            @empty\n" +
                "                                            @endforelse\n" +
                "                                        </optgroup>\n" +
                "                                    </select>\n" +
                "                                </div>\n" +
                "                                <div>\n" +
                "                                    @error('properties_id')\n" +
                "                                    <span role=\"alert\">\n" +
                "                                            <strong class=\"text-danger\">{{ $message }}</strong>\n" +
                "                                        </span>\n" +
                "                                    @enderror\n" +
                "                                </div>\n" +
                "                            </fieldset>\n" +
                "                        </div>\n" +
                "                        <div class=\"col-md-6 col-sm-12 form-group\">\n" +
                "                            <fieldset class=\"form-group\">\n" +
                "                                <label for=\"property_values\">مقدار ویژگی محصول </label>\n" +
                "                                <input type=\"text\" class=\"form-control @error('property_values') is-invalid @enderror\"\n" +
                "                                       name=\"property_values[]\"\n" +
                "                                       placeholder=\"مقدار ویژگی محصول را وارد کنید\"\n" +
                "                                @error('property_values')\n" +
                "                                <span class=\"invalid-feedback\" role=\"alert\">\n" +
                "                                        <strong class=\"text-danger\">{{ $message }}</strong>\n" +
                "                                    </span>\n" +
                "                                @enderror\n" +
                "                            </fieldset>\n" +
                "                        </div>\n" +
                "                    </div>\n" +
                "                </div>").appendTo( "#add_form_properties");

        });
    </script>
@endpush
