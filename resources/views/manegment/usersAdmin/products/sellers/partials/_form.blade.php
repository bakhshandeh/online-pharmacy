<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="name">نام فروشنده به انگلیسی</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror"
                       name="name" id="name" value="{{ $seller->name ?? old('name') }}"
                       placeholder="نام فروشنده به انگلیسی را وارد کنید"
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="persian_name">نام فروشنده به فارسی</label>
                <input type="text" class="form-control @error('persian_name') is-invalid @enderror"
                       name="persian_name" id="persian_name"
                       value="{{ $seller->persian_name ?? old('persian_name') }}"
                       placeholder="نام فروشنده به فارسی را وارد کنید"
                @error('persian_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="phone_number">تلفن</label>
                <input type="text" class="form-control @error('phone_number') is-invalid @enderror"
                       name="phone_number" id="phone_number"
                       value="{{ $seller->phone_number ?? old('phone_number') }}"
                       placeholder="تلفن را وارد کنید"
                @error('phone_number')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="email">ایمیل</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror"
                       name="email" id="email"
                       value="{{ $seller->email ?? old('email') }}"
                       placeholder="ایمیل را وارد کنید"
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="province_id">استان</label>
                <div class="form-group">
                    <select name="province_id" id="province_id" data-placeholder="انتخاب استان..." class="select2-theme form-control @error('province_id') is-invalid @enderror" >
                        <optgroup label="اسامی استانها">
                            @forelse($provinces as $province)
                                <option
                                    name="{{$province->name ?? ''}}"
                                    value="{{$province->id ?? ''}}"
                                    {{old('province') == $province->id ? 'selected' : ''}}
                                >
                                    {{$province->name ?? ''}}

                                </option>
                            @empty
                            @endforelse
                        </optgroup>
                    </select>
                </div>
                <div>
                    @error('province_id')
                    <span role="alert" >
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </fieldset>

        </div>
{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="city_id">شهر</label>--}}
{{--                <div class="form-group">--}}
{{--                    <select name="city_id" id="city_id" data-placeholder="انتخاب شهر..." class="select2-theme form-control @error('city_id') is-invalid @enderror" >--}}
{{--                        <optgroup label="اسامی شهرها">--}}
{{--                            @forelse($cities as $city)--}}
{{--                                <option--}}
{{--                                    name="{{$city->name ?? ''}}"--}}
{{--                                    value="{{$city->id ?? ''}}"--}}
{{--                                    {{old('city') == $city->id ? 'selected' : ''}}--}}
{{--                                >--}}
{{--                                    {{$city->name ?? ''}}--}}
{{--                                </option>--}}
{{--                            @empty--}}
{{--                            @endforelse--}}
{{--                        </optgroup>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    @error('city_id')--}}
{{--                    <span role="alert" >--}}
{{--                        <strong class="text-danger">{{ $message }}</strong>--}}
{{--                    </span>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--            </fieldset>--}}

{{--        </div>--}}
{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="quarter_id">بخش</label>--}}
{{--                <div class="form-group">--}}
{{--                    <select name="quarter_id" id="quarter_id" data-placeholder="انتخاب بخش..." class="select2-theme form-control @error('quarter_id') is-invalid @enderror" >--}}
{{--                        <optgroup label="اسامی بخشها">--}}
{{--                            @forelse($quarters as $quarter)--}}
{{--                                <option--}}
{{--                                    name="{{$quarter->name ?? ''}}"--}}
{{--                                    value="{{$quarter->id ?? ''}}"--}}
{{--                                    {{old('quarter') == $quarter->id ? 'selected' : ''}}--}}

{{--                                >--}}
{{--                                    {{$quarter->name ?? ''}}--}}

{{--                                </option>--}}
{{--                            @empty--}}
{{--                            @endforelse--}}
{{--                        </optgroup>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    @error('quarter_id')--}}
{{--                    <span role="alert" >--}}
{{--                        <strong class="text-danger">{{ $message }}</strong>--}}
{{--                    </span>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--            </fieldset>--}}
{{--        </div>--}}

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="postal_code">کدپستی</label>
                <input type="text" class="form-control @error('postal_code') is-invalid @enderror"
                       name="postal_code" id="postal_code" value="{{ $seller->postal_code ?? old('postal_code') }}"
                       placeholder="کدپستی راوارد کنید."
                @error('postal_code')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="street_alley"> آدرس دقیق خیابان و کوچه</label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        <textarea data-length="200" class="form-control char-textarea" name="street_alley" id="street_alley" rows="3" placeholder="آدرس دقیق خیابان و کوچه"></textarea>
                    </fieldset>
                    <small class="counter-value float-right"><span class="char-count">0</span> / 200 </small>
                </div>
                <div>
                    @error('street_alley')
                    <span role="alert" >
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
        </div>

        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="no">پلاک</label>
                <input type="text" class="form-control @error('no') is-invalid @enderror"
                       name="no" id="no" value="{{ $seller->no ?? old('no') }}"
                       placeholder="پلاک راوارد کنید."
                @error('no')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="floor">طبقه</label>
                <input type="text" class="form-control @error('floor') is-invalid @enderror"
                       name="floor" id="floor" value="{{ $seller->floor ?? old('floor') }}"
                       placeholder="طبقه راوارد کنید."
                @error('floor')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="unit">واحد</label>
                <input type="text" class="form-control @error('unit') is-invalid @enderror"
                       name="unit" id="unit" value="{{ $seller->unit ?? old('unit') }}"
                       placeholder="واحد راوارد کنید."
                @error('unit')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

    </div>
</div>

