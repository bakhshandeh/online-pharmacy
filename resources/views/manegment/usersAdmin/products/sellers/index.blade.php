@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'فروشندگان')
@section('title_page', 'لیست فروشندگان')

@section('content')
    <!-- Column selectors with Export Options and print table -->
    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">لیست فروشندگان</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                {{--                                <table class="table table-striped dataex-html5-selectors">--}}
                                <table class="table table-striped table-bordered complex-headers">
                                    <thead>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <th>
                                            نام فارسی
                                        </th>
                                        <th>
                                            تلفن
                                        </th>
                                        <th data-hide="phone,tablet">
                                            ایمیل
                                        </th>
                                        <th data-hide="phone,tablet">
                                            امتیاز
                                        </th>
                                        <th data-hide="phone,tablet">
                                            آدرس
                                        </th>
                                        <th data-hide="phone,tablet">
                                            عملیات
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($sellers as $seller)
                                        <tr>
                                            <td>{{$seller->name}}</td>
                                            <td>{{$seller->persian_name}}</td>
                                            <td>{{$seller->phone_number}}</td>
                                            <td>{{$seller->email}}</td>
                                            <td>{{$seller->score}}</td>
                                            <td>
                                                @if(!is_null($seller->province_id))
                                                    {{$seller->province->name ? "استان:". $seller->province->name."-" : ''}}
                                                    @if(!is_null($seller->city_id))
                                                        {{$seller->city->name ? "شهرستان :".$seller->city->name."-" : ''}}
                                                        @if(!is_null($seller->quarter_id))
                                                            {{$seller->quarter->name ? "بخش :".$seller->quarter->name."-" : ''}}
                                                            {{$seller->street_alley ? "خیابان و کوچه :".$seller->street_alley."-" : ''}}
                                                            {{$seller->postal_code ? "کدپستی :".$seller->postal_code."-" : ''}}
                                                            {{$seller->no ? "پلاک :".$seller->no."-" : ''}}
                                                            {{$seller->floor ? "طبقه :".$seller->floor."-" : ''}}
                                                            {{$seller->unit ? "واحد :".$seller->unit  : ''}}
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                            <td>

                                                <a href="{{route('Manegment.UsersAdmin.products.sellers.edit', ['seller' => $seller->id])}}"><i
                                                        class="bx bx-edit-alt"></i></a>

{{--                                                <form class="inline" style="display: inline !important;" method="post" action="{{route('Manegment.UsersAdmin.products.sellers.destroy', ['seller' => $seller->id])}}" >--}}
{{--                                                    @csrf--}}
{{--                                                    @method('DELETE')--}}
{{--                                                    <button type="submit" style="background: none;color: inherit;border: none;padding: 0;font: inherit;cursor: pointer;outline: inherit; display: inline;"><i class="bx bx-trash-alt" style="color: red"></i></button>--}}
{{--                                                </form>--}}

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>موردی یافت نشد.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <th>
                                            نام فارسی
                                        </th>
                                        <th>
                                            تلفن
                                        </th>
                                        <th data-hide="phone,tablet">
                                            ایمیل
                                        </th>

                                        <th data-hide="phone,tablet">
                                            امتیاز
                                        </th>
                                        <th data-hide="phone,tablet">
                                            آدرس
                                        </th>
                                        <th data-hide="phone,tablet">
                                            عملیات
                                        </th>


                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->


@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script
        src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script
        src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
{{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>--}}
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
{{--        <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>--}}
{{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>--}}
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/datatables/datatable.js')}}"></script>
    <!-- END: Page JS-->
@endpush

@push('before-body-end')


@endpush
