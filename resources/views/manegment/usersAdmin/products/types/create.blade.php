@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'انواع محصولات')
@section('title_page', 'افزودن انواع محصولات')

@section('content')
    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">افزودن نوع جدید</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('Manegment.UsersAdmin.products.types.store')}}" >
                            @csrf
                            @include('manegment.usersAdmin.products.types.partials._form')
                            <div class=" col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page JS-->

    <!-- END: Page JS-->
@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->

@endpush
