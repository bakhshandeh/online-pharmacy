@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'تولیدکنندگان')
@section('title_page', 'لیست تولیدکنندگان')

@section('content')
    <!-- Column selectors with Export Options and print table -->
    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">لیست تولیدکنندگان</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                {{--                                <table class="table table-striped dataex-html5-selectors">--}}
                                <table class="table table-striped table-bordered complex-headers">
                                    <thead>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <th>
                                            نام فارسی
                                        </th>
                                        <th>
                                            تلفن
                                        </th>
                                        <th data-hide="phone,tablet">
                                            ایمیل
                                        </th>

                                        <th data-hide="phone,tablet">
                                            امتیاز
                                        </th>
                                        <th data-hide="phone,tablet">
                                            آدرس
                                        </th>
                                        <th data-hide="phone,tablet">
                                            عملیات
                                        </th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($manufacturers as $manufacturer)
                                        <tr>
                                            <td>{{$manufacturer->name}}</td>
                                            <td>{{$manufacturer->persian_name}}</td>
                                            <td>{{$manufacturer->phone_number}}</td>
                                            <td>{{$manufacturer->email}}</td>
                                            <td>{{$manufacturer->score}}</td>
                                            <td>
                                                @if(!is_null($manufacturer->province_id))
                                                    {{$manufacturer->province->name ? "استان:". $manufacturer->province->name."-" : ''}}
                                                    @if(!is_null($manufacturer->city_id))
                                                        {{$manufacturer->city->name ? "شهرستان :".$manufacturer->city->name."-" : ''}}
                                                        @if(!is_null($manufacturer->quarter_id))
                                                            {{$manufacturer->quarter->name ? "بخش :".$manufacturer->quarter->name."-" : ''}}
                                                            {{$manufacturer->street_alley ? "خیابان و کوچه :".$manufacturer->street_alley."-" : ''}}
                                                            {{$manufacturer->postal_code ? "کدپستی :".$manufacturer->postal_code."-" : ''}}
                                                            {{$manufacturer->no ? "پلاک :".$manufacturer->no."-" : ''}}
                                                            {{$manufacturer->floor ? "طبقه :".$manufacturer->floor."-" : ''}}
                                                            {{$manufacturer->unit ? "واحد :".$manufacturer->unit  : ''}}
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                            <td>

                                                <a href="{{route('Manegment.UsersAdmin.products.manufacturers.edit', ['manufacturer' => $manufacturer->id])}}"><i class="bx bx-edit-alt"></i></a>

{{--                                                <form class="inline" method="post" style="display: inline !important;" action="{{route('Manegment.UsersAdmin.products.manufacturers.destroy', ['manufacturer' => $manufacturer->id])}}" >--}}
{{--                                                    @csrf--}}
{{--                                                    @method('DELETE')--}}
{{--                                                    <button type="submit" style="background: none;color: inherit;border: none;padding: 0;font: inherit;cursor: pointer;outline: inherit; display: inline;"><i class="bx bx-trash-alt" style="color: red"></i></button>--}}
{{--                                                </form>--}}

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>موردی یافت نشد.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>
                                            نام
                                        </th>
                                        <th>
                                            نام فارسی
                                        </th>
                                        <th>
                                            تلفن
                                        </th>
                                        <th data-hide="phone,tablet">
                                            ایمیل
                                        </th>

                                        <th data-hide="phone,tablet">
                                            امتیاز
                                        </th>
                                        <th data-hide="phone,tablet">
                                            آدرس
                                        </th>
                                        <th data-hide="phone,tablet">
                                            عملیات
                                        </th>


                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->


@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
{{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>--}}
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    {{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>--}}
{{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>--}}
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/datatables/datatable.js')}}"></script>
    <!-- END: Page JS-->
@endpush

@push('before-body-end')

@endpush
