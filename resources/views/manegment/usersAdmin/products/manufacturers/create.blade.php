@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/manegment/dashbord/users/vendors/css/forms/select/select2.min.css')}}">

    <!-- END: Page CSS-->
@endpush

@section('title', 'تولیدکنندگان')
@section('title_page', 'افزودن تولیدکنندگان')

@section('content')
    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">افزودن تولیدکننده جدید</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('Manegment.UsersAdmin.products.manufacturers.store')}}" >
                            @csrf
                            @include('manegment.usersAdmin.products.manufacturers.partials._form')
                            <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/select/form-select2.js')}}"></script>
    <!-- END: Page JS-->

@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->

@endpush
