@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'ویژگی محصولات')
@section('title_page', 'ویرایش ویژگی محصولات')

@section('content')

    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ویرایش ویژگی</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('Manegment.UsersAdmin.products.properties.update',['property' => $property->id])}}" >
                            @csrf
                            @method('put')
                            @include('manegment.usersAdmin.products.properties.partials._form')
                            <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">بروزرسانی</button>
                                <a  href="{{route('Manegment.UsersAdmin.products.properties.index')}}" class="btn btn-danger mr-1 mb-1">انصراف</a>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page JS-->

    <!-- END: Page JS-->
@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->

@endpush
