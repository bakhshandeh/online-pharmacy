<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <fieldset class="form-group">
                <label for="name">نام ویژگی</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror"
                       name="name" id="name" value="{{ $property->name ?? old('name') }}"
                       placeholder="نام ویژگی را وارد کنید"
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
    </div>
</div>

