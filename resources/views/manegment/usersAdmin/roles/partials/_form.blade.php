<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="name">نام نقش به انگلیسی</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror"
                       name="name" id="name" value="{{ $role->name ?? old('name') }}"
                       placeholder="نام نقش به انگلیسی را وارد کنید"
                    {{ $role ?? '' ? 'readonly' : '' }}>
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="persian_name">نام نقش به فارسی</label>
                <input type="text" class="form-control @error('persian_name') is-invalid @enderror"
                       name="persian_name" id="persian_name"
                       value="{{ $role->persian_name ?? old('persian_name') }}"
                       placeholder="نام نقش به فارسی را وارد کنید"
                    {{ $role ?? '' ? 'readonly' : '' }}>
                @error('persian_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-12 col-md-12">
            <div class="col-12 col-md-12 ">
                <div class="form-group col-12 col-md-12">
                    <h2>
                        <span>مجوز های سطح دسترسی مورد نیاز نقش:</span>
                    </h2>
                    @error('permissions')
                    <span class="invalid-feedback" role="alert">
                                            <strong class="text-danger">{{ $message }}</strong>
                                        </span>
                    @enderror
                </div>
                <div class="form-group inline">
                    @forelse($permissions as $permission)
                        <div class="checkbox checkbox-primary checkbox-glow col-md-4">
                            <input id="{{'permission'.$permission->id}}" type="checkbox" name="permissions[]"
                                   class="@error('permissions') is-invalid @enderror" value="{{$permission->name}}"
                            @isset($role){{ $role->permissions->contains($permission)? 'checked': ''  }}@endisset>
                            <label for="{{'permission'.$permission->id}}">
                                {{$permission->persian_name}}
                            </label>
                        </div>
                    @empty
                        <div class="form-group col-md-4">
                            <span>موردی یافت نشد.</span>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    </div>
</div>

