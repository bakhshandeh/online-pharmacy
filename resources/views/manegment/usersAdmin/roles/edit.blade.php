@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <!-- END: Page CSS-->
@endpush

@section('title', 'نقش ها')
@section('title_page', 'ویرایش نقش ها')

@section('content')

    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">ویرایش سطح دسترسی نقش</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('Manegment.UsersAdmin.roles.update',['role' => $role->id])}}" >
                            @csrf
                            @method('put')
                            @include('manegment.usersAdmin.roles.partials._form')
                            <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">بروزرسانی</button>
                                <a  href="{{route('Manegment.UsersAdmin.roles.index')}}" class="btn btn-danger mr-1 mb-1">انصراف</a>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <!-- END: Page JS-->
@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->

@endpush
