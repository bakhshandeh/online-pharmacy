@extends('manegment.dashboard.admins.layouts.panel')

@push('styles')
@endpush

@section('title', 'نسخه')
@section('title_page', 'نسخه های قیمت گذاری نشده')

@section('content')
    <!-- Column selectors with Export Options and print table -->
    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">لیست نسخه های قیمت گذاری نشده</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                {{--                                <table class="table table-striped dataex-html5-selectors">--}}
                                <table class="table table-striped table-bordered complex-headers">
                                    <thead>
                                    <tr>
                                        <th>
                                            کد نسخه
                                        </th>
                                        <th>
                                            کد مشتری
                                        </th>
                                        <th>
                                            نام بیمار
                                        </th>
                                        <th>
                                            تاریخ نسخه
                                        </th>
                                        <th>
                                            تارخ ثبت
                                        </th>
                                        <th>
                                            عملیات
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($prescriptions as $prescription)
                                        <tr>
                                            <td>{{$prescription->id ?? ''}}</td>
                                            <td>{{$prescription->customer->id ?? ''}}</td>
                                            <td>{{$prescription->sick_name ?? ''}}</td>
                                            <td>{{$prescription->date ?? ''}}</td>
                                            <td>{{$prescription->created_at ?? ''}}</td>
                                            <td>
                                                @can('Determine the price of the version')

                                                <a href="{{route('Manegment.UsersAdmin.prescriptions.set.price', ['prescription' => $prescription->id])}}"><i class="bx bx-edit-alt"></i>قیمت گذاری</a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>موردی یافت نشد.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>
                                            کد نسخه
                                        </th>
                                        <th>
                                            کد مشتری
                                        </th>
                                        <th>
                                            نام بیمار
                                        </th>
                                        <th>
                                            تاریخ نسخه
                                        </th>
                                        <th>
                                            تارخ ثبت
                                        </th>
                                        <th>
                                            عملیات
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->

@endsection

@push('scripts')
@endpush

@push('before-body-end')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    {{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>--}}
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/datatables/datatable.js')}}"></script>
    <!-- END: Page JS-->
@endpush
