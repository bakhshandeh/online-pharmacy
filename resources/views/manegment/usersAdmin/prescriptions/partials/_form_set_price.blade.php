<div class="card-body">
    <div class="row">
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="customer">نام مشتری سفارش دهنده</label>
                <input type="text" class="form-control @error('customer') is-invalid @enderror"
                       name="customer" id="customer"
                       value="{{ $prescription->customer->first_name ? $prescription->customer->first_name .' '. $prescription->customer->last_name ?? '' : old('customer') }}"
                       placeholder="نام مشتری سفارش دهنده را وارد کنید" readonly>
                @error('customer')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="national_code">کدملی بیمار</label>
                <input type="text" class="form-control @error('national_code') is-invalid @enderror"
                       name="national_code" id="national_code"
                       value="{{ $prescription->national_code ?? old('national_code') }}"
                       placeholder="کدملی بیمار را وارد کنید" readonly>
                @error('national_code')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="sick_name">نام بیمار</label>
                <input type="text" class="form-control @error('sick_name') is-invalid @enderror"
                       name="sick_name" id="sick_name"
                       value="{{ $prescription->sick_name ?? old('sick_name') }}"
                       placeholder="نام بیمار را وارد کنید" readonly>
                @error('sick_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="doctor_name">نام دکتر</label>
                <input type="text" class="form-control @error('doctor_name') is-invalid @enderror"
                       name="doctor_name" id="doctor_name"
                       value="{{ $prescription->doctor_name ?? old('doctor_name') }}"
                       placeholder="نام دکتر را وارد کنید" readonly>
                @error('doctor_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        {{--        <div class="col-md-4">--}}
        {{--            <fieldset class="form-group">--}}
        {{--                <label for="insurance_type">نام بیمه </label>--}}
        {{--                <input type="text" class="form-control @error('insurance_type') is-invalid @enderror"--}}
        {{--                       name="insurance_type" id="insurance_type"--}}
        {{--                       value="{{ $prescription->insurance_type ?? old('insurance_type') }}"--}}
        {{--                       placeholder="نام بیمه را وارد کنید" readonly>--}}
        {{--                @error('insurance_type')--}}
        {{--                <span class="invalid-feedback" role="alert">--}}
        {{--                    <strong class="text-danger">{{ $message }}</strong>--}}
        {{--                </span>--}}
        {{--                @enderror--}}
        {{--            </fieldset>--}}
        {{--        </div>--}}
        {{--        <div class="col-md-4">--}}
        {{--            <fieldset class="form-group">--}}
        {{--                <label for="insurance_validity_date">تاریخ اعتبار بیمه</label>--}}
        {{--                <input type="date" class="form-control @error('insurance_validity_date') is-invalid @enderror"--}}
        {{--                       name="insurance_validity_date" id="insurance_validity_date"--}}
        {{--                       value="{{$prescription->insurance_validity_date ? date_format($prescription->insurance_validity_date, 'Y-d-m') : old('insurance_validity_date') }}"--}}
        {{--                       placeholder="تاریخ اعتبار بیمه را وارد کنید" readonly>--}}
        {{--                @error('insurance_validity_date')--}}
        {{--                <span class="invalid-feedback" role="alert">--}}
        {{--                    <strong class="text-danger">{{ $message }}</strong>--}}
        {{--                </span>--}}
        {{--                @enderror--}}
        {{--            </fieldset>--}}
        {{--        </div>--}}

        <div class="col-md-3">
            <fieldset class="form-group">
                <label for="date">تاریخ نسخه</label>
                <input type="text" class="form-control @error('date') is-invalid @enderror"
                       name="date" id="date"
                       value="{{$prescription->date ? date_format($prescription->date,'Y-d-m') : old('date') }}"
                       placeholder="تاریخ نسخه را وارد کنید" readonly>
                @error('date')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>


        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="description_customer"> توضیحات </label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        <textarea class="form-control char-textarea" maxlength="190" name="description_customer"
                                  id="description_customer" rows="3"
                                  placeholder="توضیحات "
                                  readonly>{{ $prescription->description_customer ?? old('description_customer') }}</textarea>
                    </fieldset>
                    <small class="counter-value float-right"><span class="char-count">0</span> / 190 </small>
                </div>
                <div>
                    @error('description_customer')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
            <hr>
        </div>

        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="picture"> نسخه </label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        @if($prescription->files()->where('type', 'image')->first())
                            <a href="">
                                <img src="{{ asset(''.Storage::url($prescription->files()
                                                                    ->where('type', 'image')->first()->type.
                                                                      DIRECTORY_SEPARATOR .$prescription->files()
                                                                      ->where('type', 'image')->first()->name).'')}}"
                                     alt="" width="1000" height="500">
                            </a>
                        @endif
                    </fieldset>
                </div>
                <div>
                    @error('picture')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
            <hr>
        </div>

        <div class="col-md-12">
            <fieldset class="form-group">
                <label class="control-label" for="picture"><h3>لیست داروهای </h3></label>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered complex-headers">

                        <thead>
                            <tr>
                            <th>
                                نام داارو
                            </th>
                            <th>
                                نام فارسی داارو
                            </th>
                            <th>
                                نام عمومی داارو
                            </th>
                            <th>
                                دوز/حجم دارو
                            </th>
                            <th>
                                تولید کننده
                            </th>
                            <th>
                                موجودی
                            </th>
                            <th>
                                قیمت
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($productsCategorId4 as $product)
                            @if(!is_null($product->sellers()->where('seller_id', auth()->user()->seller_id)->first()))
                                <tr>
                                    <td>{{$product->name ?? ''}}</td>
                                    <td>{{$product->persian_name ?? ''}}</td>
                                    <td>{{$product->generic_name ?? ''}}</td>
                                    <td>{{$product->amount ?? ''}}</td>
                                    <td>{{$product->manufacturer ?? ''}}</td>
                                    <td>{{$product->sum_number ?? ''}}</td>
                                    <td>{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price ?? ''}}</td>
                                    <td>
                                        <form
                                            action="{{route('Manegment.UsersAdmin.prescriptions.add.product', ['prescription' => $prescription->id, 'product' => $product->id])}}"
                                            method="post" class="form-inline input-number">
                                        </form>
                                        <form
                                            action="{{route('Manegment.UsersAdmin.prescriptions.add.product', ['prescription' => $prescription->id, 'product' => $product->id])}}"
                                            method="post" class="form-inline input-number">
                                            @csrf
                                            <input type="hidden" name="seller_id" value="{{auth()->user()->seller_id}}">
                                            <input type="hidden" name="price"
                                                   value="{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price}}">
                                            <select name="quantity" id="quantity" class="form-control input-sm mr-sm-2">
                                                @for($i = 0; $i <= $product->sum_number ; $i++)
                                                    <option
                                                        value="{{$i}}" {{$product->pivot->quantity == $i ? 'selected' : ''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                            <button type="submit" class="btn btn-primary btn-sm"> بروزرسانی</button>
                                        </form>
                                    </td>

                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td>موردی یافت نشد.</td>
                            </tr>
                        @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                            <th>
                                نام داارو
                            </th>
                            <th>
                                نام فارسی داارو
                            </th>
                            <th>
                                نام عمومی داارو
                            </th>
                            <th>
                                دوز/حجم دارو
                            </th>
                            <th>
                                تولید کننده
                            </th>
                            <th>
                                موجودی
                            </th>
                            <th>
                                قیمت
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </fieldset>
        </div>


        <div class="col-md-12">
            <fieldset class="form-group">
                <label class="control-label" for="picture"><h3>داروهای اضافه شده برای نسخه</h3></label>

                <div class="table-responsive">
                        <table class="table table-striped table-bordered complex-headers">

                        <thead>
                        <tr>
                            <th>
                                نام داارو
                            </th>
                            <th>
                                نام فارسی داارو
                            </th>
                            <th>
                                نام عمومی داارو
                            </th>
                            <th>
                                تولید کننده
                            </th>
                            <th>
                                عملیات
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($prescription->products as $product)
                            @if(!is_null($product->sellers()->where('seller_id', auth()->user()->seller_id)->first()))
                                <tr>
                                    <td>{{$product->name ?? ''}}</td>
                                    <td>{{$product->persian_name ?? ''}}</td>
                                    <td>{{$product->generic_name ?? ''}}</td>
                                    <td>{{$product->manufacturer ?? ''}}</td>

                                    <td>
                                        <form
                                            action="{{route('Manegment.UsersAdmin.prescriptions.add.product', ['prescription' => $prescription->id, 'product' => $product->id])}}"
                                            method="post" class="form-inline input-number">
                                            @csrf
                                            <input type="hidden" name="seller_id" value="{{auth()->user()->seller_id}}">
                                            <input type="hidden" name="price"
                                                   value="{{$product->sellers()->where('seller_id', auth()->user()->seller_id)->first()->pivot->seller_price}}">
                                            <select name="quantity" id="quantity" class="form-control input-sm mr-sm-2">
                                                @for($i = 0; $i <= $product->sum_number ; $i++)
                                                    <option
                                                        value="{{$i}}" {{$product->pivot->quantity == $i ? 'selected' : ''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                            <button type="submit" class="btn btn-primary btn-sm"> بروزرسانی</button>
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @empty
                            <tr>
                                <td>موردی یافت نشد.</td>
                            </tr>
                        @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>
                                    نام داارو
                                </th>
                                <th>
                                    نام فارسی داارو
                                </th>
                                <th>
                                    نام عمومی داارو
                                </th>
                                <th>
                                    تولید کننده
                                </th>
                                <th>
                                    عملیات
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="price">قیمت </label>
                <input type="text" class="form-control @error('price') is-invalid @enderror"
                       name="price" id="price"
                       value="{{ $price ?? old('price') }}"
                       placeholder="قیمت را وارد کنید" readonly>
                @error('price')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="description_employee"> توضیحات دکتر </label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        <textarea class="form-control char-textarea" maxlength="190" name="description_employee"
                                  id="description_employee" rows="3"
                                  placeholder="توضیحات ">{{ $prescription->description_employee ?? old('description_employee') }}</textarea>
                    </fieldset>
                    <small class="counter-value float-right"><span class="char-count">0</span> / 190 </small>
                </div>
                <div>
                    @error('description_employee')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
            <hr>
        </div>

    </div>
</div>

