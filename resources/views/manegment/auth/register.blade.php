@extends('manegment.auth.layouts.auth')

@section('title', 'Register')
@section('content')
    <!-- register section starts -->
    <section class="row flexbox-container">
        <div class="col-xl-8 col-10">
            <div class="card bg-authentication mb-0">
                <div class="row m-0">
                    <!-- register section left -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                            <div class="card-header pb-1">
                                <div class="card-title">
                                    <h4 class="text-center mb-2">ثبت نام</h4>
                                </div>
                            </div>
                            <div class="text-center">
                                <p> <small class="line-height-2 d-inline-block"> لطفا جزئیات خود را برای ثبت نام وارد کرده و عضوی از جامعه عالی ما شوید.</small>
                                </p>
                            </div>
                            <div class="card-content">
                                <div class="card-body">
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-md-6 mb-50">
                                                <label for="first_name">نام</label>
                                                <input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name" autofocus placeholder="نام">
                                                @error('first_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group col-md-6 mb-50">
                                                <label for="last_name">نام خانوادگی</label>
                                                <input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus placeholder="نام خانوادگی">
                                                @error('last_name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
{{--                                        <div class="form-group mb-50">--}}
{{--                                            <label class="text-bold-700" for="exampleInputUsername1">نام کاربری</label>--}}
{{--                                            <input type="text" class="form-control text-left" id="exampleInputUsername1" placeholder="نام کاربری" dir="ltr">--}}
{{--                                        </div>--}}

                                        <div class="form-group mb-50">
                                            <label class="text-bold-700" for="phone_number">تلفن همراه</label>
                                            <input id="phone_number" type="text" class="form-control text-left @error('phone_number') is-invalid @enderror"  placeholder="تلفن همراه" dir="ltr" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number">
                                            @error('phone_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group mb-50">
                                            <label class="text-bold-700" for="email">آدرس ایمیل</label>
                                            <input id="email" type="email" class="form-control text-left @error('email') is-invalid @enderror"  placeholder="آدرس ایمیل" dir="ltr" name="email" value="{{ old('email') }}" required autocomplete="email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group mb-2">
                                            <label class="text-bold-700" for="password">رمز عبور</label>
                                            <input type="password" class="form-control text-left @error('password') is-invalid @enderror" id="password" placeholder="رمز عبور" dir="ltr" name="password" required autocomplete="new-password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group mb-2">
                                            <label class="text-bold-700" for="password-confirm">تکرار رمز عبور</label>
                                            <input type="password" class="form-control text-left" id="password-confirm" placeholder="تکرار رمز عبور" dir="ltr" name="password_confirmation" required autocomplete="new-password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <button type="submit" class="btn btn-primary glow position-relative w-100">ثبت نام<i id="icon-arrow" class="bx bx-left-arrow-alt"></i></button>
                                    </form>
                                    <hr>
                                    <div class="text-center"><small class="mr-25">حساب کاربری دارید؟</small><a href="{{route('login')}}"><small>ورود</small> </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- image section right -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                        <img class="img-fluid" src="{{ asset('assets/manegment/dashbord/users/images/pages/register.png')}}" alt="branding logo">
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- register section endss -->


@endsection
