
<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl" dir="rtl">
<!-- BEGIN: Head-->
<head>
    @include('manegment.auth.partials.head')
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">

<!-- BEGIN: Content-->
@include('manegment.auth.partials.main')
<!-- END: Content-->

<!-- BEGIN:  JS-->
@include('manegment.auth.partials.scripts')
<!-- END:  JS-->

</body>
<!-- END: Body-->
</html>
