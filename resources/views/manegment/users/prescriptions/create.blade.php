@extends('manegment.dashboard.users.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/manegment/dashbord/users/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/vendors/css/pickers/pickadate/pickadate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/vendors/css/pickers/daterange/daterangepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css') }}">
    <!-- END: Page CSS-->
    <!-- BEGIN: Vendor CSS-->
    <!-- END: Vendor CSS-->


@endpush

@section('title', 'نسخه')
@section('title_page', 'افزودن نسخه')

@section('content')
    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">افزودن نسخه جدید</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('manegment.users.prescriptions.store')}}" enctype="multipart/form-data">
                            @csrf
                            @include('manegment.users.prescriptions.partials._form')
                            <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/forms/select/select2.full.min.js')}}"></script>

    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/picker.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/picker.date.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/picker.time.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/legacy.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/daterange/moment.min.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js') }}"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/select/form-select2.js')}}"></script>

    <script src="{{ asset('assets/manegment/dashbord/users/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>
    <!-- END: Page JS-->
@endpush

@push('before-body-end')
@endpush
