<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="sick_name">نام بیمار</label>
                <input type="text" class="form-control @error('sick_name') is-invalid @enderror"
                       name="sick_name" id="sick_name"
                       value="{{ $prescription->sick_name ?? old('sick_name') }}"
                       placeholder="نام بیمار را وارد کنید">
                @error('sick_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="national_code">کدملی بیمار</label>
                <input type="text" class="form-control @error('national_code') is-invalid @enderror"
                       name="national_code" id="national_code"
                       value="{{ $prescription->national_code ?? old('national_code') }}"
                       placeholder="کدملی بیمار را وارد کنید">
                @error('national_code')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="doctor_name">نام دکتر</label>
                <input type="text" class="form-control @error('doctor_name') is-invalid @enderror"
                       name="doctor_name" id="doctor_name"
                       value="{{ $prescription->doctor_name ?? old('doctor_name') }}"
                       placeholder="نام دکتر را وارد کنید">
                @error('doctor_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="date">تاریخ نسخه</label>
                <fieldset class="form-group position-relative has-icon-left">
                    <input type="text" class="form-control shamsi-datepicker-list @error('date') is-invalid @enderror"
                           name="date" id="date"
                           value="{{ $prescription->date ?? old('date') }}"
                           placeholder="تاریخ نسخه را انتخاب کنید">
                    <div class="form-control-position">
                        <i class="bx bx-calendar"></i>
                    </div>
                </fieldset>
                @error('date')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>

        </div>

{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="insurance_type">نام بیمه </label>--}}
{{--                <input type="text" class="form-control @error('insurance_type') is-invalid @enderror"--}}
{{--                       name="insurance_type" id="insurance_type"--}}
{{--                       value="{{ $prescription->insurance_type ?? old('insurance_type') }}"--}}
{{--                       placeholder="نام بیمه را وارد کنید">--}}
{{--                @error('insurance_type')--}}
{{--                <span class="invalid-feedback" role="alert">--}}
{{--                    <strong class="text-danger">{{ $message }}</strong>--}}
{{--                </span>--}}
{{--                @enderror--}}
{{--            </fieldset>--}}
{{--        </div>--}}
{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="insurance_validity_date">تاریخ اعتبار بیمه</label>--}}

{{--        <fieldset class="form-group position-relative has-icon-left">--}}
{{--            <input type="text" class="form-control shamsi-datepicker-list @error('insurance_validity_date') is-invalid @enderror"--}}
{{--                   name="insurance_validity_date" id="insurance_validity_date"--}}
{{--                   value="{{ $prescription->insurance_validity_date ?? old('insurance_validity_date') }}"--}}
{{--                   placeholder="تاریخ اعتبار بیمه را وارد کنید">--}}
{{--            <div class="form-control-position">--}}
{{--                <i class="bx bx-calendar"></i>--}}
{{--            </div>--}}
{{--        </fieldset>--}}
{{--                @error('insurance_validity_date')--}}
{{--                <span class="invalid-feedback" role="alert">--}}
{{--                    <strong class="text-danger">{{ $message }}</strong>--}}
{{--                </span>--}}
{{--                @enderror--}}
{{--            </fieldset>--}}
{{--        </div>--}}

        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="description_customer"> توضیحات </label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        <textarea class="form-control char-textarea" maxlength="190" name="description_customer"
                                  id="description_customer" rows="3"
                                  placeholder="توضیحات "></textarea>
                    </fieldset>
                    <small class="counter-value float-right"><span class="char-count">0</span> / 190 </small>
                </div>
                <div>
                    @error('description_customer')
                    <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
            <hr>
        </div>

        <div class="col-md-6">
            <div class="card-header">
                <h4 class="card-title">افزودن نسخه</h4>
            </div>
            <fieldset class="form-group col-md-12">
                <input type="file" name="file" id="file" class="custom-file-input" accept="image/jpeg, image/jpg">
                <label class="custom-file-label" for="file">عکس نسخه را انتخاب کنید</label>
                @error('file')
                <span role="alert">
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                @enderror
            </fieldset>
        </div>

    </div>
</div>

