<div class="card-body">
    <div class="row">
        <div class="col-md-12">
            <fieldset class="form-group">
                <div class="table-responsive">
{{--                                                    <table class="table table-striped dataex-html5-selectors">--}}
                    <table class="table table-striped table-bordered complex-headers">
{{--                    <table class="table nowrap scroll-horizontal-vertical">--}}
                        <thead>
                            <tr>
                                <th>
                                    نام بیمار
                                </th>
                                <th>
                                    نام دکتر
                                </th>
                                <th>
                                    تاریخ نسخه
                                </th>
                                <th>
                                    لیست داروهای موجود
                                </th>
                                <th>
                                    توضیحات داروخانه
                                </th>
                                <th>
                                    قیمت
                                </th>
                                <th>
                                    عملیات
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse($prescriptions as $prescription)
                                <tr>
                                    <td>{{$prescription->sick_name ?? ''}}</td>
                                    <td>{{$prescription->doctor_name ?? ''}}</td>
                                    <td>{{$prescription->date ?? ''}}</td>
                                    <td>
                                        @foreach($prescription->products as $product)
                                            {{$product->name ?? ''}}<br>
                                        @endforeach

                                    </td>
                                    <td>{{$prescription->description_employee ?? ''}}</td>

                                    <td>{{$prescription->price ?? ''}}</td>

                                    <td>
                                        <a href="{{route('basket.add', ['product' => 0 ,'prescription' => $prescription->id, 'seller' => $prescription->seller()->where('id',$prescription->seller_id)->first()->id])}}" class="btn btn-sm btn-primary">
                                            پرداخت
                                        </a>
                                        <br>
                                        <a class="btn btn-sm btn-danger" href="{{route('manegment.users.prescriptions.cancel' ,['prescription' => $prescription->id])}}">
                                                لفو
                                        </a>
                                    </td>
                                </tr>
                        @empty
                            <tr>
                                <td>موردی یافت نشد.</td>
                            </tr>
                        @endforelse

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>
                                    کد نسخه
                                </th>
                                <th>
                                    نام بیمار
                                </th>
                                <th>
                                    نام دکتر
                                </th>
                                <th>
                                    تاریخ نسخه
                                </th>
                                <th>
                                    توضیحات داروخانه
                                </th>
                                <th>
                                    قیمت
                                </th>
                                <th>
                                    عملیات
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </fieldset>
        </div>


    </div>
</div>

