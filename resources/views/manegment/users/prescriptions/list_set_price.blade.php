@extends('manegment.dashboard.users.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/manegment/dashbord/users/vendors/css/forms/select/select2.min.css')}}">

    <!-- END: Page CSS-->
@endpush

@section('title', 'نسخه')
@section('title_page', 'لیست نسخه های قیمت گذاری شده')

@section('content')
    <!-- Basic Inputs start -->
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title"> لیست نسخه های قیمت گذاری شده </h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="#" enctype="multipart/form-data">
                            @csrf
                            @include('manegment.users.prescriptions.partials._form_list_set_price')
{{--                            <div class="col-sm-12 d-flex justify-content-end">--}}
{{--                                <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت</button>--}}
{{--                            </div>--}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Inputs end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->

    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
    {{--    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>--}}
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/form-tooltip-valid.js')}}"></script>

    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/datatables/datatable.js')}}"></script>
    <!-- END: Page JS-->
@endpush

@push('before-body-end')

@endpush
