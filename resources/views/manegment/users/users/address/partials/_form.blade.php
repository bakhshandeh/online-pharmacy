<div class="card-body">
    <div class="row">
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="recipient_first_name">نام گیرنده</label>
                <input type="text" class="form-control @error('recipient_first_name') is-invalid @enderror"
                       name="recipient_first_name" id="recipient_first_name" value="{{ $addressUser->recipient_first_name ?? old('recipient_first_name') }}"
                       placeholder="نام گیرنده را وارد کنید"
                       @error('recipient_first_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="recipient_last_name">نام خانوادگی گیرنده</label>
                <input type="text" class="form-control @error('recipient_last_name') is-invalid @enderror"
                       name="recipient_last_name" id="recipient_last_name"
                       value="{{ $addressUser->recipient_last_name ?? old('recipient_last_name') }}"
                       placeholder="نام خانوادگی گیرنده  را وارد کنید"
                       @error('recipient_last_name')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="recipient_phone_number">تلفن گیرنده</label>
                <input type="text" class="form-control @error('recipient_phone_number') is-invalid @enderror"
                       name="recipient_phone_number" id="recipient_phone_number"
                       value="{{ $addressUser->recipient_phone_number ?? old('recipient_phone_number') }}"
                       placeholder="تلفن گیرنده را وارد کنید"
                       @error('recipient_phone_number')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="recipient_national_code">کدملی گیرنده</label>
                <input type="text" class="form-control @error('recipient_national_code') is-invalid @enderror"
                       name="recipient_national_code" id="recipient_national_code"
                       value="{{ $addressUser->recipient_national_code ?? old('recipient_national_code') }}"
                       placeholder="کدملی گیرنده را وارد کنید"
                       @error('recipient_national_code')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="province_id">استان</label>
                <div class="form-group">
                    <select name="province_id" id="province_id" data-placeholder="انتخاب استان..." class="select2-theme form-control @error('province_id') is-invalid @enderror" >
                        <optgroup label="اسامی استانها">
                            @forelse($provinces as $province)
                            <option
                                name="{{$province->name ?? ''}}"
                                value="{{$province->id ?? ''}}"
                                {{old('province') == $province->id ? 'selected' : ''}}
                            >
                            {{$province->name ?? ''}}

                            </option>
                            @empty
                            @endforelse
                        </optgroup>
                    </select>
                </div>
                <div>
                    @error('province_id')
                    <span role="alert" >
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

            </fieldset>

        </div>
{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="city_id">شهر</label>--}}
{{--                <div class="form-group">--}}
{{--                    <select name="city_id" id="city_id" data-placeholder="انتخاب شهر..." class="select2-theme form-control @error('city_id') is-invalid @enderror" >--}}
{{--                        <optgroup label="اسامی شهرها">--}}
{{--                            @forelse($cities as $city)--}}
{{--                                <option--}}
{{--                                    name="{{$city->name ?? ''}}"--}}
{{--                                    value="{{$city->id ?? ''}}"--}}
{{--                                    {{old('city') == $city->id ? 'selected' : ''}}--}}
{{--                                >--}}
{{--                                    {{$city->name ?? ''}}--}}
{{--                                </option>--}}
{{--                            @empty--}}
{{--                            @endforelse--}}
{{--                        </optgroup>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    @error('city_id')--}}
{{--                    <span role="alert" >--}}
{{--                        <strong class="text-danger">{{ $message }}</strong>--}}
{{--                    </span>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--            </fieldset>--}}

{{--        </div>--}}

{{--        <div class="col-md-6">--}}
{{--            <fieldset class="form-group">--}}
{{--                <label for="quarter_id">بخش</label>--}}
{{--                <div class="form-group">--}}
{{--                    <select name="quarter_id" id="quarter_id" data-placeholder="انتخاب بخش..." class="select2-theme form-control @error('quarter_id') is-invalid @enderror" >--}}
{{--                        <optgroup label="اسامی بخشها">--}}
{{--                            @forelse($quarters as $quarter)--}}
{{--                            <option--}}
{{--                                name="{{$quarter->name ?? ''}}"--}}
{{--                                value="{{$quarter->id ?? ''}}"--}}
{{--                                {{old('quarter') == $quarter->id ? 'selected' : ''}}--}}

{{--                            >--}}
{{--                            {{$quarter->name ?? ''}}--}}

{{--                            </option>--}}
{{--                            @empty--}}
{{--                            @endforelse--}}
{{--                        </optgroup>--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--                <div>--}}
{{--                    @error('quarter_id')--}}
{{--                    <span role="alert" >--}}
{{--                        <strong class="text-danger">{{ $message }}</strong>--}}
{{--                    </span>--}}
{{--                    @enderror--}}
{{--                </div>--}}

{{--            </fieldset>--}}
{{--        </div>--}}

        <div class="col-md-6">
            <fieldset class="form-group">
                <label for="postal_code">کدپستی</label>
                <input type="text" class="form-control @error('postal_code') is-invalid @enderror"
                       name="postal_code" id="postal_code" value="{{ $addressUser->postal_code ?? old('postal_code') }}"
                       placeholder="کدپستی راوارد کنید."
                       @error('postal_code')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

        <div class="col-md-12">
            <fieldset class="form-group col-md-12">
                <label class="control-label" for="street_alley"> آدرس دقیق خیابان و کوچه</label>
                <div class="col-12">
                    <fieldset class="form-label-group mb-0">
                        <textarea data-length="200" class="form-control char-textarea" name="street_alley" id="street_alley" rows="3" placeholder="آدرس دقیق خیابان و کوچه"></textarea>
                    </fieldset>
                    <small class="counter-value float-right"><span class="char-count">0</span> / 200 </small>
                </div>
                <div>
                    @error('street_alley')
                    <span role="alert" >
                        <strong class="text-danger">{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </fieldset>
        </div>

        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="no">پلاک</label>
                <input type="text" class="form-control @error('no') is-invalid @enderror"
                       name="no" id="no" value="{{ $addressUser->no ?? old('no') }}"
                       placeholder="پلاک راوارد کنید."
                       @error('no')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="floor">طبقه</label>
                <input type="text" class="form-control @error('floor') is-invalid @enderror"
                       name="floor" id="floor" value="{{ $addressUser->floor ?? old('floor') }}"
                       placeholder="طبقه راوارد کنید."
                       @error('floor')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>
        <div class="col-md-4">
            <fieldset class="form-group">
                <label for="unit">واحد</label>
                <input type="text" class="form-control @error('unit') is-invalid @enderror"
                       name="unit" id="unit" value="{{ $addressUser->unit ?? old('unit') }}"
                       placeholder="واحد راوارد کنید."
                       @error('unit')
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </fieldset>
        </div>

    </div>
</div>

