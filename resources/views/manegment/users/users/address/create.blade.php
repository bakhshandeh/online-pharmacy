@extends('manegment.dashboard.users.layouts.panel')

@push('styles')
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/manegment/dashbord/users/vendors/css/forms/select/select2.min.css')}}">
    <!-- END: Page CSS-->
@endpush

@section('title', 'درج آدرس')

@section('content')
    <section id="basic-input">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">افزودن آدرس جدید</h4>
                    </div>
                    <div class="card-content">
                        <form method="post" action="{{route('manegment.users.users.address.store')}}" >
                            @csrf
                            <input type="hidden" name="backReferer" id="backReferer" value="{{$backReferer}}">
                            @include('manegment.users.users.address.partials._form')
                            <div class="col-sm-12 d-flex justify-content-end">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">ثبت آدرس</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
    <script src="{{asset('assets/manegment/dashbord/users/vendors/js/forms/select/select2.full.min.js')}}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{asset('assets/manegment/dashbord/users/js/scripts/forms/select/form-select2.js')}}"></script>
    <!-- END: Page JS-->

@endpush

@push('before-body-end')

@section('script')
{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function () {--}}
{{--            $('select[name="province_id"]').on('change', function () {--}}
{{--                var provinceID = $(this).val();--}}
{{--                if (provinceID) {--}}
{{--                    $.ajax({--}}
{{--                        url: '{{route('get.city')}}',--}}
{{--                        type: "GET",--}}
{{--                        data:{province_id:province_id},--}}
{{--                        dataType: "json",--}}
{{--                        success: function (data) {--}}
{{--                            $("city_id").empty();--}}
{{--                            // $.each(data, function (key, value) {--}}
{{--                            //     $("city_id").append('<option value="' + value.id + '>' + value.name + '</option>');--}}
{{--                            // });--}}
{{--                        }--}}
{{--                    });--}}
{{--                } else {--}}
{{--                    $("city_id").empty();--}}
{{--                }--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}

@endpush
