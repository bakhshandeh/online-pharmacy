@extends('manegment.dashboard.users.layouts.panel')

@push('styles')
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/vendors/css/pickers/pickadate/pickadate.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/vendors/css/pickers/daterange/daterangepicker.css') }}">--}}
{{--    <link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css') }}">--}}
@endpush

@section('title', 'نویسنده')

@section('content')
    <!-- Shamsi Date Picker start -->
{{--    <section id="shamsi-pick-a-date">--}}
{{--        <div class="card">--}}
{{--            <div class="card-body">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12">--}}
{{--                        <h4 class="card-title">انتخابگر تاریخ شمسی</h4>--}}
{{--                        <p class="mb-2">پیاده سازی پایه نیازمند هدف قرار دادن یک <code>input</code> و فراخوانی انتخاب گر است.</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-6">--}}
{{--                        <div class="mb-1">--}}
{{--                            <h6>انتخاب گر تاریخ ساده</h6>--}}
{{--                            <p>از <code>.shamsi-datepicker</code> برای انتخاب گر پایه استفاده کنید.</p>--}}
{{--                            <fieldset class="form-group position-relative has-icon-left">--}}
{{--                                <input type="text" class="form-control shamsi-datepicker" placeholder="انتخاب تاریخ">--}}
{{--                                <div class="form-control-position">--}}
{{--                                    <i class="bx bx-calendar"></i>--}}
{{--                                </div>--}}
{{--                            </fieldset>--}}
{{--                        </div>--}}
{{--                        <div class="mb-1">--}}
{{--                            <h6>انتخاب گر تاریخ با نمایش لیست</h6>--}}
{{--                            <p>از <code>.shamsi-datepicker-list</code> برای انتخاب گر با نمایش لیست انتخاب ماه و سال استفاده کنید.</p>--}}
{{--                            <fieldset class="form-group position-relative has-icon-left">--}}
{{--                                <input type="text" class="form-control shamsi-datepicker-list" placeholder="انتخاب تاریخ">--}}
{{--                                <div class="form-control-position">--}}
{{--                                    <i class="bx bx-calendar"></i>--}}
{{--                                </div>--}}
{{--                            </fieldset>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6">--}}
{{--                        <div class="mb-1">--}}
{{--                            <h6>انتخاب گر تاریخ محدود</h6>--}}
{{--                            <p>از <code>.shamsi-datepicker-limited</code> برای انتخاب گر محدود به 14 روز استفاده کنید.</p>--}}
{{--                            <fieldset class="form-group position-relative has-icon-left">--}}
{{--                                <input type="text" class="form-control shamsi-datepicker-limited" placeholder="انتخاب تاریخ">--}}
{{--                                <div class="form-control-position">--}}
{{--                                    <i class="bx bx-calendar"></i>--}}
{{--                                </div>--}}
{{--                            </fieldset>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- Shamsi Date Picker end -->
@endsection
@push('scripts')
    <!-- BEGIN: Page Vendor JS-->
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/picker.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/picker.date.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/picker.time.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/pickadate/legacy.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/daterange/moment.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/daterange/daterangepicker.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js') }}"></script>--}}
{{--    <script src="{{ asset('assets/manegment/dashbord/users/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js') }}"></script>--}}
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
{{--    <script src="{{ asset('assets/manegment/dashbord/users/js/scripts/pickers/dateTime/pick-a-datetime.js') }}"></script>--}}
    <!-- END: Page JS-->

@endpush

@push('before-body-end')
    <!-- Page-Level Scripts -->


@endpush
