<!-- BEGIN: Vendor JS-->
<script src="{{ asset('assets/manegment/dashbord/users/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('assets/manegment/dashbord/users/js/core/app-menu.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/core/app.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/components.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/footer.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/customizer.js') }}"></script>
<!-- END: Theme JS-->


@stack('scripts')
