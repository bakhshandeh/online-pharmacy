<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body" id="content-body">

            <!-- Dashboard  Start -->
            @include('flash::message')
            @yield('content')
            <!-- Dashboard  end -->

        </div>
    </div>
</div>
