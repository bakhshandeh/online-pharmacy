<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="#">
                    <div class="brand-logo">
                        <img class="logo" src="{{ asset('assets/manegment/dashbord/users/images/logo/logo.png') }}">
                    </div>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                    <i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="bx-disc"></i>
                </a>
            </li>
            <li class="nav-item mr-auto">
                <h2 class="brand-text mb-0">
                    <a class="navbar-brand" href="#">
                        {{ Auth::user()->first_name.' '.Auth::user()->last_name}}
                    </a>
                </h2>
            </li>

        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="">
            <li class=" nav-item">
                <a href="#">
{{--                    <i class="bx bx-wrench"></i>--}}
{{--                    <span class="menu-title" data-i18n="Account Settings">مدیریت</span>--}}
                </a>
            </li>
            @role('admin project')

            <li class=" navigation-header">
                <span>مدیریت کاربران</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">مدیریت سطح دسترسی</span></a>
                <ul class="menu-content">
                    <li>
                        <a href="{{route('Manegment.UsersAdmin.permissions.create')}}">
                            <i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">افزودن سطح دسترسی </span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">مدیریت نقش</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.roles.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                  data-i18n="Invoice List">لیست نقش ها </span></a>
                    </li>
                    <li><a href="{{route('Manegment.UsersAdmin.roles.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                 data-i18n="Invoice Add">افزودن نقش جدید</span></a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">مدیریت کاربران</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.users.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                  data-i18n="Invoice List">لیست کاربران</span></a>
                    </li>

                </ul>
            </li>

{{--            <li class=" navigation-header">--}}
{{--                <span>اطلاع رسانی</span>--}}
{{--            </li>--}}
{{--            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">مدیریت ایمیل</span></a>--}}
{{--                <ul class="menu-content">--}}
{{--                    <li><a href="{{route('Manegment.UsersAdmin.notification.form.mail')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                  data-i18n="Invoice List">ارسال ایمیل</span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">مدیریت پیام کوتاه</span></a>--}}
{{--                <ul class="menu-content">--}}
{{--                    <li><a href="{{route('Manegment.UsersAdmin.notification.form.sms')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                  data-i18n="Invoice List">ارسال پیام کوتاه</span></a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--            </li>--}}


            @endrole
            <li class=" navigation-header">
                <span>مدیریت محصولات</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">محصولات</span></a>
                <ul class="menu-content">
{{--                    <li><a href="{{route('Manegment.UsersAdmin.products.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                  data-i18n="Invoice List">لیست محصولات</span></a>--}}
{{--                    </li>--}}
                    @can('Increase inventory of products')

                    <li><a href="{{route('Manegment.UsersAdmin.products.add-inventory.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                                              data-i18n="Invoice Add">افزودن موجودی محصولات</span></a>
                    </li>
                    @endcan
                    @role('admin project')

                    <li><a href="{{route('Manegment.UsersAdmin.products.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                                              data-i18n="Invoice Add">افزودن محصول جدید</span></a>
                    </li>
                    @endrole
                </ul>
            </li>
            @role('admin project')

            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">دسته بندی محصولات</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.products.categories.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                  data-i18n="Invoice List">لیست دسته بندی</span></a>
                    </li>
{{--                    <li><a href="{{route('Manegment.UsersAdmin.products.categories.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                 data-i18n="Invoice Add">افزودن دسته بندی جدید</span></a>--}}
{{--                    </li>--}}
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">ویژگی محصولات</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.products.properties.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                  data-i18n="Invoice List">لیست ویژگی</span></a>
                    </li>
                    <li><a href="{{route('Manegment.UsersAdmin.products.properties.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                 data-i18n="Invoice Add">افزودن ویژگی جدید</span></a>
                    </li>
                </ul>
            </li>

            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">نوع محصولات</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.products.types.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                  data-i18n="Invoice List">لیست انواع </span></a>
                    </li>
                    <li><a href="{{route('Manegment.UsersAdmin.products.types.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                 data-i18n="Invoice Add">افزودن نوع جدید</span></a>
                    </li>
                </ul>
            </li>


{{--            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">فایل محصولات</span></a>--}}
{{--                <ul class="menu-content">--}}
{{--                    <li><a href="{{route('Manegment.UsersAdmin.products.files.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                                                        data-i18n="Invoice List">لیست فایل</span></a>--}}
{{--                    </li>--}}
{{--                    <li><a href="{{route('Manegment.UsersAdmin.products.files.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                                                    data-i18n="Invoice Add">افزودن فایل جدید</span></a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}


            <li class=" navigation-header">
                <span>مدیریت سفارشات</span>
            </li>

            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">سفارش</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.accounting.payments.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                                                  data-i18n="Invoice List">لیست سفارش های نهایی شده</span></a>
                    </li>
                </ul>
            </li>
            @endrole
            @can('Viewing list prescriptions allowed')

            <li class=" navigation-header">
                <span>مدیریت نسخه</span>
            </li>

            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">نسخه ها</span></a>
                <ul class="menu-content">
                    <li><a href="{{route('Manegment.UsersAdmin.prescriptions.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                                             data-i18n="Invoice List">لیست نسخه های قیمت گذاری نشده</span></a>
                    </li>
{{--                    <li><a href="{{route('manegment.users.prescriptions.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                                              data-i18n="Invoice Add">افزودن نسخه جدید</span></a>--}}
{{--                    </li>--}}

                    {{--                    <li><a href="{{route('Manegment.UsersAdmin.products.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
                    {{--                                                                                                                              data-i18n="Invoice Add">افزودن محصول جدید</span></a>--}}
                    {{--                    </li>--}}
                </ul>
            </li>
            @endcan
            @role('admin project')

            <li class=" navigation-header">
                <span>مدیریت فروشندگان</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice"> فروشندگان</span></a>
                <ul class="menu-content">
                    <li>
                        <a href="{{route('Manegment.UsersAdmin.products.sellers.index')}}">
                            <i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">لیست فروشندگان </span>
                        </a>
                    </li>
{{--                    <li>--}}
{{--                        <a href="{{route('Manegment.UsersAdmin.products.sellers.create')}}">--}}
{{--                            <i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">افزودن فروشنده جدید </span>--}}
{{--                        </a>--}}
                    </li>
                </ul>
            </li>


            <li class=" navigation-header">
                <span>مدیریت تولید کنندگان</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice"> تولید کنندگان</span></a>
                <ul class="menu-content">
                    <li>
                        <a href="{{route('Manegment.UsersAdmin.products.manufacturers.index')}}">
                            <i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">لیست تولید کنندگان </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{route('Manegment.UsersAdmin.products.manufacturers.create')}}">
                            <i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="Invoice List">افزودن تولید کننده جدید </span>
                        </a>
                    </li>
                </ul>
            </li>

            @endrole



        </ul>
    </div>
</div>
