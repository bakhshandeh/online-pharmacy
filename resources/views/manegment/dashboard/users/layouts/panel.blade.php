<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl" dir="rtl">
<!-- BEGIN: Head-->
<head>
    @include('manegment.dashboard.users.partials.head')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

<!-- BEGIN: Header-->
@include('manegment.dashboard.users.partials.navigation')
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
@include('manegment.dashboard.users.partials.sidebar')
<!-- END: Main Menu-->

<!-- BEGIN: Content-->
@include('manegment.dashboard.users.partials.main')
<!-- END: Content-->

<!-- BEGIN: Footer-->
@include('manegment.dashboard.users.partials.footer')

<!-- END: Footer-->

<!-- Mainly scripts -->
@include('manegment.dashboard.users.partials.scripts')
@stack('before-body-end')
</body>
<!-- END: Body-->
</html>
