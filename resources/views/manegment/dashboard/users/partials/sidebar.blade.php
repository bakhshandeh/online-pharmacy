<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="#">
                    <div class="brand-logo">
                        <img class="logo" src="{{ asset('assets/manegment/dashbord/users/images/logo/logo.png') }}">
                    </div>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                    <i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="bx-disc"></i>
                </a>
            </li>
            <li class="nav-item mr-auto">
                <h2 class="brand-text mb-0">
                    <a class="navbar-brand" href="#">
                        {{ Auth::user()->first_name.' '.Auth::user()->last_name}}
                    </a>
                </h2>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="">
            <li class=" nav-item">
                <a href="#">
{{--                    <i class="bx bx-wrench"></i>--}}
{{--                    <span class="menu-title" data-i18n="Account Settings">تنظیمات حساب کاربری</span>--}}
                </a>
            </li>

            <li class=" navigation-header">
                <span>مدیریت پروفایل</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">آدرس ها</span></a>
                <ul class="menu-content">
                    {{--                    <li><a href="{{route('Manegment.UsersAdmin.products.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
                    {{--                                                                                                  data-i18n="Invoice List">لیست محصولات</span></a>--}}
{{--                    </li>--}}
                    <li><a href="{{route('manegment.users.users.address.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                                                            data-i18n="Invoice Add">افزودن آدرس جدید</span></a>
                    </li>

{{--                    <li><a href="{{route('Manegment.UsersAdmin.products.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
{{--                                                                                                                              data-i18n="Invoice Add">افزودن محصول جدید</span></a>--}}
{{--                    </li>--}}
                </ul>
            </li>

            <li class=" navigation-header">
                <span>مدیریت نسخه</span>
            </li>
            <li class=" nav-item"><a href="#"><i class="bx bx-file"></i><span class="menu-title" data-i18n="Invoice">نسخه ها</span></a>
                <ul class="menu-content">
                    {{--                    <li><a href="{{route('Manegment.UsersAdmin.products.index')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"--}}
                    {{--                                                                                                  data-i18n="Invoice List">لیست محصولات</span></a>--}}
                    {{--                    </li>--}}
                    <li><a href="{{route('manegment.users.prescriptions.create')}}"><i class="bx bx-left-arrow-alt"></i><span class="menu-item"
                                                                                                                              data-i18n="Invoice Add">افزودن نسخه جدید</span></a>
                    </li>

                    <li>
                        <a href="{{route('manegment.users.prescriptions.list.set.price')}}">
                            <i class="bx bx-left-arrow-alt"></i>
                            <span class="menu-item" data-i18n="Invoice Add"> قیمت گذاری شده</span>
                        </a>
                    </li>
                </ul>
            </li>

            @can('viewing the admin page is allowed')
                <li class=" navigation-header">
                    <span>مدیریت</span>
                </li>
{{--            @role('Allow login to admin panel')--}}
                <li class=" nav-item">
                    <a href="#">
                        <i class="bx bx-dashboard"></i>
                        <span class="menu-title" data-i18n="Dashboard">پنل مدیریت</span>
                        {{--                    <span class="badge badge-light-danger badge-pill badge-round float-right mr-2">2</span>--}}
                    </a>
                    <ul class="menu-content">
                        <li>
                            <a href="{{route('Manegment.UsersAdmin.dashboard')}}">
                                <i class="bx bx-left-arrow-alt"></i>
                                <span class="menu-item" data-i18n="eCommerce">ورود به پنل مدیریت</span>
                            </a>
                        </li>

                    </ul>
                </li>
{{--            @endrole--}}
            @endcan

        </ul>
    </div>
</div>
