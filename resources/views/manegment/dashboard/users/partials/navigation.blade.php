<div class="header-navbar-shadow"></div>
<nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top ">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">

                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                    class="ficon bx bx-menu"></i></a></li>
                    </ul>
                </div>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
                                                                   href="#" data-toggle="dropdown">
                            <div class="user-nav d-sm-flex d-none"><span class="user-name">{{ Auth::user()->first_name.' '.Auth::user()->last_name}}</span><span
                                    class="user-status text-muted">{{Auth::user()->roles()->first()->persian_name ?? 'مهمان'}}</span></div>
                            <span><img class="round"
                                       src="{{ asset('assets/manegment/dashbord/users/images/portrait/small/avatar-s-11.jpg') }}"
                                       alt="avatar" height="40" width="40"></span></a>
                        <div class="dropdown-menu pb-0">
{{--                            <a class="dropdown-item" href="{{route('manegment.users.dashboard')}}">--}}
{{--                                <i class="bx bx-user mr-50"></i>--}}
{{--                                پروفایل کاربری--}}
{{--                            </a>--}}
                            <a class="dropdown-item" href="{{route('home')}}">
                                <i class="bx bx-user mr-50"></i>
                                صفحه اصلی
                            </a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="bx bx-power-off mr-50"></i>
                                خروج
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
