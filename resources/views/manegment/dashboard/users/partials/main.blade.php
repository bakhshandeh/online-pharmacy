<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">

            <!-- Dashboard  Start -->
            <section id="dashboard">
                @include('flash::message')
                @yield('content')
            </section>
            <!-- Dashboard  end -->

        </div>
    </div>
</div>
