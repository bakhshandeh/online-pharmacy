<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
<title>@section('title'){{ config('app.name', 'Laravel') }}@show</title>
<link rel="shortcut icon" type="image/x-icon"
      href="{{ asset('assets/manegment/dashbord/users/images/ico/favicon.ico') }}">
<meta name="theme-color" content="#5A8DEE">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}"/>

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/manegment/dashbord/users/vendors/css/vendors.min.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/manegment/dashbord/users/vendors/css/charts/apexcharts.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/manegment/dashbord/users/vendors/css/extensions/dragula.min.css') }}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/css/bootstrap-extended.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/css/colors.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/css/components.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/manegment/dashbord/users/css/themes/dark-layout.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/manegment/dashbord/users/css/themes/semi-dark-layout.css') }}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/manegment/dashbord/users/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/manegment/dashbord/users/css/pages/dashboard-analytics.css') }}">
<!-- END: Page CSS-->

@stack('styles')
