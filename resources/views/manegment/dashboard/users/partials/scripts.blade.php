<!-- BEGIN: Vendor JS-->
<script src="{{ asset('assets/manegment/dashbord/users/vendors/js/vendors.min.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/fonts/LivIconsEvo/js/LivIconsEvo.min.js') }}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('assets/manegment/dashbord/users/vendors/js/charts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/vendors/js/extensions/dragula.min.js') }}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('assets/manegment/dashbord/users/js/core/app-menu.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/core/app.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/components.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/footer.js') }}"></script>
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/customizer.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{ asset('assets/manegment/dashbord/users/js/scripts/pages/dashboard-analytics.js') }}"></script>
<!-- END: Page JS-->

@stack('scripts')
