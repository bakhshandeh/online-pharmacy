<footer class="site__footer">
    <div class="site-footer">
        <div class="container">
            <div class="site-footer__widgets">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="site-footer__widget footer-contacts">
                            <h5 class="footer-contacts__title">تاسیس داروخانه
                            </h5>
                            <div class="footer-contacts__text">
                                داروخانه با بیش از 50 سال سابقه فعالیت عرضه داروهای تخصصی و همچنین فروش لوازم آرایشی، بهداشتی مورد تأیید وزارت بهداشت و مو، ناخن و زیبائی یکی از معدود داروخانه های پیشرو در ارائه خدمات نوین بمنظور ارتقاء سطح بهداشت و سلامت جامعه می باشد ، همچنین ساخت نسخ ترکیبی پوست و مو با مواد اولیه با کیفیت بالا یکی از زمینه های عرضه خدمات داروخانه می باشد.


                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3 col-lg-2">
                        <div class="site-footer__widget footer-links">
                            <h5 class="footer-links__title">آدرس داروخانه
                            </h5>
                            <ul class="footer-links__list">
                                <li class="footer-links__item"><a href="" class="footer-links__link">
                                        قم - باجک -کوچه45
                                    </a></li>
                                <li class="footer-links__item"><a href="" class="footer-links__link">
                                        تلفن : 0253 -1223456
                                    </a></li>
                                <li class="footer-links__item"><a href="" class="footer-links__link">
                                        ایمیل : b@a.com
                                    </a></li>
                            </ul>
                            <div>
                                <img src="{{asset('assets/them/images/logos/logo.png')}}" class="img-sm" width="150px" height="100px">

                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-12 col-lg-4">
                        <div class="site-footer__widget footer-newsletter">
                            <h5 class="footer-newsletter__title">خبرنامه</h5>
                            <div class="footer-newsletter__text">
                                سازمان جهانی بهداشت اعلام کرده آزمایش ۳ داروی هیدروکسی کلروکین، لوپیناویر و ریتوناویر برای درمان بیماران مبتلا به کووید۱۹ در بیمارستان را متوقف کرده است.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="site-footer__bottom">
{{--                <div class="site-footer__copyright">ارائه شده در وب‌سایت <a href="https://www.rtl-theme.com"--}}
{{--                                                                            target="_blank">راست‌چین</a></div>--}}
{{--                <div class="site-footer__payments"><img src="{{ asset('assets/them/images/payments.png')}}" alt="">--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</footer>
