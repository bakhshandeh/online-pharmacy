<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1">
<meta name="format-detection" content="telephone=no">
<title>@section('title'){{ config('app.name', 'Laravel') }}@show</title>
<link rel="icon" type="image/png" href="{{ asset('assets/them/images/favicon.png')}}">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}"/>

<!-- css -->
<link rel="stylesheet" href="{{ asset('assets/them/vendor/bootstrap-4.2.1/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/them/vendor/owl-carousel-2.3.4/assets/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/them/vendor/lightgallery-1.6.12/css/lightgallery.min.css')}}">
<link rel="stylesheet" href="{{ asset('assets/them/css/style.css')}}">
<link rel="stylesheet" href="{{ asset('assets/them/css/custom.css')}}">
<!-- js -->
<script src="{{ asset('assets/them/vendor/jquery-3.3.1/jquery.min.j')}}s"></script>
<script src="{{ asset('assets/them/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assets/them/vendor/owl-carousel-2.3.4/owl.carousel.min.js')}}"></script>
<script src="{{ asset('assets/them/vendor/nouislider-12.1.0/nouislider.min.js')}}"></script>
<script src="{{ asset('assets/them/vendor/lightgallery-1.6.12/js/lightgallery.min.js')}}"></script>
<script src="{{ asset('assets/them/vendor/lightgallery-1.6.12/js/lg-thumbnail.min.js')}}"></script>
<script src="{{ asset('assets/them/vendor/lightgallery-1.6.12/js/lg-zoom.min.js')}}"></script>
<script src="{{ asset('assets/them/js/number.js')}}"></script>
<script src="{{ asset('assets/them/js/main.js')}}"></script>
<script src="{{ asset('assets/them/vendor/svg4everybody-2.1.9/svg4everybody.min.js')}}"></script>
<script>
    svg4everybody();
</script>
<!-- font - fontawesome -->
<link rel="stylesheet" href="{{ asset('assets/them/vendor/fontawesome-5.6.3/css/all.min.css')}}">
<!-- font - stroyka -->
<link rel="stylesheet" href="{{ asset('assets/them/fonts/stroyka/stroyka.css')}}">

@stack('styles')
