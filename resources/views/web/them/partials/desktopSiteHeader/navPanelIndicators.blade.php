@inject('basket','App\Support\Basket\Basket')
<div class="nav-panel__indicators">
    {{--    <div class="indicator"><a href="" class="indicator__button"><span class="indicator__area"><svg width="20px" height="20px"><use xlink:href="{{ asset('assets/them/images/sprite.svg#heart-20')}}"></use></svg> <span class="indicator__value">0</span></span></a></div>--}}
    <div class="indicator indicator--trigger--click"><a href="" class="indicator__button"><span class="indicator__area"><svg
                    width="20px" height="20px"><use
                        xlink:href="{{ asset('assets/them/images/sprite.svg#cart-20')}}"></use></svg> <span
                    class="indicator__value">{{$basket->itemCount()}}</span></span></a>
        <div class="indicator__dropdown">
            <!-- .dropcart -->
        @include('web.them.partials.dropcart')
        <!-- .dropcart / end -->
        </div>
    </div>
</div>
