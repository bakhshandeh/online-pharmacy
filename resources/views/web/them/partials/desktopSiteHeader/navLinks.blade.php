<div class="nav-panel__nav-links nav-links">
    <ul class="nav-links__list">
        <li class="nav-links__item"><a href="{{route('products.index',['category' => 3])}}"><span>محصولات آرایشی</span></a></li>
{{--        <li class="nav-links__item"><a href="{{route('products.index',['category' => 4])}}"><span>دارو</span></a></li>--}}
{{--        <li class="nav-links__item"><a href="{{route('products.index',['category' => 2])}}"><span>دارو های بدون نسخه</span></a></li>--}}
{{--        <li class="nav-links__item"><a href="{{route('products.index',['category' => 1])}}"><span> دارو های با نسخه</span></a></li>--}}


        <li class="nav-links__item nav-links__item--with-submenu"><a href="{{route('products.index',['category' => 4])}}"><span>دارو <svg class="nav-links__arrow" width="9px" height="6px"><use xlink:href="images/sprite.svg#arrow-rounded-down-9x6"></use></svg></span></a>
            <div class="nav-links__menu">
                <!-- .menu -->
                <ul class="menu menu--layout--classic">
                    <li><a href="{{route('products.index',['category' => 2])}}">دارو های بدون نسخه</a></li>
                    <li><a href="{{route('products.index',['category' => 1])}}">دارو های با نسخه</a></li>
                </ul>
                <!-- .menu / end -->
            </div>
        </li>


        <li class="nav-links__item"><a href="{{route('manegment.users.prescriptions.create')}}"><span> خرید دارو های با نسخه</span></a></li>
    </ul>
</div>







