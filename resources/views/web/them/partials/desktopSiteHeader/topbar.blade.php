<div class="site-header__topbar topbar">
    <div class="topbar__container container">
        <div class="topbar__row">
{{--            <div class="topbar__item topbar__item--link"><a class="topbar-link" href="about-us.html">درباره ما</a></div>--}}
{{--            <div class="topbar__item topbar__item--link"><a class="topbar-link" href="contact-us.html">تماس</a></div>--}}
{{--            <div class="topbar__item topbar__item--link"><a class="topbar-link" href="">مکان فروشگاه</a></div>--}}
{{--            <div class="topbar__item topbar__item--link"><a class="topbar-link" href="track-order.html">پیگیری سفارش</a></div>--}}
{{--            <div class="topbar__item topbar__item--link"><a class="topbar-link" href="blog-classic.html">بلاگ</a></div>--}}
            <div class="topbar__spring"></div>
            <div class="topbar__item">
                <div class="topbar-dropdown">
                    <button class="topbar-dropdown__btn" type="button">حساب کاربری
                        <svg width="7px" height="5px">
                            <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>
                        </svg>
                    </button>
                    <div class="topbar-dropdown__body">
                        <!-- .menu -->
                        <ul class="menu menu--layout--topbar">
                            @if(Auth::check())
                                <li><a href="{{route('manegment.users.dashboard')}}">داشبورد</a></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="bx bx-power-off mr-50"></i>
                                        خروج
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @else
                                <li><a href="{{route('login')}}">ورود</a></li>
                                <li><a href="{{route('register')}}">ثبت نام</a></li>
                            @endif
                        </ul>
                        <!-- .menu / end -->
                    </div>
                </div>
            </div>
            <div class="topbar__item">
{{--                <div class="topbar-dropdown">--}}
{{--                    <button class="topbar-dropdown__btn" type="button">واحد پول: <span class="topbar__item-value">تومان</span>--}}
{{--                        <svg width="7px" height="5px">--}}
{{--                            <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>--}}
{{--                        </svg>--}}
{{--                    </button>--}}
{{--                    <div class="topbar-dropdown__body">--}}
{{--                        <!-- .menu -->--}}
{{--                        <ul class="menu menu--layout--topbar">--}}
{{--                            <li><a href="">€ یورو</a></li>--}}
{{--                            <li><a href="">£ پوند</a></li>--}}
{{--                            <li><a href="">$ دلار آمریکا</a></li>--}}
{{--                            <li><a href="">₽ روبل روسیه</a></li>--}}
{{--                        </ul>--}}
{{--                        <!-- .menu / end -->--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
{{--            <div class="topbar__item">--}}
{{--                <div class="topbar-dropdown">--}}
{{--                    <button class="topbar-dropdown__btn" type="button">زبان: <span class="topbar__item-value">فارسی</span>--}}
{{--                        <svg width="7px" height="5px">--}}
{{--                            <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-down-7x5')}}"></use>--}}
{{--                        </svg>--}}
{{--                    </button>--}}
{{--                    <div class="topbar-dropdown__body">--}}
{{--                        <!-- .menu -->--}}
{{--                        <ul class="menu menu--layout--topbar menu--with-icons">--}}
{{--                            <li>--}}
{{--                                <a href="">--}}
{{--                                    <div class="menu__icon"><img srcset="{{ asset('assets/them/images/languages/language-1.png')}}, {{ asset('assets/them/images/languages/language-1@2x.png 2x')}}" src="{{ asset('assets/them/images/languages/language-1.png')}}" alt=""></div>انگلیسی</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="">--}}
{{--                                    <div class="menu__icon"><img srcset="{{ asset('assets/them/images/languages/language-2.png')}}, {{ asset('assets/them/images/languages/language-2@2x.png 2x')}}" src="{{ asset('assets/them/images/languages/language-2.png')}}" alt=""></div>فرانسوی</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="">--}}
{{--                                    <div class="menu__icon"><img srcset="{{ asset('assets/them/images/languages/language-3.png')}}, {{ asset('assets/them/images/languages/language-3@2x.png 2x')}}" src="{{ asset('assets/them/images/languages/language-3.png')}}" alt=""></div>آلمانی</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="">--}}
{{--                                    <div class="menu__icon"><img srcset="{{ asset('assets/them/images/languages/language-4.png')}}, {{ asset('assets/them/images/languages/language-4@2x.png 2x')}}" src="{{ asset('assets/them/images/languages/language-4.png')}}" alt=""></div>روسی</a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="">--}}
{{--                                    <div class="menu__icon"><img srcset="{{ asset('assets/them/images/languages/language-5.png')}}, {{ asset('assets/them/images/languages/language-5@2x.png 2x')}}" src="{{ asset('assets/them/images/languages/language-5.png')}}" alt=""></div>ایتالیایی</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <!-- .menu / end -->--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>
</div>
