<header class="site__header d-lg-block d-none">
    <div class="site-header">
        <!-- .topbar -->
    @include('web.them.partials.desktopSiteHeader.topbar')
    <!-- .topbar / end -->
        @include('web.them.partials.desktopSiteHeader.siteHeaderMiddleContainer')

        <div class="site-header__nav-panel">
            <div class="nav-panel">
                <div class="nav-panel__container container">
                    <div class="nav-panel__row">
                        <div class="nav-panel__departments">
                            <!-- .departments -->
                        @include('web.them.partials.desktopSiteHeader.navPanelDepartments')
                        <!-- .departments / end -->
                        </div>
                        <!-- .nav-links -->
                    @include('web.them.partials.desktopSiteHeader.navLinks')
                    <!-- .nav-links / end -->
                        @include('web.them.partials.desktopSiteHeader.navPanelIndicators')
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
