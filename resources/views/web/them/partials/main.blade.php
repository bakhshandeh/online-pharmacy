<div class="site__body">
    <div class="page-header">
        <div class="page-header__container container">
{{--            <div class="page-header__breadcrumb">--}}
{{--                <nav aria-label="breadcrumb">--}}
{{--                    <ol class="breadcrumb">--}}
{{--                        <li class="breadcrumb-item"><a href="index.html">خانه</a>--}}
{{--                            <svg class="breadcrumb-arrow" width="6px" height="9px">--}}
{{--                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>--}}
{{--                            </svg>--}}
{{--                        </li>--}}
{{--                        <li class="breadcrumb-item"><a href="">تکه مسیر</a>--}}
{{--                            <svg class="breadcrumb-arrow" width="6px" height="9px">--}}
{{--                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-right-6x9')}}"></use>--}}
{{--                            </svg>--}}
{{--                        </li>--}}
{{--                        <li class="breadcrumb-item active" aria-current="page">سبد خرید</li>--}}
{{--                    </ol>--}}
{{--                </nav>--}}
{{--            </div>--}}
            <div class="page-header__title">
                <h1>@section('title_page')@show</h1>
                <div>        @include('flash::message')
                </div>
            </div>
        </div>
    </div>
    <div class="block-empty__body">
        @yield('content')
    </div>
</div>
