
<!DOCTYPE html>
<html lang="fa" dir="rtl">

<head>
    @include('web.them.partials.head')
</head>

<body>
<!-- quickview-modal -->
<div id="quickview-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content"></div>
    </div>
</div>
<!-- quickview-modal / end -->
<!-- mobilemenu -->
@include('web.them.partials.mobileMenu')
<!-- mobilemenu / end -->
<!-- site -->
<div class="site">
    <!-- mobile site__header -->
    @include('web.them.partials.mobileSiteHeader')
    <!-- mobile site__header / end -->
    <!-- desktop site__header -->
    @include('web.them.partials.desktopSiteHeader')
    <!-- desktop site__header / end -->
    <!-- site__body -->
    @include('web.them.partials.main')
    <!-- site__body / end -->
    <!-- site__footer -->
    @include('web.them.partials.footer')
    <!-- site__footer / end -->
</div>
<!-- site / end -->

<!-- Mainly scripts -->
@include('web.them.partials.scripts')
@stack('before-body-end')
</body>

</html>
