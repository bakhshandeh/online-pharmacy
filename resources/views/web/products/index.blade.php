@extends('web.them.layouts.panel')

@push('style')

@endpush

@section('title', 'محصولات')
@section('title_page', 'لیست محصولات')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="block">
                    <div class="products-view">
                        <div class="products-view__options">
                            <div class="view-options">
                                <div class="view-options__layout">
                                    <div class="layout-switcher">
                                        <div class="layout-switcher__list">
                                            <button data-layout="grid-5-full" data-with-features="false" title="Grid"
                                                    type="button"
                                                    class="layout-switcher__button layout-switcher__button--active">
                                                <svg width="16px" height="16px">
                                                    <use
                                                        xlink:href="{{ asset('assets/them/images/sprite.svg#layout-grid-16x16')}}"></use>
                                                </svg>
                                            </button>
                                            <button data-layout="grid-5-full" data-with-features="true"
                                                    title="Grid With Features" type="button"
                                                    class="layout-switcher__button">
                                                <svg width="16px" height="16px">
                                                    <use
                                                        xlink:href="{{ asset('assets/them/images/sprite.svg#layout-grid-with-details-16x16')}}"></use>
                                                </svg>
                                            </button>
                                            <button data-layout="list" data-with-features="false" title="List"
                                                    type="button" class="layout-switcher__button">
                                                <svg width="16px" height="16px">
                                                    <use
                                                        xlink:href="{{ asset('assets/them/images/sprite.svg#layout-list-16x16')}}"></use>
                                                </svg>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                {{--                                <div class="view-options__legend">در حال نمایش 6 از 98 محصول</div>--}}
                                <div class="view-options__divider"></div>
                                {{--                                <div class="view-options__control">--}}
                                {{--                                    <label for="">مرتب‌سازی بر اساس</label>--}}
                                {{--                                    <div>--}}
                                {{--                                        <select class="form-control form-control-sm" name="" id="">--}}
                                {{--                                            <option value="">پیش فرض</option>--}}
                                {{--                                            <option value="">نام (صعودی)</option>--}}
                                {{--                                        </select>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="view-options__control">--}}
                                {{--                                    <label for="">نمایش</label>--}}
                                {{--                                    <div>--}}
                                {{--                                        <select class="form-control form-control-sm" name="" id="">--}}
                                {{--                                            <option value="">12</option>--}}
                                {{--                                            <option value="">24</option>--}}
                                {{--                                        </select>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                            </div>
                        </div>
                        <div class="products-view__list products-list" data-layout="grid-5-full"
                             data-with-features="false">
                            <div class="products-list__body">
                                @foreach($products as $product)
                                    <div class="products-list__item">
                                        <div class="product-card">
                                            {{--                                        <button class="product-card__quickview" type="button">--}}
                                            {{--                                            <svg width="16px" height="16px">--}}
                                            {{--                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#quickview-16')}}"></use>--}}
                                            {{--                                            </svg> <span class="fake-svg-icon"></span></button>--}}
                                            <div class="product-card__image">
                                                @if($product->files()->where('type', 'image')->first())
                                                    <a href=""><img
                                                            src="{{ asset(''.Storage::url($product->files()->where('type', 'image')->first()->type. DIRECTORY_SEPARATOR .$product->files()->where('type', 'image')->first()->name).'')}}"
                                                            alt=""></a>
                                                @endif
                                            </div>
                                            <div class="product-card__info">
                                                <div class="product-card__name"><a
                                                        href="product.html">{{$product->persian_name ?? ''}}{{$product->name ? '('. $product->name. ')' : ''}}{{$product->amount ? ' - '.$product->amount : ''}}</a>
                                                </div>
                                                {{--                                            <div class="product-card__rating">--}}
                                                {{--                                                <div class="rating">--}}
                                                {{--                                                    <div class="rating__body">--}}
                                                {{--                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">--}}
                                                {{--                                                            <g class="rating__fill">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                            <g class="rating__stroke">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal-stroke')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                        </svg>--}}
                                                {{--                                                        <div class="rating__star rating__star--only-edge rating__star--active">--}}
                                                {{--                                                            <div class="rating__fill">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                            <div class="rating__stroke">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">--}}
                                                {{--                                                            <g class="rating__fill">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                            <g class="rating__stroke">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal-stroke')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                        </svg>--}}
                                                {{--                                                        <div class="rating__star rating__star--only-edge rating__star--active">--}}
                                                {{--                                                            <div class="rating__fill">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                            <div class="rating__stroke">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">--}}
                                                {{--                                                            <g class="rating__fill">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                            <g class="rating__stroke">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal-stroke')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                        </svg>--}}
                                                {{--                                                        <div class="rating__star rating__star--only-edge rating__star--active">--}}
                                                {{--                                                            <div class="rating__fill">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                            <div class="rating__stroke">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <svg class="rating__star rating__star--active" width="13px" height="12px">--}}
                                                {{--                                                            <g class="rating__fill">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                            <g class="rating__stroke">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal-stroke')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                        </svg>--}}
                                                {{--                                                        <div class="rating__star rating__star--only-edge rating__star--active">--}}
                                                {{--                                                            <div class="rating__fill">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                            <div class="rating__stroke">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                        <svg class="rating__star" width="13px" height="12px">--}}
                                                {{--                                                            <g class="rating__fill">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                            <g class="rating__stroke">--}}
                                                {{--                                                                <use xlink:href="{{ asset('assets/them/images/sprite.svg#star-normal-stroke')}}"></use>--}}
                                                {{--                                                            </g>--}}
                                                {{--                                                        </svg>--}}
                                                {{--                                                        <div class="rating__star rating__star--only-edge">--}}
                                                {{--                                                            <div class="rating__fill">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                            <div class="rating__stroke">--}}
                                                {{--                                                                <div class="fake-svg-icon"></div>--}}
                                                {{--                                                            </div>--}}
                                                {{--                                                        </div>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                </div>--}}
                                                {{--                                                <div class="product-card__rating-legend">9 نقد و بررسی</div>--}}
                                                {{--                                            </div>--}}

                                                <ul class="product-card__features-list">
                                                    <li> نام عمومی: {{$product->generic_name ?? ''}}</li>
                                                    <li>تولیدکننده: {{$product->manufacturer ?? ''}}</li>

                                                </ul>
                                            </div>
                                            <div class="product-card__actions">
                                                @if($product->sum_number > 0)
                                                    <div class="product-card__availability">موجودی: <span
                                                            class="text-success">موجود در انبار</span>
                                                    </div>
                                                    <div class="product-card__prices">
                                                        {{$product->sellers()->min('seller_price') ?? ''}} تومان
                                                    </div>
                                                @elseif($product->sum_number <= 0)
                                                    <div class="product-card__availability">موجودی:
                                                        <span
                                                            class="text-danger">اتمام موجودی در انبار
                                                        </span>
                                                    </div>
                                                    <div class="product-card__prices"><span
                                                            class="text-danger">ناموجودی
                                                        </span>
                                                    </div>
                                                @endif

                                                <div class="product-card__buttons">
                                                    @if($product->sum_number != 0)
                                                        @if($product->categories->first()->id != 1)
                                                            <button class="btn btn-primary product-card__addtocart"
                                                                    type="button"><a
                                                                    href="{{route('basket.add', ['product' => $product->id, 'prescription' => 0, 'seller' => $product->sellers()->where('seller_price',$product->sellers()->min('seller_price'))->first()->id])}}">افزودن
                                                                    به سبد</a></button>
                                                            <button
                                                                class="btn btn-secondary product-card__addtocart product-card__addtocart--list"
                                                                type="button"><a
                                                                    href="{{route('basket.add', ['product' => $product->id, 'prescription' => 0, 'seller' => $product->sellers()->where('seller_price',$product->sellers()->min('seller_price'))->first()->id])}}">افزودن
                                                                    به سبد</a></button>
                                                        @endif
                                                    @endif
                                                        @if($product->categories->first()->id == 1)
                                                            <div class="product-card__addtocart ">
                                                                <span class="text-success">فقط با نسخه

                                                                </span>
                                                            </div>
                                                            <div class="product-card__addtocart product-card__addtocart--list">
                                                                <span class="text-success">فقط با نسخه

                                                                </span>
                                                            </div>
                                                        @endif

                                                        {{--                                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__wishlist" type="button">--}}
                                                    {{--                                                    <svg width="16px" height="16px">--}}
                                                    {{--                                                        <use xlink:href="{{ asset('assets/them/images/sprite.svg#wishlist-16')}}"></use>--}}
                                                    {{--                                                    </svg> <span class="fake-svg-icon fake-svg-icon--wishlist-16"></span>--}}
                                                    {{--                                                </button>--}}
                                                    {{--                                                <button class="btn btn-light btn-svg-icon btn-svg-icon--fake-svg product-card__compare" type="button">--}}
                                                    {{--                                                    <svg width="16px" height="16px">--}}
                                                    {{--                                                        <use xlink:href="{{ asset('assets/them/images/sprite.svg#compare-16')}}"></use>--}}
                                                    {{--                                                    </svg> <span class="fake-svg-icon fake-svg-icon--compare-16"></span>
                                                                                                                </button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        {{--                        <div class="products-view__pagination">--}}
                        {{--                            <ul class="pagination justify-content-center">--}}
                        {{--                                <li class="page-item disabled">--}}
                        {{--                                    <a class="page-link page-link--with-arrow" href="" aria-label="Previous">--}}
                        {{--                                        <svg class="page-link__arrow page-link__arrow--left" aria-hidden="true" width="8px" height="13px">--}}
                        {{--                                            <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-left-8x13')}}"></use>--}}
                        {{--                                        </svg>--}}
                        {{--                                    </a>--}}
                        {{--                                </li>--}}
                        {{--                                <li class="page-item"><a class="page-link" href="">1</a></li>--}}
                        {{--                                <li class="page-item active"><a class="page-link" href="">2 <span class="sr-only">(کنونی)</span></a></li>--}}
                        {{--                                <li class="page-item"><a class="page-link" href="">3</a></li>--}}
                        {{--                                <li class="page-item">--}}
                        {{--                                    <a class="page-link page-link--with-arrow" href="" aria-label="Next">--}}
                        {{--                                        <svg class="page-link__arrow page-link__arrow--right" aria-hidden="true" width="8px" height="13px">--}}
                        {{--                                            <use xlink:href="{{ asset('assets/them/images/sprite.svg#arrow-rounded-right-8x13')}}"></use>--}}
                        {{--                                        </svg>--}}
                        {{--                                    </a>--}}
                        {{--                                </li>--}}
                        {{--                            </ul>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')

@endpush

@push('before-body-end')

@endpush
