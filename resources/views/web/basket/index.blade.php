@extends('web.them.layouts.panel')

@push('style')

@endpush

@section('title', 'سبد خرید')
@section('title_page', 'سبد خرید')

@section('content')
{{--    {{dd($items)}}--}}
{{--    @if($items->isEmpty())--}}
{{--        <div class="block-empty__body">--}}
{{--            <div class="block-empty__message">سبد خرید شما خالی است!</div>--}}
{{--            <div class="block-empty__actions"><a class="btn btn-primary btn-sm" href="{{route('products.index',['category' => 3])}}">ادامه</a></div>--}}
{{--        </div>--}}
{{--    @else--}}
        <div class="cart block">
        <div class="container">
            <table class="cart__table cart-table">
                <thead class="cart-table__head">
                <tr class="cart-table__row">
                    <th class="cart-table__column cart-table__column--image">تصویر</th>
                    <th class="cart-table__column cart-table__column--product">محصول/ کد نسخه</th>
                    <th class="cart-table__column cart-table__column--product">فروشنده</th>
                    <th class="cart-table__column cart-table__column--price">قیمت</th>
                    <th class="cart-table__column cart-table__column--quantity">تعداد</th>
                    <th class="cart-table__column cart-table__column--total">جمع کل</th>
                    <th class="cart-table__column cart-table__column--remove"></th>
                </tr>
                </thead>
                <tbody class="cart-table__body">
                @foreach($items[0] as $item)
                    <tr class="cart-table__row">
                        <td class="cart-table__column cart-table__column--image">
                            @if($item->files()->where('type', 'image')->first())
                                <a href=""><img src="{{ asset(''.Storage::url($item->files()->where('type', 'image')->first()->type. DIRECTORY_SEPARATOR .$item->files()->where('type', 'image')->first()->name).'')}}" alt=""></a>
                            @endif
{{--                            <a href=""><img src="images/products/product-1.jpg" alt=""></a>--}}
                        </td>
                        <td class="cart-table__column cart-table__column--product"><a href="" class="cart-table__product-name">{{$item->persian_name ?? ''}}{{$item->name ? '(' .$item->name . ')' : ''}}{{$item->amount ?? ''}}</a>
                            <ul class="cart-table__options">
                                <li> نام عمومی: {{$item->generic_name ?? ''}}</li>
                                <li>تولیدکننده: {{$item->manufacturer ?? ''}}</li>
                            </ul>
                        </td>
                        <td class="cart-table__column cart-table__column--seller" data-title="فروشنده">{{$item->seller->persian_name ?? ''}} {{$item->seller->name ? '(' .$item->seller->name . ')' : ''}}</td>
                        <td class="cart-table__column cart-table__column--price" data-title="قیمت">{{number_format($item->sellers()->where('seller_id',1)->first()->pivot->seller_price)}} تومان </td>
                        <td class="cart-table__column cart-table__column--quantity" data-title="تعداد">
{{--                            <div class="input-number">--}}
{{--                                <input class="form-control input-number__input" type="number" min="1" value="{{$item->quantity}}">--}}
{{--                                <div class="input-number__add"></div>--}}
{{--                                <div class="input-number__sub"></div>--}}
{{--                            </div>--}}
                            <form action="{{route('basket.update', [$item->id, $item->seller->id])}}" method="post" class="form-inline input-number">
                                @csrf
                                <select name="quantity" id="quantity" class="form-control input-sm mr-sm-2">
                                    @for($i = 0; $i <= $item->sum_number ; $i++)
                                        <option {{$item->quantity == $i ? 'selected' : ''}} value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>
                                <br></br>
                                <button type="submit" class="btn btn-primary btn-sm"> بروزرسانی </button>
                            </form>
                        </td>
                        <td class="cart-table__column cart-table__column--total" data-title="جمع کل">{{number_format($item->sellers()->where('seller_id',1)->first()->pivot->seller_price * $item->quantity)}} تومان</td>
                        <td class="cart-table__column cart-table__column--remove">
                            <button type="button" class="btn btn-light btn-sm btn-svg-icon">
                                <svg width="12px" height="12px">
                                    <use xlink:href="images/sprite.svg#cross-12"></use>
                                </svg>
                            </button>
                        </td>
                    </tr>

                @endforeach

                @foreach($items[1] as $item)
                    <tr class="cart-table__row">
                        <td class="cart-table__column cart-table__column--image">
                            @if($item->files()->where('type', 'image')->first())
                                <a href=""><img src="{{ asset(''.Storage::url($item->files()->where('type', 'image')->first()->type. DIRECTORY_SEPARATOR .$item->files()->where('type', 'image')->first()->name).'')}}" alt=""></a>
                            @endif
                            {{--                            <a href=""><img src="images/products/product-1.jpg" alt=""></a>--}}
                        </td>
                        <td class="cart-table__column cart-table__column--product"><a href="" class="cart-table__product-name"> کد نسخه: {{$item->id ?? ''}}</a>
                            <ul class="cart-table__options">
                                <li>نام بیمار: {{$item->sick_name ?? ''}}</li>
                                <li>نام دکتر: {{$item->doctor_name ?? ''}}</li>
                            </ul>
                        </td>
                        <td class="cart-table__column cart-table__column--seller" data-title="فروشنده">{{$item->seller->persian_name ?? ''}} {{$item->seller->name ? '(' .$item->seller->name . ')' : ''}}</td>
                        <td class="cart-table__column cart-table__column--price" data-title="قیمت">{{number_format($item->price)}} تومان </td>
                        <td class="cart-table__column cart-table__column--quantity" data-title="تعداد">
                            {{--                            <div class="input-number">--}}
                            {{--                                <input class="form-control input-number__input" type="number" min="1" value="{{$item->quantity}}">--}}
                            {{--                                <div class="input-number__add"></div>--}}
                            {{--                                <div class="input-number__sub"></div>--}}
                            {{--                            </div>--}}
                            1
                        </td>
                        <td class="cart-table__column cart-table__column--total" data-title="جمع کل">{{number_format($item->price)}} تومان</td>
                        <td class="cart-table__column cart-table__column--remove">
                            <button type="button" class="btn btn-light btn-sm btn-svg-icon">
                                <svg width="12px" height="12px">
                                    <use xlink:href="images/sprite.svg#cross-12"></use>
                                </svg>
                            </button>
                        </td>
                    </tr>

                @endforeach

                </tbody>
            </table>


                <div class="cart__actions">
                <form class="cart__coupon-form">
{{--                    <label for="input-coupon-code" class="sr-only">رمز عبور</label>--}}
{{--                    <input type="text" class="form-control" id="input-coupon-code" placeholder="کد تخفیف">--}}
{{--                    <button type="submit" class="btn btn-primary">اعمال کد تخفیف</button>--}}
                </form>
                <div class="cart__buttons">
                    <a href="{{route('products.index',['category' => 3])}}" class="btn btn-light">ادامه خرید</a>
{{--                    <a href="" class="btn btn-primary cart__update-button">به روز رسانی سبد</a>--}}
                </div>
            </div>
            @include('web.basket.partials.summary')
        </div>
    </div>
{{--    @endif--}}

@endsection

@push('scripts')

@endpush

@push('before-body-end')

@endpush
