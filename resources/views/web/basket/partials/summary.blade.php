@inject('basket','App\Support\Basket\Basket')
<div class="row justify-content-end pt-5">
    <div class="col-12 col-md-7 col-lg-6 col-xl-5">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">مجموع سبد</h3>
                <table class="cart__totals">
                    <thead class="cart__totals-header">
                    <tr>
                        <th>مبلغ کل</th>
                        <td>{{number_format($basket->subTotal())}} تومان</td>
                    </tr>
                    </thead>
                    <tbody class="cart__totals-body">
                    <tr>
                        <th>حمل و نقل</th>
                        <td>{{number_format(10000)}} تومان
{{--                            <div class="cart__calc-shipping"><a href="#">محاسبه هزینه</a></div>--}}
                        </td>
                    </tr>
                    <tr>
                        <th>مالیات</th>
                        <td>0 تومان</td>
                    </tr>
                    </tbody>
                    <tfoot class="cart__totals-footer">
                    <tr>
                        <th>مبلغ قابل پرداخت</th>
                        <td>{{number_format($basket->subTotal() + 10000)}} تومان</td>
                    </tr>
                    </tfoot>
                </table><a class="btn btn-primary btn-xl btn-block cart__checkout-button" href="{{route('basket.checkout.form')}}">ثبت و ادامه سفارش</a></div>
        </div>
    </div>
</div>
