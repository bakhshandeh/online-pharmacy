@extends('web.them.layouts.panel')

@push('style')

@endpush

@section('title', 'بررسی پرداخت')
@section('title_page', 'بررسی نهایی پرداخت')

@section('content')
    @inject('basket','App\Support\Basket\Basket')
    <form action="{{route('basket.checkout')}}" method="post" id="checkout-form">
        @csrf
        <div class="checkout block">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xl-7">
                        <div class="card mb-lg-0">
                            <div class="card-body">
                                <div class="card-header"><h4> آدرس گیرنده</h4></div>
                                <div class="card-body">
                                    <ul class="list-group list-group-flush">
                                        @if(!is_null($addressUser))
                                        {{--                                    <li class="list-group-item">--}}
                                        {{--                                        <div class="form-group">--}}
                                        {{--                                            <div class="form-check">--}}
                                        {{--                                                <span class="form-check-input input-check">--}}
                                        {{--                                                    <span class="input-check__body">--}}
                                        <input hidden class="input-check__input " type="radio" id="active-address"
                                               name="active-address" value="{{$addressUser->id ?? ''}}" checked>
                                        {{--                                                        <span class="input-check__box"></span>--}}
                                        {{--                                                        <svg class="input-check__icon" width="9px" height="7px">--}}
                                        {{--                                                        --}}{{--												        <use xlink:href="images/sprite.svg#check-9x7"></use>--}}
                                        {{--                                                        --}}{{--											            </svg>--}}
                                        {{--											        </span>--}}
                                        {{--											</span>--}}
                                        {{--                                                <label class="form-check-label" for="checkout-different-address">ارسال این آدرس:</label>--}}
                                        {{--                                            </div>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </li>--}}
                                        <li class="list-group-item">
                                            گیرنده: {{$addressUser->recipient_first_name . ' ' . $addressUser->recipient_last_name}}</li>
                                        <li class="list-group-item">کدملی
                                            : {{$addressUser->recipient_national_code}}</li>
                                        <li class="list-group-item">تلفن :{{$addressUser->recipient_phone_number}}</li>
                                        <li class="list-group-item"> آدرس:
                                            @if(!is_null($addressUser->province_id))
                                                {{$addressUser->province->name ? "استان:". $addressUser->province->name."-" : ''}}
                                                @if(!is_null($addressUser->city_id))
                                                    {{$addressUser->city->name ? "شهرستان :".$addressUser->city->name."-" : ''}}
                                                    @if(!is_null($addressUser->quarter_id))
                                                        {{$addressUser->quarter->name ? "بخش :".$addressUser->quarter->name."-" : ''}}
                                                        {{$addressUser->street_alley ? "خیابان و کوچه :".$addressUser->street_alley."-" : ''}}
                                                        {{$addressUser->no ? "پلاک :".$addressUser->no."-" : ''}}
                                                        {{$addressUser->floor ? "طبقه :".$addressUser->floor."-" : ''}}
                                                        {{$addressUser->unit ? "واحد :".$addressUser->unit."-"  : ''}}
                                                        {{$addressUser->postal_code ? "کدپستی :".$addressUser->postal_code : ''}}

                                                    @endif
                                                @endif
                                            @endif
                                        </li>
                                        @endif
                                    </ul>

                                    {{--                                @foreach($addressUser as $_addressUser)--}}
                                    {{--                                    <ul class="list-group list-group-flush">--}}
                                    {{--                                        <li class="list-group-item">--}}
                                    {{--                                            <div class="form-group">--}}
                                    {{--                                                <div class="form-check">--}}
                                    {{--                                                <span class="form-check-input input-check">--}}
                                    {{--                                                    <span class="input-check__body">--}}
                                    {{--                                                        <input class="input-check__input " type="radio" id="active-address" name="active-address" value="{{$_addressUser->id}}">--}}
                                    {{--                                                        <span class="input-check__box"></span>--}}
                                    {{--                                                        <svg class="input-check__icon" width="9px" height="7px">--}}
                                    {{--                                                        --}}{{--												        <use xlink:href="images/sprite.svg#check-9x7"></use>--}}
                                    {{--                                                        --}}{{--											            </svg>--}}
                                    {{--											        </span>--}}
                                    {{--											</span>--}}
                                    {{--                                                    <label class="form-check-label" for="checkout-different-address">ارسال این آدرس:</label>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </li>--}}
                                    {{--                                        <li class="list-group-item">گیرنده: {{$_addressUser->recipient_first_name . ' ' . $_addressUser->recipient_last_name}}</li>--}}
                                    {{--                                        <li class="list-group-item">کدملی : {{$_addressUser->recipient_national_code}}</li>--}}
                                    {{--                                        <li class="list-group-item">تلفن :{{$_addressUser->recipient_phone_number}}</li>--}}
                                    {{--                                        <li class="list-group-item"> آدرس:--}}
                                    {{--                                            @if(!is_null($_addressUser->province_id))--}}
                                    {{--                                                {{$_addressUser->province->name ? "استان:". $_addressUser->province->name."-" : ''}}--}}
                                    {{--                                                @if(!is_null($_addressUser->city_id))--}}
                                    {{--                                                    {{$_addressUser->city->name ? "شهرستان :".$_addressUser->city->name."-" : ''}}--}}
                                    {{--                                                    @if(!is_null($_addressUser->quarter_id))--}}
                                    {{--                                                        {{$_addressUser->quarter->name ? "بخش :".$_addressUser->quarter->name."-" : ''}}--}}
                                    {{--                                                        {{$_addressUser->street_alley ? "خیابان و کوچه :".$_addressUser->street_alley."-" : ''}}--}}
                                    {{--                                                        {{$_addressUser->no ? "پلاک :".$_addressUser->no."-" : ''}}--}}
                                    {{--                                                        {{$_addressUser->floor ? "طبقه :".$_addressUser->floor."-" : ''}}--}}
                                    {{--                                                        {{$_addressUser->unit ? "واحد :".$_addressUser->unit."-"  : ''}}--}}
                                    {{--                                                        {{$_addressUser->postal_code ? "کدپستی :".$_addressUser->postal_code : ''}}--}}

                                    {{--                                                    @endif--}}
                                    {{--                                                @endif--}}
                                    {{--                                            @endif--}}
                                    {{--                                        </li>--}}
                                    {{--                                    </ul>--}}
                                    {{--                                @endforeach--}}

                                    <a href="{{route('manegment.users.users.address.create')}}?redirect=basket"
                                       class="btn btn-primary btn-block">افزودن آدرس جدید</a>

                                </div>
                                @error('active-address')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="card-divider"></div>

                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xl-5 mt-4 mt-lg-0">
                        <div class="card mb-0">
                            <div class="card-body">
                                <h3 class="card-title">پرداخت</h3>
                                <table class="checkout__totals">
                                    <thead class="checkout__totals-header">
                                    </thead>

                                    <tbody class="checkout__totals-subtotals">
                                    <tr>
                                        <th>مبلغ کل</th>
                                        <td>{{number_format($basket->subTotal())}} تومان</td>
                                    </tr>
                                    <tr>
                                        <th>حمل و نقل</th>
                                        <td>{{number_format(10000)}} تومان</td>
                                    </tr>
                                    <tr>
                                        <th>مالیات</th>
                                        <td>0 تومان</td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="checkout__totals-footer">
                                    <tr>
                                        <th>مبلغ قابل پرداخت</th>
                                        <td>{{number_format($basket->subTotal() + 10000)}} تومان</td>
                                    </tr>
                                    </tfoot>
                                </table>
                                <div class="payment-methods">
                                    <ul class="payment-methods__list">
                                        {{--                                    <li class="payment-methods__item payment-methods__item--active">--}}
                                        {{--                                        <label class="payment-methods__item-header"><span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" checked> <span class="input-radio__circle"></span> </span>--}}
                                        {{--													</span><span class="payment-methods__item-title">فیش بانکی</span></label>--}}
                                        {{--                                        <div class="payment-methods__item-container">--}}
                                        {{--                                            <div class="payment-methods__item-description text-muted">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی</div>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </li>--}}
                                        {{--                                    <li class="payment-methods__item">--}}
                                        {{--                                        <label class="payment-methods__item-header"><span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" id="online" value="online"> <span class="input-radio__circle"></span> </span>--}}
                                        {{--													</span><span class="payment-methods__item-title">پرداخت آنلاین</span></label>--}}
                                        {{--                                        <div class="payment-methods__item-container">--}}
                                        {{--                                            <select name="gateway" id="quantity" class="form-control ">--}}
{{--                                                                                        <option value="saman" selected >سامان</option>--}}
{{--                                                                                        <option value="pasargad" disabled>پاسارگاد</option>--}}
                                        {{--                                            </select>--}}
                                        {{--                                            <div class="payment-methods__item-description text-muted">پرداخت از درگاه های الکترونیکی صورت می پذیرد.</div>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </li>--}}
                                        {{--                                    <li class="payment-methods__item">--}}
                                        {{--                                        <label class="payment-methods__item-header"><span class="payment-methods__item-radio input-radio"><span class="input-radio__body"><input class="input-radio__input" name="checkout_payment_method" type="radio" id="cart" value="cart"> <span class="input-radio__circle"></span> </span>--}}
                                        {{--													</span><span class="payment-methods__item-title">پرداخت کارت به کارت</span></label>--}}

                                        {{--                                        <div class="payment-methods__item-container">--}}
                                        {{--                                            <div class="payment-methods__item-description text-muted">لطفا مبلغ را به شماره کارت ۴۴۴۴-۴۴۴۴-۴۴۴۴-۴۴۴۴ واریز نمایید و کد پیگیرا را به همکاران ما اطلاع دهید.</div>--}}
                                        {{--                                        </div>--}}
                                        {{--                                    </li>--}}
                                        <li class="payment-methods__item">
                                            <label class="payment-methods__item-header"><span
                                                    class="payment-methods__item-radio input-radio"><span
                                                        class="input-radio__body">
                                                        <input class="input-radio__input" name="checkout_payment_method"
                                                               type="radio" id="cash" value="cash" checked>
                                                        <span class="input-radio__circle"></span> </span>
													</span><span
                                                    class="payment-methods__item-title">پرداخت در نقدی</span></label>
                                            <div class="payment-methods__item-container">
                                                <div class="payment-methods__item-description text-muted">در این روش شما
                                                    می توانید درب منزل خود مبلغ را پرداخت کنید.
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                                {{--                            <div class="checkout__agree form-group">--}}
                                {{--                                <div class="form-check"><span class="form-check-input input-check"><span class="input-check__body"><input class="input-check__input" type="checkbox" id="checkout-terms"> <span class="input-check__box"></span>--}}
                                {{--											<svg class="input-check__icon" width="9px" height="7px">--}}
                                {{--												<use xlink:href="images/sprite.svg#check-9x7"></use>--}}
                                {{--											</svg>--}}
                                {{--											</span>--}}
                                {{--											</span>--}}
                                {{--                                    <label class="form-check-label" for="checkout-terms">من <a target="_blank" href="terms-and-conditions.html">قوانین و مقررات</a> را خوانده و موافقم *</label>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                                <button type="submit" class="btn btn-primary btn-xl btn-block">ثبت سفارش</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($errors->any())
            @foreach ($errors->all() as $error)
                <div>{{ $error }}</div>
            @endforeach
        @endif

    </form>

@endsection

@push('scripts')

@endpush

@push('before-body-end')

@endpush
