<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('province_id')->constrained('provinces')->onDelete('cascade');
            $table->foreignId('city_id')->nullable()->constrained('cities')->onDelete('cascade');
            $table->foreignId('quarter_id')->nullable()->constrained('quarters')->onDelete('cascade');

            $table->string('street_alley');
            $table->integer('postal_code');
            $table->integer('no');
            $table->integer('floor')->nullable();
            $table->integer('unit')->nullable();

            $table->string('recipient_first_name');
            $table->string('recipient_last_name');
            $table->string('recipient_national_code',10);
            $table->char('recipient_phone_number',20);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_users');
    }
}
