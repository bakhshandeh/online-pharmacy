<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('last_name')->nullable()->after('first_name');
            $table->string('phone_number')->unique()->after('last_name');
            $table->timestamp('phone_number_verified_at')->nullable()->after('phone_number');
            $table->string('national_code',10)->nullable()->after('email_verified_at');
            $table->timestamp('date_birth')->nullable()->after('national_code');
            $table->string('job')->nullable()->after('date_birth');
            $table->string('card_number')->nullable()->after('job');
            $table->string('picture')->nullable()->after('card_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_name');
            $table->dropColumn('phone_number');
            $table->dropColumn('phone_number_verified_at');
            $table->dropColumn('national_code');
            $table->dropColumn('date_birth');
            $table->dropColumn('job');
            $table->dropColumn('card_number');
            $table->dropColumn('picture');
        });
    }
}
