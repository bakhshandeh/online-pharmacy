<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('province_id')->nullable()->constrained('provinces')->onDelete('cascade');
            $table->foreignId('city_id')->nullable()->constrained('cities')->onDelete('cascade');
            $table->foreignId('quarter_id')->nullable()->constrained('quarters')->onDelete('cascade');

            $table->string('name')->unique();
            $table->string('persian_name')->unique();
            $table->string('phone_number')->nullable();
            $table->string('email')->nullable();
            $table->integer('score')->nullable()->default(0);

            $table->string('street_alley')->nullable();
            $table->integer('postal_code')->nullable();
            $table->integer('no')->nullable();
            $table->integer('floor')->nullable();
            $table->integer('unit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
