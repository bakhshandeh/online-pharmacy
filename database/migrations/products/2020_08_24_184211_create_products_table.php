<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique();
            $table->foreignId('user_id')->constrained('users')->onDelete('cascade');
            $table->foreignId('edit_user_id')->nullable()->constrained('users')->onDelete('cascade');
            $table->string('name');
            $table->string('persian_name')->nullable();
            $table->string('generic_name')->nullable();
            $table->string('manufacturer');
            $table->string('amount');
            $table->longText('description')->nullable();
            $table->unsignedInteger('sum_number')->default(0)->nullable();
            $table->integer('score')->nullable()->default(0);
            $table->boolean('sell_with_prescription')->default(false);


            $table->primary(['name', 'manufacturer', 'amount']);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
