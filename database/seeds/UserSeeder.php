<?php

use App\Repositories\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [1,1,'admin', 'admin', '09111111111', 'admin@yahoo.com','11111111',['admin project']],
            [2,1,'doctor', 'doctor', '09222222222', 'doctor@yahoo.com','22222222',['doctor']],
            [3,1,'user', 'user', '09333333333', 'user@yahoo.com','33333333',['user']],
        ];


        foreach ($users as $_user) {
            $user = new User();
            $user->id = $_user[0];
            $user->seller_id = $_user[1];
            $user->first_name = $_user[2];
            $user->last_name = $_user[3];
            $user->phone_number = $_user[4];
            $user->email = $_user[5];
            $user->password = Hash::make($_user[6]);
            $user->save();

            $user->refreshRoles($_user[7]);
        }
    }
}
