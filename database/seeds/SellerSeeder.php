<?php

use App\Repositories\Models\Products\Sellers\Seller;
use Illuminate\Database\Seeder;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seller = new Seller;
        $seller->id = 1;
        $seller->name = "Online Pharmacy";
        $seller->persian_name = "داروخانه آنلاین";
        $seller->save();

    }
}
