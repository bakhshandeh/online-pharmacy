<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(Provinces::class);
         $this->call(Cities::class);
         $this->call(Quarters::class);
         $this->call(PermissionSeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(SellerSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(CategoriesSeeder::class);

    }
}
