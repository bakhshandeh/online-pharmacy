<?php

use App\Repositories\Models\Products\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [1,'دارو بانسخه'],
            [2,'دارو بدون نسخه'],
            [3,'آرایشی و بهداشتی'],
            [4,'دارو'],
        ];


        foreach ($categories as $_category) {
            $category = new Category();
            $category->id = $_category[0];
            $category->name = $_category[1];
            $category->save();

        }
    }
}
