<?php

use App\Repositories\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['0' => 1, '1' =>'admin project', '2' =>'مدیر پروژه','3' => ['viewing the admin page is allowed','add permission','edit permission','delete permission','add role','edit role','delete role','add user','edite user','delete user','add or delete role for user','send mail','send sms','Viewing the user page is allowed']],
//            ['0' =>2, '1' =>'site designer', '2' =>'طراح سایت','3' => ['viewing the admin page is allowed','add permission','edit permission','delete permission','add role','edit role','delete role','add user','edite user','delete user','add or delete role for user','send mail','send sms','Viewing the user page is allowed']],
//            ['0' =>3, '1' =>'website design support', '2' =>'پشتیبان طراحی وب سایت','3' => ['viewing the admin page is allowed','add permission','edit permission','delete permission','add role','edit role','delete role','add user','edite user','delete user','add or delete role for user','send mail','send sms','Viewing the user page is allowed']],
//            ['0' =>4, '1' =>'admin', '2' =>'مدیر','3' => ['viewing the admin page is allowed','add user','edite user','delete user','add or delete role for user','send mail','send sms','Viewing the user page is allowed']],
//            ['0' =>5, '1' =>'web support', '2' =>'پشتیبان سایت','3' => ['viewing the admin page is allowed','add user','edite user','add or delete role for user','send mail','send sms','Viewing the user page is allowed']],
//            ['0' =>6, '1' =>'employee', '2' =>'کارمند','3' => ['viewing the admin page is allowed','send mail','send sms','Viewing the user page is allowed']],
//            ['0' =>7, '1' =>'user vip', '2' =>'کاربر ویژه','3' => ['Viewing the user page is allowed']],
            ['0' =>8, '1' =>'user', '2' =>'کاربر','3' => ['Viewing the user page is allowed']],
//            ['0' =>9, '1' =>'user guest', '2' =>'کاربر مهمان','3' => ['Viewing the user page is allowed']],
            ['0' =>10, '1' =>'doctor', '2' => 'پزشک','3' => ['viewing the admin page is allowed','Viewing the user page is allowed','Determine the price of the version','Viewing list prescriptions allowed','Increase inventory of products']]
        ];

        foreach ($roles as $_role) {

            $role = new Role();
            $role->id = $_role[0];
            $role->name = $_role[1];
            $role->persian_name = $_role[2];
            $role->save();

            $role->givePermissionsTo($_role[3]);

        }
    }
}
