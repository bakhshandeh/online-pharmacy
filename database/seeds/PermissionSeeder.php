<?php

use App\Repositories\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [1,'viewing the admin page is allowed','مشاهده صفحه مدیریت مجاز است'],
            [2,'add permission','افزودن سطح دسترسی'],
            [3,'edit permission','ویرایش سطح دسترسی'],
            [4,'delete permission','حذف سطح دسترسی'],
            [5,'add role','افزودن نقش'],
            [6,'edit role','ویرایش نقش'],
            [7,'delete role','حذف نقش'],
            [8,'add user','افزودن کاربر'],
            [9,'edite user','ویرایش کاربر'],
            [10,'delete user','حذف کاربر'],
            [11,'add or delete role for user','اضافه یا حذف کردن نقش برای کاربر'],
            [12,'send mail','ارسال ایمیل'],
            [13,'send sms','ارسال پیام کوتاه'],
            [14,'Viewing the user page is allowed','مشاهده صفحه کاربری مجاز است'],
            [15,'Viewing list prescriptions allowed','مشاهده لیست نسخه مجاز است'],
            [16,'Determine the price of the version','تعیین قیمت نسخه مجاز است'],
            [17,'Increase inventory of products','افزایش موجودی محصولات مجاز است'],
        ];

        foreach ($permissions as $_permission) {
            $permission = new Permission();
            $permission->id = $_permission[0];
            $permission->name = $_permission[1];
            $permission->persian_name = $_permission[2];
            $permission->save();
        }
    }
}
