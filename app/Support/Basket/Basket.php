<?php


namespace App\Support\Basket;


use App\Exceptions\QuantityExceededException;
use App\Repositories\Models\Prescriptions\Prescription;
use App\Repositories\Models\Products\Product;
use App\Repositories\Models\Products\ProductSeller;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Support\Storage\Contracts\StorageInterface;

class Basket
{
    private $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function add(Product $product = null, Prescription $prescriptions = null, int $quantity = null, Seller $seller = null)
    {
        if (!is_null($product))
        {
            if ($this->has($product))
            {
                $quantity = $this->get($product, null)['quantity'] + $quantity;
            }

        }
        $this->update($product, $prescriptions, $quantity, $seller);

//        if (!is_null($prescriptions))
//        {
//            if ($this->has($product, $prescriptions))
//            {
//                $quantity = $this->get($product)['quantity'] + $quantity;
//            }
//
//            $this->update($product, $prescriptions, $quantity, $seller);
//        }


    }

    public function update(Product $product = null, Prescription $prescriptions = null, int $quantity, Seller $seller =null)
    {
        if (!is_null($product))
        {
            $seller_id = $seller->id;
            if (!$product->hasSumNumber($quantity))
            {
                throw new QuantityExceededException();
            }
//        if (!$product->hasNumberSeller($quantity, $seller_id))
//        {
//            $productSeller = ProductSeller::all()->where('product_id', $product->id);
//
//            $quantity_old = $productSeller->where('seller_id', '>=' ,$seller_id)->first()->number;
//            $quantity_new = $quantity - $quantity_old;
//            $seller_id_new = '';
//            if(!is_null($productSeller->where('number', '>=' ,$quantity_new)->where('number', $productSeller->max('number'))->first()))
//            {
//                $seller_id_new = ProductSeller::all()->where('product_id', $product->id)->where('number', '>=' ,$quantity_new)->first()->seller_id;
//            }
//
//            $this->storage->set($product->id, [
//                'quantity' => $quantity,
//                'seller_id' => [
//                    $quantity_old => $seller_id,
//                    $quantity_new => $seller_id_new
//                ]
//            ]);
//            flash(' به دلیل کمی موجودی محصول فروشنده انتخابی از فروشندگان دیگر هم انتخاب شده. لطفا قیمت را مجددا چک بفرمایید')->success();
//            return;
//        }

//        $this->storage->set($product->id, [
//            'quantity' => $quantity,
//            'seller_id' => [
//                $quantity => $seller_id
//            ]
//        ]);


            if (!$quantity)
            {
                return $this->storage->unset($product->id);
            }

            $this->storage->set($product->id, [
                'quantity' => $quantity,
                'seller_id' => $seller_id
            ]);
        }
        if (!is_null($prescriptions))
        {

            $this->storage->set($prescriptions->id, [
                'quantity' => $quantity,
                'price' => $prescriptions->price,
            ]);
        }

    }

    public function all()
    {
        $prescriptions = Prescription::find(array_keys($this->storage->all()));
        $products = Product::find(array_keys($this->storage->all()));

        if (!is_null($products))
        {
            foreach ($products as $product)
            {
//                if ($this->storage->exists($product->id.'.quantity'))
//                {
                    $product->quantity = $this->get($product, null)['quantity'];
                    $product->seller = Seller::find($this->get($product,null)['seller_id']);
//                }

            }
        }

        if (is_null($prescriptions))
        {
            foreach ($prescriptions as $prescription)
            {

//                if ($this->storage->exists($prescription->id.'.price'))
//                {

                    $prescription->price = $this->get(null, $prescription)['price'];
//                }
            }
        }


        return [$products, $prescriptions];
    }

    public function subTotal()
    {
        $total = 0;
        if (!is_null($this->all()[0]))
        {
            foreach ($this->all()[0] as $item)
            {
                $total += $item->sellers()->where('seller_id',1)->first()->pivot->seller_price * $item->quantity;
            }
        }
        if (!is_null($this->all()[1]))
        {
            foreach ($this->all()[1] as $item)
            {
                $total += $item->price;
            }
        }

        return $total;
    }

    public function get(Product $product = null, Prescription $prescriptions = null)
    {

        if (!is_null($product))
        {
            return $this->storage->get($product->id);
        }
        if (!is_null($prescriptions))
        {
            return $this->storage->get($prescriptions->id);
        }
    }

    public function itemCount()
    {
        return $this->storage->count();
    }

    public function has(Product $product = null, Prescription $prescriptions = null)
    {
        if (!is_null($product))
        {
            return $this->storage->exists($product->id);

        }
        if (!is_null($prescriptions))
        {
            return $this->storage->exists($prescriptions->id);

        }
    }

    public function clear()
    {
        return $this->storage->clear();
    }


}
