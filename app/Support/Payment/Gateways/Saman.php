<?php


namespace App\Support\Payment\Gateways;


use App\Repositories\Models\accounting\Order;
use Illuminate\Http\Request;

class Saman implements GatewayInterface
{
    private $merchantID;
    private $callback;
    private $terminalId;
    private $cellNumber;

    public function __construct()
    {
        $this->merchantID = '123456789';
        $this->callback = route('accounting.payment.verify', $this->getName());
    }


    public function pay(Order $order)
    {
        $this->redirectToBank($order);
    }

    private function redirectToBank($order)
    {
        echo "<form id='samanpeyment' action='https://sep.shaparak.ir/payment.aspx' method='post'>
<input type='hidden' name='Amount' value='{{$order->amount}}' />
<input type='hidden' name='ResNum' value='{{$order->code}}'>
<input type='hidden' name='RedirectURL' value='{{$this->callback}}'/>
<input type='hidden' name='MID' value='{{$this->merchantID}}'/>
</form><script>document.forms['samanpeyment'].submit()</script>";


//        echo "<form id='samanpeyment' action='https://sep.shaparak.ir/payment.aspx' method='post'>
//        echo "<form id='samanpeyment' action='https://verify.sep.ir/Payments/ReferencePayment.asmx' method='post'>
//    <input type='hidden' name='Amount' value='{{$order->amount}}'>
//    <input type='hidden' name='ResNum' value='{{$order->code}}'>
//    <input type='hidden' name='RedirectURL' value='{{$this->callback}}'>
//    <input type='hidden' name='MID' value='{{$this->merchantID}}'>
//    <input type='hidden' name='cellNumber' value='{{$this->cellNumber}}'>
//    <input type='hidden' name='terminalId' value='{{$this->terminalId}}'>
//</form><script>document.forms['samanpeyment'].submit()</script>";
    }

    public function verify(Request $request)
    {
        // TODO: Implement verify() method.
    }

    public function getName(): string
    {
        return 'saman';
    }
}
