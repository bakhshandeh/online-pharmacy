<?php


namespace App\Support\Payment;


use App\Repositories\Models\accounting\Order;
use App\Repositories\Models\accounting\Payment;
use App\Support\Basket\Basket;
use App\Support\Payment\Gateways\Pasargad;
use App\Support\Payment\Gateways\Saman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Transaction
{
    private $request;
    private $basket;

    public function __construct(Request $request, Basket $basket)
    {
        $this->request = $request;
        $this->basket = $basket;
    }

    public function checkout()
    {
        DB::beginTransaction();

        try {
            $order = $this->makeOrder();

            $payment = $this->makePayment($order);

            DB::commit();
        }catch (\Exception $e)
        {
            DB::rollBack();
            return null;
        }



        if ($payment->isOnline())
        {
            $this->gatewayFactory()->pay($order);
        }


        $this->normalizeQuantity($order);

        $this->basket->clear();

        return $order;
    }

    private function normalizeQuantity($order)
    {
        if (!is_null($order->products))
        {
            foreach ($order->products as $product)
            {
                $product->decrementSumNumber($product->pivot->quantity);
            }
        }

    }


    private function gatewayFactory()
    {
        $gateway = [
            'saman' => Saman::class,
            'pasargad' => Pasargad::class,
        ][$this->request->gateway];

        return resolve($gateway);
    }

    private function makeOrder()
    {
        $transportCost = 10000;

        $order = Order::create([
            'customer_id' => auth()->user()->id,
            'code' => bin2hex(Str::random(16)),
            'amount' => $this->basket->subTotal() + $transportCost,
        ]);

//        if (!is_null($order->products))
//        {
            $order->products()->attach($this->products());
//        }

        return $order;
    }

    private function products()
    {
        if ($this->basket->all()[0]->count())
        {
            foreach ($this->basket->all()[0] as $product)
            {
                $products[$product->id] = [
                    'quantity' => $product->quantity,
                ];
            }
            if (!$this->basket->all()[1]->count())
            {
                return $products;
            }
        }
        if ($this->basket->all()[1]->count())
        {
            foreach ($this->basket->all()[1] as $prescription)
            {
                foreach ($prescription->products as $product)
                {
                    $products[$product->id] = [
                        'quantity' => $product->pivot->quantity,
                    ];
                }

            }
            return $products;
        }
        return null;
    }

    private function makePayment($order)
    {
        return Payment::create([
            'order_id' => $order->id,
            'method' => $this->request->checkout_payment_method,
//            'geteway' => $this->request->,
//            'ref_num' => $this->request->,
            'amount' => $order->amount,
//            'status' => $this->request->
        ]);
    }
}
