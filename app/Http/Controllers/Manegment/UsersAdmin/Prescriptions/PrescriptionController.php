<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Prescriptions;

use App\Http\Controllers\Controller;
use App\Repositories\Models\Prescriptions\Prescription;
use App\Repositories\Models\Prescriptions\PrescriptionProduct;
use App\Repositories\Models\Products\Category;
use App\Repositories\Models\Products\Product;
use App\Repositories\Models\Products\Sellers\Seller;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prescriptions = Prescription::all()->where('send_customer', 0);
        $prescriptions->load('files','customer');

        return view('manegment.usersAdmin.prescriptions.index',compact(['prescriptions']));
    }

    public function setPrice(Prescription $prescription)
    {

        $categoryId4 = Category::all()->where('id','==',4);
        $productsCategorId4 = $categoryId4->load(['products'])->first();

        $productsCategorId4 = $productsCategorId4->products->where('sum_number','>','0');
//        $productsCategorId4 = $productsCategorId4->products;
        $productsCategorId4->load(['files','sellers','categories']);

        $price = PrescriptionProduct::where('prescription_id', $prescription->id)->sum('price');

        $prescription->load('files','customer');
        return view('manegment.usersAdmin.prescriptions.set_price',compact(['prescription', 'productsCategorId4', 'price']));
    }
    public function storePrice(Request $request, Prescription $prescription)
    {
        $prescription->price = $request->get('price');
        $prescription->description_employee = $request->get('description_employee');
        $prescription->send_customer = true;
        $prescription->seller_id = auth()->user()->seller_id;
        $prescription->employee_id = auth()->user()->id;
        $prescription->save();

        return redirect()->route('Manegment.UsersAdmin.prescriptions.index');
    }

    public function addProduct(Prescription $prescription ,Product $product, Request $request)
    {
        $seller = Seller::findOrFail($request->post('seller_id'));

        if ($request->post('quantity') == 0)
        {
            $prescription->products()->detach($product->id);
        }else
        {
            $PrescriptionProduct_id = is_null(PrescriptionProduct::all()->last()) ? 1 : PrescriptionProduct::all()
                    ->sortBy('id')->last()->id+1;
            $check = PrescriptionProduct::where('prescription_id', $prescription->id)
                ->where('product_id', $product->id)->first();
            $price = $request->post('price') * $request->post('quantity');
            if (is_null($check))
            {
                $prescription->products()->attach($product->id, array('quantity' => $request
                    ->post('quantity'), 'seller_id' => $seller->id, 'price' => $price, 'id' => $PrescriptionProduct_id));
            }else
            {
                $check->id = $PrescriptionProduct_id;
                $check->quantity = $request->post('quantity');
                $check->seller_id = $seller->id;
                $check->price = $price;
                $check->save();
            }
        }
        return back();
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prescription $prescription)
    {
        $prescription->delete();

        flash('داده مورد نظر پاک شد.')->success();
        return redirect()->back();
    }

}
