<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Notification;

use App\Http\Controllers\Controller;
use App\Jobs\Notification\SendMail;
use App\Jobs\Notification\SendSms;
use App\Repositories\Models\User;
use App\Services\Notification\Constants\MailTypes;
use App\Services\Notification\Exceptions\UserDoesNotHavePhoneNumber;
use App\Services\Notification\Notification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * show send mail form
     */
    public function mail()
    {
        $users = User::all();
        $mailTypes = MailTypes::toString();
        return view('manegment.usersAdmin.notifications.send-mail',compact(['users', 'mailTypes']));

    }

    public function sendMail(Request $request)
    {
        $request->validate([
           'user' => 'required | integer | exists:users,id',
            'mail_type' => 'required | integer'
        ],
        [
            'user.required' => 'این فیلد نباید خالی باشد',
            'user.integer' => 'این فیلد باید عدد باشد',
            'user.exists' => 'این فیلد با مقدار جدول مورد نظر یکی نمی باشد',

            'user.required' => 'این فیلد نباید خالی باشد',
            'user.integer' => 'این فیلد باید عدد باشد',

        ]);

        try {
            $notification = resolve(Notification::class);
            $mailable = MailTypes::toMail($request->mail_type);
//            $notification->sendMail(User::find($request->user), new $mailable);
            SendMail::dispatch(User::find($request->user), new $mailable);

            flash('ایمیل با موفقیت ارسال شد.')->success();
            return redirect()->back();
        }catch (\Throwable $th){
            flash('سرویس ایمیل با مشکل مواجه شده است. لطفا دقایقی دیگر سعی نید.')->error();
            return redirect()->back();
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * show send sms form
     */
    public function sms()
    {
        $users = User::all();

        return view('manegment.usersAdmin.notifications.send-sms', compact('users'));

    }

    public function sendSms(Request $request)
    {
        $request->validate([
            'user' => 'required | integer | exists:users,id',
            'message' => 'required | string | max:256',
        ],
        [
            'user.required' => 'این فیلد نباید خالی باشد',
            'user.integer' => 'این فیلد باید عدد باشد',
            'user.exists' => 'این فیلد با مقدار جدول مورد نظر یکی نمی باشد',


            'message.required' => 'این فیلد نباید خالی باشد',
            'message.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'message.string' => 'این فیلد باید رشته باشد',

        ]);

        try {
            SendSMS::dispatch(User::find($request->user), $request->message);

            flash('پیام کوتاه با موفقیت ارسال شد.')->success();
            return redirect()->back();
        }catch (\Exception $e) {
            flash('سرویس پیام کوتاه با مشکل مواجه شده است. لطفا دقایقی دیگر سعی نید.')->error();
            return redirect()->back();
        }

//        try {
//            $notification->sendSms(User::find($request->user), $request->message);
//
//            flash('پیام کوتاه با موفقیت ارسال شد.')->success();
//            return redirect()->back();
//        }catch (UserDoesNotHavePhoneNumber $e){
//            flash('کاربر مورد نظر تلفن همراه خود را ثبت نکرده است.')->error();
//            return redirect()->back();
//        }catch (\Exception $e) {
//            flash('سرویس پیام کوتاه با مشکل مواجه شده است. لطفا دقایقی دیگر سعی نید.')->error();
//            return redirect()->back();
//        }
    }
}
