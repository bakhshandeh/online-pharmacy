<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products\Sellers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\UsersAdmin\Products\Sallers\StoreSaller;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Repositories\Models\ProvincesCities\City;
use App\Repositories\Models\ProvincesCities\Province;
use App\Repositories\Models\ProvincesCities\Quarter;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::all();
        return view('manegment.usersAdmin.products.sellers.index', compact('sellers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        $cities = City::all();
        $quarters = Quarter::all();
        return view('manegment.usersAdmin.products.sellers.create', compact(['cities', 'quarters', 'provinces']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSaller $request)
    {
        $province = Province::findOrFail($request->get('province_id'));
        $city = City::find($request->get('city_id'));
        $quarter = Quarter::find($request->get('quarter_id'));

        $seller = new Seller();
        $seller->province_id = $province->id;
        $city ? $seller->city_id = $city->id : '';
        $quarter ? $seller->quarter_id = $quarter->id : '';
        $seller->name = $request->get('name');
        $seller->persian_name = $request->get('persian_name');
        $seller->phone_number = $request->get('phone_number');
        $seller->email = $request->get('email');
        $seller->street_alley = $request->get('street_alley');
        $seller->postal_code = $request->get('postal_code');
        $seller->no = $request->get('no');
        $seller->floor = $request->get('floor');
        $seller->unit = $request->get('unit');
        $seller->save();

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.sellers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Seller $seller)
    {
        $provinces = Province::all();
        $cities = City::all();
        $quarters = Quarter::all();

        return view('Manegment.UsersAdmin.products.sellers.edit', compact(['seller', 'provinces', 'cities', 'quarters']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller)
    {
        $province = Province::findOrFail($request->get('province_id'));
        $city = City::findOrFail($request->get('city_id'));
        $quarter = Quarter::findOrFail($request->get('quarter_id'));

//        $seller = Seller::findOrFail($id);
        $seller->province_id = $province->id;
        $seller->city_id = $city->id;
        $seller->quarter_id = $quarter->id;
        $seller->name = $request->get('name');
        $seller->persian_name = $request->get('persian_name');
        $seller->phone_number = $request->get('phone_number');
        $seller->email = $request->get('email');
        $seller->street_alley = $request->get('street_alley');
        $seller->postal_code = $request->get('postal_code');
        $seller->no = $request->get('no');
        $seller->floor = $request->get('floor');
        $seller->unit = $request->get('unit');
        $seller->save();

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.sellers.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller)
    {
        $seller->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
