<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products\Manufacturers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\UsersAdmin\Products\Manufacturers\StoreManufacturers;
use App\Repositories\Models\Products\manufacturers\Manufacturer;
use App\Repositories\Models\ProvincesCities\City;
use App\Repositories\Models\ProvincesCities\Province;
use App\Repositories\Models\ProvincesCities\Quarter;
use Illuminate\Http\Request;

class ManufacturerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manufacturers = Manufacturer::all();
        return view('manegment.usersAdmin.products.manufacturers.index', compact('manufacturers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        $cities = City::all();
        $quarters = Quarter::all();
        return view('manegment.usersAdmin.products.manufacturers.create', compact(['cities', 'quarters', 'provinces']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreManufacturers $request)
    {
        $province = Province::findOrFail($request->get('province_id'));
        $city = City::find($request->get('city_id'));
        $quarter = Quarter::find($request->get('quarter_id'));

        $manufacturer = new Manufacturer();
        $manufacturer->province_id = $province->id;
        $city ? $manufacturer->city_id = $city->id : '';
        $quarter ? $manufacturer->quarter_id = $quarter->id : '';
        $manufacturer->name = $request->get('name');
        $manufacturer->persian_name = $request->get('persian_name');
        $manufacturer->phone_number = $request->get('phone_number');
        $manufacturer->email = $request->get('email');
        $manufacturer->street_alley = $request->get('street_alley');
        $manufacturer->postal_code = $request->get('postal_code');
        $manufacturer->no = $request->get('no');
        $manufacturer->floor = $request->get('floor');
        $manufacturer->unit = $request->get('unit');
        $manufacturer->save();

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.manufacturers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Manufacturer $manufacturer)
    {
        $provinces = Province::all();
        $cities = City::all();
        $quarters = Quarter::all();

        return view('Manegment.UsersAdmin.products.manufacturers.edit', compact(['manufacturer', 'provinces', 'cities', 'quarters']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Manufacturer $manufacturer)
    {
        $province = Province::findOrFail($request->get('province_id'));
        $city = City::findOrFail($request->get('city_id'));
        $quarter = Quarter::findOrFail($request->get('quarter_id'));

//        $manufacturer = Manufacturer::findOrFail($id);
        $manufacturer->province_id = $province->id;
        $manufacturer->city_id = $city->id;
        $manufacturer->quarter_id = $quarter->id;
        $manufacturer->name = $request->get('name');
        $manufacturer->persian_name = $request->get('persian_name');
        $manufacturer->phone_number = $request->get('phone_number');
        $manufacturer->email = $request->get('email');
        $manufacturer->street_alley = $request->get('street_alley');
        $manufacturer->postal_code = $request->get('postal_code');
        $manufacturer->no = $request->get('no');
        $manufacturer->floor = $request->get('floor');
        $manufacturer->unit = $request->get('unit');
        $manufacturer->save();

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.manufacturers.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Manufacturer $manufacturer)
    {
        $manufacturer->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
