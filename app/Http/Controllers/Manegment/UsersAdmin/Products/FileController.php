<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\FileUpload\UploadMultipleFiles;
use App\Repositories\Models\Products\Product_File;
use App\Services\Uploader\Uploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function Sodium\compare;

class FileController extends Controller
{
    private $uploader;
    /**
     * FileController constructor.
     * @param Uploader $uploader
     */
    public function __construct(Uploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $files = Product_File::all();
        return view('manegment.usersAdmin.products.products.files.index', compact(['files']));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manegment.usersAdmin.products.products.files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadMultipleFiles $request)
    {
        try
        {
            $this->uploader->
            upload($request, null);

            flash('اطلاعات با موفقیت ذخیره.')->success();
            return redirect()->back();
        }
        catch (\Exception $e)
        {
            flash('آپلود فایل صورت نگرفت چون این فایل قبلا آپلود شده است.')->error();
            return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product_File $file)
    {
        return $file->download();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_File $file)
    {
        $file->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
