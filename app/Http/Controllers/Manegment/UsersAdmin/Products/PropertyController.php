<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products;

use App\Http\Controllers\Controller;
use App\Repositories\Models\Products\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $properties = Property::all();
        return view('manegment.usersAdmin.products.properties.index', compact('properties'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manegment.usersAdmin.products.properties.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required | string | max:256|unique:properties',
        ],
            [
                'name.required' => 'این فیلد نباید خالی باشد',
                'name.string' => 'این فیلد باید رشته باشد',
                'name.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
                'name.unique' => 'نام وارد شده تکراری می باشد. نام جدید وارد کنید',


            ]);
        $property = Property::create($request->only(['name']));

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.properties.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Property $property)
    {
        return view('manegment.usersAdmin.products.properties.edit', compact(['property']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        $request->validate([
            'name' => 'required | string | max:256|unique:properties',
        ],
            [
                'name.required' => 'این فیلد نباید خالی باشد',
                'name.string' => 'این فیلد باید رشته باشد',
                'name.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
                'name.unique' => 'نام وارد شده تکراری می باشد. نام جدید وارد کنید',


            ]);

        $property->update($request->only(['name']));

        flash('اطلاعات با موفقیت به روزرسانی شد.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.properties.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        $property->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
