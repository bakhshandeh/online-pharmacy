<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\UsersAdmin\Products\StoreProducts;
use App\Repositories\Models\Products\Category;
use App\Repositories\Models\Products\manufacturers\Manufacturer;
use App\Repositories\Models\Products\Product;
use App\Repositories\Models\Products\Product_File;
use App\Repositories\Models\Products\ProductProperty;
use App\Repositories\Models\Products\ProductSeller;
use App\Repositories\Models\Products\Property;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Repositories\Models\Products\Type;
use App\Services\Uploader\Uploader;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProductController extends Controller
{
    private $uploader;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd("h");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manufacturers = Manufacturer::all();
        $sellers = Seller::all();
        $categories = Category::all();
        $types = Type::all();
        $properties = Property::all();

        return view('manegment.usersAdmin.products.products.create', compact(['manufacturers','sellers','categories', 'types', 'properties']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProducts $request , Uploader $uploader)
    {

        $manufacturersInProduct ="";
        $manufacturers = $this->getAllManufacturers($request->get('manufacturers_id'));
        foreach ($manufacturers as $manufacturer)
        {
            $manufacturersInProduct = $manufacturersInProduct .'*'. $manufacturer->name;
        }
//-------------
        $product = new Product();
        $product->id = is_null(Product::all()->last()) ? 50000 : Product::all()->sortBy('id')->last()->id+1;
        $product->user_id = auth()->user()->id;
        $product->name = $request->get('name');
        $product->persian_name = $request->get('persian_name');
        $product->generic_name = $request->get('generic_name');
        $product->manufacturer = $manufacturersInProduct;
        $product->amount = $request->get('amount');
        $product->description = $request->get('description');
        $product->sell_with_prescription = is_null($request->get('sell_with_prescription')) ? 0 : 1;
        $product->save();
////-------------
        $product = Product::all()->sortBy('id')->last();
//
        $product->giveManufacturersTo($request->get('manufacturers_id'));
        $product->giveCategoriesTo($request->get('categories_id'));
        $product->giveTypesTo($request->get('types_id'));
//-------------
        $property_values = $request->get('property_values');
        $properties = $this->getAllProperties($request->get('properties_id'));
        foreach ($properties as $key => $property)
        {
            $product_property_id = is_null(ProductProperty::all()->last()) ? 1 : ProductProperty::all()->sortBy('id')->last()->id+1;
            $product->properties()->attach($property->id, ['id' => $product_property_id ,'value' => $property_values[$key]]);
        }
//-------------
        if ((boolean) $request->allFiles())
        {
            $this->uploader = $uploader;
            $modelTableFile = Product_File::class;


            try
            {
                $this->uploader->uploadFor($request, $product, $modelTableFile);
            }
            catch (\Exception $e)
            {
                flash('آپلود فایل صورت نگرفت چون این فایل قبلا آپلود شده است.')->error();
                return redirect()->back();
            }
        }

        flash('محصول ثبت شد.')->success();
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }

    public function addInventoryCreate()
    {
        $products = Product::all()->sortBy('name');
        return view('manegment.usersAdmin.products.products.add_inventory.create', compact(['products']));

    }

    public function addInventoryStore(Request $request)
    {
        $request->validate([
            'number' => 'required | integer | min:0',
            'seller_price' => 'required | integer | min:0',
            'seller_purchase_price' => 'required | integer | min:0',
        ],
            [
                'number.required' => 'این فیلد نباید خالی باشد',
                'number.integer' => 'این فیلد باید عدد باشد',
                'number.min' => 'این فیلد حداقل باید ۰ باشد',

                'seller_price.required' => 'این فیلد نباید خالی باشد',
                'seller_price.integer' => 'این فیلد باید عدد باشد',
                'seller_price.min' => 'این فیلد حداقل باید ۰ باشد',

                'seller_purchase_price.required' => 'این فیلد نباید خالی باشد',
                'seller_purchase_price.integer' => 'این فیلد باید عدد باشد',
                'seller_purchase_price.min' => 'این فیلد حداقل باید ۰ باشد',

            ]);


        $seller_id = auth()->user()->seller_id;

        $product = Product::findOrFail($request->get('products_id'));

        $product_seller = ProductSeller::where('seller_id',$seller_id)->where('product_id',$product->id)->first();

        if (is_null($product_seller))
        {
            $sum_number = $product->sum_number + $request->get('number');
            $product->update(['sum_number' => $sum_number]);

            $profit = $request->get('seller_price') - $request->get('seller_purchase_price');
            $profit = $profit * $request->get('number');
            $id = is_null(ProductSeller::all()->last()) ? 1 : ProductSeller::all()->sortBy('id')->last()->id+1;
            $product->sellers()->attach($seller_id, ['id' => $id,'number' => $request->get('number'),'seller_price' => $request->get('seller_price'),
                'seller_purchase_price' => $request->get('seller_purchase_price'),'profit' => $profit]);
        }
        else
        {
            $sum_number = $product->sum_number + $request->get('number');
            $product->update(['sum_number' => $sum_number]);

            $number = $product_seller->number + $request->get('number');
            $profit = $request->get('seller_price') - $request->get('seller_purchase_price');
            $profit = $profit * $request->get('number');
            $profit = $product_seller->profit + $profit;


            $product_seller->number = $number;
            $product_seller->seller_price = $request->get('seller_price');
            $product_seller->seller_purchase_price = $request->get('seller_purchase_price');
            $product_seller->profit = $profit;
            $product_seller->save();

        }

        return back();
    }

    protected function getAllProperties(array $properties)
    {
        return Property::whereIn('id',Arr::flatten($properties))->get();
    }
    protected function getAllManufacturers(array $manufacturers)
    {
        return Manufacturer::whereIn('id',Arr::flatten($manufacturers))->get();
    }
}
