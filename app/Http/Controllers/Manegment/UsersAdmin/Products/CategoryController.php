<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products;

use App\Http\Controllers\Controller;
use App\Repositories\Models\Products\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('manegment.usersAdmin.products.categories.index', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manegment.usersAdmin.products.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required | string | max:256|unique:categories',
        ],
            [
                'name.required' => 'این فیلد نباید خالی باشد',
                'name.string' => 'این فیلد باید رشته باشد',
                'name.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
                'name.unique' => 'نام وارد شده تکراری می باشد. نام جدید وارد کنید',


            ]);

        $category = Category::create($request->only(['name']));

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('manegment.usersAdmin.products.categories.edit', compact(['category']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required | string | max:256|unique:categories',
        ],
            [
                'name.required' => 'این فیلد نباید خالی باشد',
                'name.string' => 'این فیلد باید رشته باشد',
                'name.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
                'name.unique' => 'نام وارد شده تکراری می باشد. نام جدید وارد کنید',


            ]);

        $category->update($request->only(['name']));

        flash('اطلاعات با موفقیت به روزرسانی شد.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
