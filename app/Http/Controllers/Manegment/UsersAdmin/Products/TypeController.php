<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Products;

use App\Http\Controllers\Controller;
use App\Repositories\Models\Products\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all();
        return view('manegment.usersAdmin.products.types.index', compact('types'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manegment.usersAdmin.products.types.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required | string | max:256| |unique:types',
        ],
            [
                'name.required' => 'این فیلد نباید خالی باشد',
                'name.string' => 'این فیلد باید رشته باشد',
                'name.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
                'name.unique' => 'نام وارد شده تکراری می باشد. نام جدید وارد کنید',

            ]);

        $type = Type::create($request->only(['name']));

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.types.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        return view('manegment.usersAdmin.products.types.edit', compact(['type']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $request->validate([
            'name' => 'required | string | max:256 |unique:types',
        ],
            [
                'name.required' => 'این فیلد نباید خالی باشد',
                'name.string' => 'این فیلد باید رشته باشد',
                'name.max' => 'این فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
                'name.unique' => 'نام وارد شده تکراری می باشد. نام جدید وارد کنید',

            ]);

        $type->update($request->only(['name']));

        flash('اطلاعات با موفقیت به روزرسانی شد.')->success();
        return redirect()->route('Manegment.UsersAdmin.products.types.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $type->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
