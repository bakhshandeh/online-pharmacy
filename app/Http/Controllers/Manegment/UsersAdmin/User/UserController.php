<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\User;

use App\Http\Controllers\Controller;
use App\Repositories\Models\Permission;
use App\Repositories\Models\Role;
use App\Repositories\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')->get();
        return view('manegment.usersAdmin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

    }


    public function editRolesPermissionsUser(User $user)
    {
        $roles = Role::all();
        $permissions = Permission::all();

        $user->load(['roles', 'permissions']);
        return view('manegment.usersAdmin.users.edit', compact(['user', 'permissions', 'roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
    }

    public function updateRolesPermissionsUser(Request $request, User $user)
    {

        $user->refreshPermissions($request->permissions);
        $user->refreshRoles($request->roles);
        $user->seller_id = 1;
        $user->save();

        flash('اطلاعات با موفقیت تغییر یافت.')->success();
        return redirect()->route('Manegment.UsersAdmin.users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }


}
