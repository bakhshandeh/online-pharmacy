<?php

namespace App\Http\Controllers\Manegment\UsersAdmin\Role;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\UsersAdmin\Role\StoreRole;
use App\Repositories\Models\Permission;
use App\Repositories\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::with(['permissions'])->get();
        return view('manegment.usersAdmin.roles.index', compact('roles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all()->sortBy('persian_name');
        return view('manegment.usersAdmin.roles.create', compact('permissions'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRole $request)
    {
//        dd($request->all());
//        dd($request->get('permissions'));
        $role = Role::create($request->only(['name', 'persian_name']));

        $role->givePermissionsTo($request->get('permissions'));

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return back();    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $permissions = Permission::all()->sortBy('persian_name');
        $role->load('Permissions');

        return view('manegment.usersAdmin.roles.edit', compact(['role', 'permissions']));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
//        $role->update($request->only(['name', 'persian_name']));

        $role->refreshPermissions($request->permissions);

        flash('اطلاعات با موفقیت به روزرسانی شد.')->success();
        return redirect()->route('Manegment.UsersAdmin.roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }
}
