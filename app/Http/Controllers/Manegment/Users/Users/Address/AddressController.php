<?php

namespace App\Http\Controllers\Manegment\Users\Users\Address;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\Users\Users\Address;
use App\Repositories\Models\ProvincesCities\City;
use App\Repositories\Models\ProvincesCities\Province;
use App\Repositories\Models\ProvincesCities\Quarter;
use App\Repositories\Models\Users\Address_User;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $backReferer =$request->get('redirect');

        $provinces = Province::all();
        $cities = City::all();
        $quarters = Quarter::all();
        return view('manegment.users.users.address.create', compact(['cities', 'quarters', 'provinces', 'backReferer']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Address $request)
    {
        $province = Province::findOrFail($request->get('province_id'));
        $city = City::find($request->get('city_id'));
        $quarter = Quarter::find($request->get('quarter_id'));

        $addressUser = new Address_User();
        $addressUser->user_id = auth()->user()->id;
        $addressUser->province_id = $province->id;
        $city ? $addressUser->city_id = $city->id : '';
        $quarter ? $addressUser->quarter_id = $quarter->id : '';
        $addressUser->recipient_first_name = $request->get('recipient_first_name');
        $addressUser->recipient_last_name = $request->get('recipient_last_name');
        $addressUser->recipient_phone_number = $request->get('recipient_phone_number');
        $addressUser->recipient_national_code = $request->get('recipient_national_code');
        $addressUser->street_alley = $request->get('street_alley');
        $addressUser->postal_code = $request->get('postal_code');
        $addressUser->no = $request->get('no');
        $addressUser->floor = $request->get('floor');
        $addressUser->unit = $request->get('unit');
        $addressUser->save();

        if ($request->get('backReferer') == 'basket')
        {
            flash('اطلاعات با موفقیت ذخیره.')->success();
            return redirect()->route('basket.checkout.form');
        }

        flash('اطلاعات با موفقیت ذخیره.')->success();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
