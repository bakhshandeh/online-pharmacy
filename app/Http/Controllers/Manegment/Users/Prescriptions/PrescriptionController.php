<?php

namespace App\Http\Controllers\Manegment\Users\Prescriptions;

use App\Http\Controllers\Controller;
use App\Http\Requests\Manegment\Users\Prescriptions\StorePrescription;
use App\Repositories\Models\Prescriptions\Prescription;
use App\Repositories\Models\Prescriptions\Prescription_File;
use App\Services\Uploader\Uploader;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    private $uploader;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manegment.users.prescriptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePrescription $request, Uploader $uploader)
    {
        $prescription = new Prescription();
        $prescription->customer_id = auth()->user()->id;
        is_null($request->get('doctor_name')) ? '' : $prescription->doctor_name = $request->get('doctor_name');
        $prescription->sick_name = $request->get('sick_name');
        is_null($request->get('national_code')) ? $prescription->national_code = $request->get('national_code') : '';
//        is_null($request->get('insurance_type')) ? '' : $prescription->insurance_type = $request->get('insurance_type');
//        is_null($request->get('insurance_validity_date')) ? '' : $prescription->insurance_validity_date = $request->get('insurance_validity_date');
        $prescription->date = $request->get('date');
        is_null($request->get('description_customer')) ? '' : $prescription->description_customer = $request->get('description_customer');
        $prescription->save();

        $prescription = Prescription::all()->sortBy('id')->last();


        if ((boolean) $request->allFiles())
        {
            $this->uploader = $uploader;
            $modelTableFile = Prescription_File::class;

            try
            {
                $this->uploader->uploadFor($request, $prescription, $modelTableFile);
            }
            catch (\Exception $e)
            {
                flash('آپلود فایل صورت نگرفت چون این فایل قبلا آپلود شده است.')->error();
                return redirect()->back();
            }
        }

        flash('نسخه ثبت شد.')->success();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prescription $prescription)
    {
        $prescription->delete();

        flash('داده مورد نظر از پاک شد.')->success();
        return redirect()->back();
    }

    public function listSetPrice()
    {
        $prescriptions = Prescription::all()->where('customer_id',auth()->user()->id)
            ->where('send_customer',1)->where('add_to_cart', 0);
        return view('manegment.users.prescriptions.list_set_price', compact(['prescriptions']));

    }

    public function cancel(Prescription $prescription)
    {
        $prescription->add_to_cart = 4;
        $prescription->save();

        flash('خرید نسخه با کد: '.$prescription->id.' لغو شد. ')->success();
        return back();

    }

}
