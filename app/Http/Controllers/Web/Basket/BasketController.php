<?php

namespace App\Http\Controllers\Web\Basket;

use App\Exceptions\QuantityExceededException;
use App\Http\Controllers\Controller;
use App\Repositories\Models\Prescriptions\Prescription;
use App\Repositories\Models\Products\Product;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Repositories\Models\Users\Address_User;
use App\Support\Basket\Basket;
use App\Support\Payment\Transaction;
use Illuminate\Http\Request;

class BasketController extends Controller
{
    private $basket;
    private $transaction;

    public function __construct(Basket $basket, Transaction $transaction)
    {
        $this->middleware('auth')->only(['checkOutForm', 'checkOut']);
        $this->basket = $basket;
        $this->transaction = $transaction;
    }

    public function add( $product = null,  $prescription = null,  $seller= null)
    {

        try {
            if ($product != 0)
            {
                $prescription = null;
                $product = Product::findOrFail($product);
                $seller = Seller::findOrFail($seller);

                $this->basket->add($product, $prescription ,1, $seller);
                flash('محصول مورد نظر شما به سبد خرید افزوده شد.')->success();
                return back();
            }
            if ($prescription != 0)
            {
                $product = null;
                $seller = null;
                $prescription = Prescription::findOrFail($prescription);

                $this->basket->add($product,$prescription,0,$seller);

                $prescription->add_to_cart = 1;
                $prescription->save();

                flash('نسخه مورد نظر شما به سبد خرید افزوده شد.')->success();
                return back();
            }


        }catch (QuantityExceededException $exception)
        {
            flash('محصول مورد نظر به تعدادی که شما درخواست داده اید موجود نیست.')->error();
            return back();
        }
    }


    public function index()
    {
        $items = $this->basket->all();

        return view('web.basket.index',compact(['items']));

    }

    public function update(Request $request, Product $product, Seller $seller)
    {
        $this->basket->update($product,null, $request->quantity, $seller);
        return back();
    }

    public function checkOutForm()
    {
        $addressUser = Address_User::all()->where('user_id', auth()->user()->id)->last();
        return view('web.basket.checkOut', compact(['addressUser']));
    }

    public function checkOut(Request $request)
    {
        $this->validateForm($request);

        $order = $this->transaction->checkout();


        flash( 'سفارش شما به شماره :'.$order->id .'ثبت شد.' )->success();
        return redirect()->route('products.index',['category' => 3]);
    }

    private function validateForm($request)
    {
        $request->validate([
            'checkout_payment_method' => ['required'],
            'gateway' => ['required_if:checkout_payment_method,online'],
            'active-address' => ['required']
        ],[
            'active-address.required' => 'آدرس الزامی میباشد.'
        ]);
    }


}
