<?php

namespace App\Http\Controllers\Web\Products;

use App\Http\Controllers\Controller;
use App\Repositories\Models\Products\Category;
use App\Repositories\Models\Products\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $categories = Category::all()->where('id',$category->id);
        $products = $categories->load(['products'])->first();

        $products = $products->products;
        $products->load(['files','sellers','categories']);

        return view('web.products.index', compact(['products']));
    }

}
