<?php

namespace App\Http\Requests\Manegment\FileUpload;

use Illuminate\Foundation\Http\FormRequest;

class UploadMultipleFiles extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file.*' => 'required|mimetypes:video/avi,video/mp4,video/quicktime,image/jpeg,application/zip,application/pdf'
        ];
    }

    public function messages()
    {
        return [
            'file.*.required' => 'فایل نباید خالی باشد',
            'file.*.mimetypes' => 'هیچ فایلی ذخیره نشد چون نوع فایل/فایل های انتخابی صحیح نمیباشد.',
        ];
    }
}
