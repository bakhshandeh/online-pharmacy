<?php

namespace App\Http\Requests\Manegment\Users\Prescriptions;

use Illuminate\Foundation\Http\FormRequest;

class StorePrescription extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimetypes:image/jpeg,image/jpg',
            'doctor_name' => 'nullable|string|max:256',
            'sick_name' => 'required|string|max:256',
            'national_code' => 'nullable|min:0|',
            'insurance_type' => 'nullable|string|max:256',
            'insurance_validity_date' => 'nullable',
            'date' => 'required',
            'description_customer' => 'nullable|string',

        ];
    }

    public function messages()
    {
        return [
            'file.required' => 'فایل نباید خالی باشد',
            'file.mimetypes' => ' فایل ذخیره نشد چون نوع فایل انتخابی صحیح نمیباشد(jpeg, jpg).',
            'file.size' => ' فایل حداکثر باید ۲۰۰ کیلوبایت باشد.',

//            'doctor_name.required' => 'فیلد نباید خالی باشد',
            'doctor_name.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'doctor_name.string' => 'فیلد باید رشته باشد',

            'sick_name.required' => 'فیلد نباید خالی باشد',
            'sick_name.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'sick_name.string' => 'فیلد باید رشته باشد',

            'national_code.integer' => 'فیلد باید عدد باشد',
            'national_code.max' => 'فیلد باید حداکثر ۱۰ کاراکترعدد باشد',
            'national_code.min' => 'فیلد باید بزرگتر ار ۰ باشد',

//            'generic_name.required' => 'فیلد نباید خالی باشد',
            'insurance_type.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'insurance_type.string' => 'فیلد باید رشته باشد',
//            'generic_name.unique' => 'فیلد تکراری می باشد',

            'insurance_validity_date.required' => 'فیلد نباید خالی باشد',
            'insurance_validity_date.date' => 'فیلد باید تاریخ باشد',

            'date.required' => 'فیلد نباید خالی باشد',
            'date.date' => 'فیلد باید تاریخ باشد',

            'description_customer.string' => 'فیلد باید رشته باشد',

        ];
    }

}
