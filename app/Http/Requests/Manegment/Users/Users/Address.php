<?php

namespace App\Http\Requests\Manegment\Users\Users;

use Illuminate\Foundation\Http\FormRequest;

class Address extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'province_id' => 'required|min:1|numeric',
            'city_id' => 'nullable|min:1|numeric',
            'quarter_id' => 'nullable|min:1|numeric',
            'street_alley' => 'required|string|max:200',
            'postal_code' => 'required|numeric',
            'no' => 'required|numeric',
            'floor' => 'nullable|numeric',
            'unit' => 'nullable|numeric',
            'recipient_first_name' => 'required|string',
            'recipient_last_name' => 'required|string',
            'recipient_phone_number' => 'required|numeric',
            'recipient_national_code' => 'required|numeric',

        ];
    }

    public function messages()
    {
        return [
            'province_id.required' => 'این فیلد نباید خالی باشد',
            'province_id.numeric' => 'این فیلد باید عدد باشد',
            'province_id.min' => 'مقدار انتخاب شده صحیح نمیباشد',

//            'city_id.required' => 'این فیلد نباید خالی باشد',
            'city_id.numeric' => 'این فیلد باید عدد باشد',
            'city_id.min' => 'مقدار انتخاب شده صحیح نمیباشد',

//            'quarter_id.required' => 'این فیلد نباید خالی باشد',
            'quarter_id.numeric' => 'این فیلد باید عدد باشد',
            'quarter_id.min' => 'مقدار انتخاب شده صحیح نمیباشد',



            'street_alley.required' => 'این فیلد نباید خالی باشد',
            'street_alley.string' => 'این فیلد باید رشته باشد',
            'street_alley.max' => 'این فیلد حداکثر باید ۲۰۰ کاراکتر باشد',

            'postal_code.required' => 'این فیلد نباید خالی باشد',
            'postal_code.numeric' => 'این فیلد باید عدد باشد',

            'no.required' => 'این فیلد نباید خالی باشد',
            'no.numeric' => 'این فیلد باید عدد باشد',

            'floor.numeric' => 'این فیلد باید عدد باشد',

            'unit.numeric' => 'این فیلد باید عدد باشد',


            'recipient_first_name.required' => 'این فیلد نباید خالی باشد',
            'recipient_first_name.string' => 'این فیلد باید رشته باشد',

            'recipient_last_name.required' => 'این فیلد نباید خالی باشد',
            'recipient_last_name.string' => 'این فیلد باید رشته باشد',


            'recipient_national_code.required' => 'این فیلد نباید خالی باشد',
//            'recipient_national_code.min' => 'این فیلد حداقل باید ۱۰ کاراکتر باشد',
//            'recipient_national_code.max' => 'این فیلد حداکثر باید ۱۰ کاراکتر باشد',
            'recipient_national_code.numeric' => 'این فیلد باید عدد باشد',

            'recipient_phone_number.required' => 'این فیلد نباید خالی باشد',
//            'recipient_phone_number.max' => 'این فیلد حداکثر باید ۱۳ کاراکتر باشد',
            'recipient_phone_number.numeric' => 'این فیلد باید عدد باشد',


        ];
    }
}
