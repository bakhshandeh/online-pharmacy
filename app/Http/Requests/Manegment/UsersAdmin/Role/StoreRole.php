<?php

namespace App\Http\Requests\Manegment\UsersAdmin\Role;

use Illuminate\Foundation\Http\FormRequest;

class StoreRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:256|unique:roles',
            'persian_name' => 'required|string|max:256|unique:roles',
            'permissions' => 'array|min:1',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'فیلد نام نقش نباید خالی باشد',
            'name.max' => 'فیلد نام نقش حداکثر باید ۱۰۰ کاراکتر باشد',
            'name.string' => 'فیلد نام نقش باید ۱۰۰رشته باشد',
            'name.unique' => 'فیلد نام نقش تکراری می باشد',

            'persian_name.required' => 'فیلد نام نقش به فارسی نباید خالی باشد',
            'persian_name.max' => 'فیلد نام نقش به فارسی حداکثر باید ۱۰۰ کاراکتر باشد',
            'persian_name.string' => 'فیلد نام نقش به فارسی باید ۱۰۰رشته باشد',
            'persian_name.unique' => 'فیلد نام نقش به فارسی تکراری می باشد',

            'permissions.array' => 'کد مترجمان انتخاب شده صحیح نمیباشد',
            'permissions.min' => 'کد مترجمان انتخاب شده صحیح نمیباشد',
        ];
    }

}
