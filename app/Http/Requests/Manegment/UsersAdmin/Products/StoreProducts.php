<?php

namespace App\Http\Requests\Manegment\UsersAdmin\Products;

use Illuminate\Foundation\Http\FormRequest;

class StoreProducts extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file.*' => 'nullable|mimetypes:video/avi,video/mp4,video/quicktime,image/jpeg,application/zip,application/pdf',
            'name' => 'required|string|max:256',
            'persian_name' => 'nullable|string|max:256',
            'generic_name' => 'nullable|string|max:256',
            'amount' => 'required|string|max:256',
            'description' => 'nullable|string',
            'sell_with_prescription' => 'boolean',


            'manufacturers_id' => 'required|exists:manufacturers,id',
            'categories_id' => 'required|exists:categories,id',
            'types_id' => 'required|exists:types,id',
            'properties_id' => 'required|exists:properties,id',
            'property_values' => 'required',


        ];
    }

    public function messages()
    {
        return [
            'file.*.required' => 'فایل نباید خالی باشد',
            'file.*.mimetypes' => 'هیچ فایلی ذخیره نشد چون نوع فایل/فایل های انتخابی صحیح نمیباشد.',

            'name.required' => 'فیلد نباید خالی باشد',
            'name.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'name.string' => 'فیلد باید رشته باشد',
//            'name.unique' => 'فیلد تکراری می باشد',

//            'persian_name.required' => 'فیلد نباید خالی باشد',
            'persian_name.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'persian_name.string' => 'فیلد باید رشته باشد',
//            'persian_name.unique' => 'فیلد تکراری می باشد',

//            'generic_name.required' => 'فیلد نباید خالی باشد',
            'generic_name.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'generic_name.string' => 'فیلد باید رشته باشد',
//            'generic_name.unique' => 'فیلد تکراری می باشد',

            'description.string' => 'فیلد باید رشته باشد',


            'amount.required' => 'فیلد نباید خالی باشد',
            'amount.max' => 'فیلد حداکثر باید ۲۵۶ کاراکتر باشد',
            'amount.string' => 'فیلد باید رشته باشد',

            'sell_with_prescription.boolean' => 'فیلد ۰ و۱ باشد',


            'manufacturers_id.required' => 'این فیلد نباید خالی باشد',
            'manufacturers_id.exists' => 'این فیلد با مقدار جدول مورد نظر یکی نمی باشد',
            'manufacturers_id.*.integer' => 'این فیلد باید عدد باشد',

            'categories_id.required' => 'این فیلد نباید خالی باشد',
            'categories_id.exists' => 'این فیلد با مقدار جدول مورد نظر یکی نمی باشد',
            'categories_id.*.integer' => 'این فیلد باید عدد باشد',

            'types_id.required' => 'این فیلد نباید خالی باشد',
            'types_id.exists' => 'این فیلد با مقدار جدول مورد نظر یکی نمی باشد',
            'types_id.*.integer' => 'این فیلد باید عدد باشد',

            'properties_id.required' => 'این فیلد نباید خالی باشد',
            'properties_id.exists' => 'این فیلد با مقدار جدول مورد نظر یکی نمی باشد',
            'properties_id.*.integer' => 'این فیلد باید عدد باشد',

            'property_values.required' => 'این فیلد نباید خالی باشد',


        ];
    }
}
