<?php

namespace App\Http\Requests\Manegment\UsersAdmin\Products\Sallers;

use Illuminate\Foundation\Http\FormRequest;

class StoreSaller extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:sellers',
            'persian_name' => 'required|string|unique:sellers',
            'phone_number' => 'nullable|numeric',
            'email' => 'nullable|email',
            'score' => 'nullable|numeric',
            'province_id' => 'min:1|numeric',
            'city_id' => 'min:1|numeric',
            'quarter_id' => 'min:1|numeric',
            'street_alley' => 'nullable|string|max:200',
            'postal_code' => 'nullable|numeric',
            'no' => 'nullable|numeric',
            'floor' => 'nullable|numeric',
            'unit' => 'nullable|numeric',


        ];
    }

    public function messages()
    {
        return [


            'name.required' => 'این فیلد نباید خالی باشد',
            'name.string' => 'این فیلد باید رشته باشد',
            'name.unique' => 'این فیلد نباید تکراری باشد',

            'persian_name.required' => 'این فیلد نباید خالی باشد',
            'persian_name.string' => 'این فیلد باید رشته باشد',
            'persian_name.unique' => 'این فیلد نباید تکراری باشد',

            'phone_number.numeric' => 'این فیلد باید عدد باشد',

            'email.email' => 'ایمیل وارد کنید.(test@yahoo.com)',

            'score.numeric' => 'این فیلد باید عدد باشد',

            'province_id.required' => 'این فیلد نباید خالی باشد',
            'province_id.numeric' => 'این فیلد باید عدد باشد',
            'province_id.min' => 'مقدار انتخاب شده صحیح نمیباشد',

            'city_id.required' => 'این فیلد نباید خالی باشد',
            'city_id.numeric' => 'این فیلد باید عدد باشد',
            'city_id.min' => 'مقدار انتخاب شده صحیح نمیباشد',

            'quarter_id.required' => 'این فیلد نباید خالی باشد',
            'quarter_id.numeric' => 'این فیلد باید عدد باشد',
            'quarter_id.min' => 'مقدار انتخاب شده صحیح نمیباشد',

            'street_alley.required' => 'این فیلد نباید خالی باشد',
            'street_alley.string' => 'این فیلد باید رشته باشد',
            'street_alley.max' => 'این فیلد حداکثر باید ۲۰۰ کاراکتر باشد',

            'postal_code.numeric' => 'این فیلد باید عدد باشد',

            'no.required' => 'این فیلد نباید خالی باشد',
            'no.numeric' => 'این فیلد باید عدد باشد',

            'floor.numeric' => 'این فیلد باید عدد باشد',

            'unit.numeric' => 'این فیلد باید عدد باشد',

        ];
    }

}
