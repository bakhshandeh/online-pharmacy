<?php

namespace App\Http\Requests\Manegment\UsersAdmin\Permission;

use Illuminate\Foundation\Http\FormRequest;

class StorePermission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:256|unique:permissions',
            'persian_name' => 'required|string|max:256|unique:permissions',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'فیلد نام سطح دسترسی نباید خالی باشد',
            'name.max' => 'فیلد نام سطح دسترسی حداکثر باید ۱۰۰ کاراکتر باشد',
            'name.string' => 'فیلد نام سطح دسترسی باید ۱۰۰رشته باشد',
            'name.unique' => 'فیلد نام سطح دسترسی تکراری می باشد',

            'persian_name.required' => 'فیلد نام سطح دسترسی به فارسی نباید خالی باشد',
            'persian_name.max' => 'فیلد نام سطح دسترسی به فارسی حداکثر باید ۱۰۰ کاراکتر باشد',
            'persian_name.string' => 'فیلد نام سطح دسترسی به فارسی باید ۱۰۰رشته باشد',
            'persian_name.unique' => 'فیلد نام سطح دسترسی به فارسی تکراری می باشد',

        ];
    }
}
