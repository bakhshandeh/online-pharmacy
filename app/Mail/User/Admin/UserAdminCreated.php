<?php

namespace App\Mail\User\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserAdminCreated extends Mailable
{
    use Queueable, SerializesModels;

//    set property only public for viwe example: public avg;
//    set property only  for myclass example: private firstname;
//    set property only  for myclass example: private lastname;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        // set data property example: this->firstName='ali'
        // set data property example: this->lastName='bh'
        // set data property example: this->avg=28
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
            return $this->view('manegment.mails.users.register');
//            ->with(
//                'fullName' => $this->firstName ." ". $this->lastName,
//    );
    }
}
