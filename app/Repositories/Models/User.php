<?php

namespace App\Repositories\Models;

use App\Repositories\Models\Prescriptions\Prescription;
use App\Repositories\Models\Products\Product;
use App\Services\Permission\Traits\HasPermissions;
use App\Services\Permission\Traits\HasRoles;
use App\Services\Prescriptions\Traits\HasPrescriptions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable , HasPermissions, HasRoles , HasPrescriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seller_id', 'first_name', 'last_name', 'national_code', 'date_birth', 'job', 'card_number',
        'phone_number', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_number_verified_at' => 'datetime',
        'date_birth' => 'datetime',
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class)->withPivot('value');
    }
}
