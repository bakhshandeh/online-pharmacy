<?php

namespace App\Repositories\Models\Products;

use Illuminate\Database\Eloquent\Model;

class ProductProperty extends Model
{
    protected $table = 'product_property';
}
