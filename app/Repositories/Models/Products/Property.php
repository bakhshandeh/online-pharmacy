<?php

namespace App\Repositories\Models\Products;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';
    protected $fillable = ['name',];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('value');
    }

    public function types()
    {
        return $this->belongsToMany(Type::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
