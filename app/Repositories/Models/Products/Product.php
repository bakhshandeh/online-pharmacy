<?php

namespace App\Repositories\Models\Products;


use App\Repositories\Models\User;
use App\Services\Accounting\Traits\HasOrders;
use App\Services\Prescriptions\Traits\HasPrescriptions;
use App\Services\Products\Traits\HasCategories;
use App\Services\Products\Traits\HasManufacturers;
use App\Services\Products\Traits\HasProductFiles;
use App\Services\Products\Traits\HasProperties;
use App\Services\Products\Traits\HasSellers;
use App\Services\Products\Traits\HasTypes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasProperties, HasTypes, HasCategories;
    use HasManufacturers, HasProductFiles, HasSellers;
    use HasOrders;
    use HasPrescriptions;


    protected $table = 'products';
    protected $fillable = ['id', 'user_id', 'edit_user_id', 'name', 'persian_name', 'generic_name', 'manufacturer', 'amount', 'description',
        'sum_number', 'score', 'sell_with_prescription'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function editUser()
    {
        return $this->belongsTo(User::class, 'edit_user_id');
    }

//    public function productSellers()
//    {
//
//    }

    public function hasSumNumber(int $quantity)
    {
        return $this->sum_number >= $quantity;
    }

    public function hasNumberSeller(int $quantity, $seller_id)
    {
        return ProductSeller::all()->where('product_id', $this->id)->where('seller_id', $seller_id)->first()->number >= $quantity;
    }
    public function decrementSumNumber(int $count)
    {
        return $this->decrement('sum_number',$count);
    }
}
