<?php

namespace App\Repositories\Models\Products\Sellers;

use App\Services\Uploader\StorageManager;
use Illuminate\Database\Eloquent\Model;

class Seller_File extends Model
{
    protected $table = 'seller_files';

//'time'->comment('based on seconds');
//'type'->comment('video,image,,pdf,archive');

    protected $fillable = ['name', 'size', 'time', 'type', 'is_private',];

    public function isMedia()
    {
        return $this->type == 'video';
    }

    public function absolutePath()
    {
        return resolve(StorageManager::class)->getAbsolutePathOf($this->name, $this->type, $this->is_private);
    }

    public function download()
    {
        return resolve(StorageManager::class)->getFile($this->name, $this->type, $this->is_private);
    }

    public function delete()
    {
        resolve(StorageManager::class)->deletefile($this->name, $this->type, $this->is_private);
        parent::delete();
    }

    public function sellers()
    {
        return $this->belongsToMany(Seller::class,'file_seller','file_id', 'seller_id');
    }
}
