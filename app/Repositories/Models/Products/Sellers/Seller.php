<?php

namespace App\Repositories\Models\Products\Sellers;

use App\Repositories\Models\ProvincesCities\City;
use App\Repositories\Models\ProvincesCities\Province;
use App\Repositories\Models\ProvincesCities\Quarter;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table = 'sellers';
    protected $fillable = ['name', 'persian_name',];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('number','seller_price','seller_purchase_price', 'profit');
    }


    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function quarter()
    {
        return $this->belongsTo(Quarter::class);
    }
}
