<?php

namespace App\Repositories\Models\Products;

use App\Services\Products\Traits\HasProperties;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasProperties;

    protected $table = 'types';
    protected $fillable = ['name',];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

}
