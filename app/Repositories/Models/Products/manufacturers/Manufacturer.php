<?php

namespace App\Repositories\Models\Products\manufacturers;

use App\Repositories\Models\ProvincesCities\City;
use App\Repositories\Models\ProvincesCities\Province;
use App\Repositories\Models\ProvincesCities\Quarter;
use App\Services\Products\Traits\HasProducts;
use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
    use HasProducts;

    protected $table = 'manufacturers';
    protected $fillable = ['name', 'persian_name',];

//    public function products()
//    {
//        return $this->belongsToMany(Product::class);
//    }


    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function quarter()
    {
        return $this->belongsTo(Quarter::class);
    }
}
