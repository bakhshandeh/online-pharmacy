<?php

namespace App\Repositories\Models\products\manufacturers;

use App\Services\Uploader\StorageManager;
use Illuminate\Database\Eloquent\Model;

class Manufacturer_File extends Model
{
    protected $table = 'manufacturer_files';

//'time'->comment('based on seconds');
//'type'->comment('video,image,,pdf,archive');

    protected $fillable = ['name', 'size', 'time', 'type', 'is_private',];

    public function isMedia()
    {
        return $this->type == 'video';
    }

    public function absolutePath()
    {
        return resolve(StorageManager::class)->getAbsolutePathOf($this->name, $this->type, $this->is_private);
    }

    public function download()
    {
        return resolve(StorageManager::class)->getFile($this->name, $this->type, $this->is_private);
    }

    public function delete()
    {
        resolve(StorageManager::class)->deletefile($this->name, $this->type, $this->is_private);
        parent::delete();
    }

    public function manufacturers()
    {
        return $this->belongsToMany(Manufacturer::class,'file_manufacturer','file_id', 'manufacturer_id');
    }
}
