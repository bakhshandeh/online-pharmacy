<?php

namespace App\Repositories\Models\Products;

use Illuminate\Database\Eloquent\Model;

class FileProduct extends Model
{
    protected $table = 'file_product';
}
