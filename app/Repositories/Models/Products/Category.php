<?php

namespace App\Repositories\Models\Products;

use App\Services\Products\Traits\HasProducts;
use App\Services\Products\Traits\HasProperties;
use App\Services\Products\Traits\HasTypes;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasProperties,HasTypes;
    use HasProducts;


    protected $table = 'categories';
    protected $fillable = ['name',];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
