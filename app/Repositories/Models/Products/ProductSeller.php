<?php

namespace App\Repositories\Models\Products;

use App\Services\Products\Traits\HasProducts;
use Illuminate\Database\Eloquent\Model;

class ProductSeller extends Model
{
    protected $table = 'product_seller';
    protected $fillable = ['number','seller_price','seller_purchase_price', 'profit'];

    public function decrementNumber(int $count)
    {
        return $this->decrement('number');
    }
}
