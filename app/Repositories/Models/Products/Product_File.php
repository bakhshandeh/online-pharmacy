<?php

namespace App\Repositories\Models\Products;

use App\Services\Uploader\StorageManager;
use Illuminate\Database\Eloquent\Model;

class Product_File extends Model
{

    protected $table = 'product_files';

//'time'->comment('based on seconds');
//'type'->comment('video,image,,pdf,archive');

    protected $fillable = ['name', 'size', 'time', 'type', 'is_private',];

    public function isMedia()
    {
        return $this->type == 'video';
    }

    public function absolutePath()
    {
        return resolve(StorageManager::class)->getAbsolutePathOf($this->name, $this->type, $this->is_private);
    }

    public function download()
    {
        return resolve(StorageManager::class)->getFile($this->name, $this->type, $this->is_private);
    }

    public function delete()
    {
        resolve(StorageManager::class)->deletefile($this->name, $this->type, $this->is_private);
        parent::delete();
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'file_product','file_id', 'product_id');
    }
}
