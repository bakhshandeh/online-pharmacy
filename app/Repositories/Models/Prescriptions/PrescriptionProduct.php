<?php

namespace App\Repositories\Models\Prescriptions;

use Illuminate\Database\Eloquent\Model;

class PrescriptionProduct extends Model
{
    protected $table = 'prescription_product';
    protected $fillable = [
        'id', 'prescription_id', 'product_id', 'seller_id', 'quantity','price'
        ];
}
