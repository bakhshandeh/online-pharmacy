<?php


namespace App\Repositories\Models\Prescriptions;


use Illuminate\Database\Eloquent\Model;

class FilePrescription extends Model
{
    protected $table = 'file_prescription';
}
