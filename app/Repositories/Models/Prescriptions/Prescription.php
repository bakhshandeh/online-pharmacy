<?php

namespace App\Repositories\Models\Prescriptions;

use App\Repositories\Models\Products\Product;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Repositories\Models\User;
use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    /**
     * @var string
     */
    protected $table = 'prescriptions';

    /**
     * @var string[]
     */
    protected $fillable = ['customer_id', 'employee_id', 'doctor_name', 'sick_name',
        'insurance_type', 'insurance_validity_date', 'date', 'description_customer', 'description_employee',
        'picture', 'send_customer'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'insurance_validity_date' => 'datetime',
        'date' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class,'prescription_product')->withPivot(['seller_id','quantity','price','id']);
    }

    public function files()
    {
        return $this->belongsToMany(Prescription_File::class, 'file_prescription', 'prescription_id', 'file_id');

    }

    public function customer()
    {
        return $this->belongsTo(User::class, 'customer_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'id');
    }
}
