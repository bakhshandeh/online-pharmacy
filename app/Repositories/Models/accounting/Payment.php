<?php

namespace App\Repositories\Models\accounting;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = ['order_id', 'method', 'geteway', 'ref_num', 'amount', 'status',];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    protected $attributes = [
        'status' => 0
    ];

    public function isOnline()
    {
        return $this->method == 'online';
    }

}
