<?php

namespace App\Repositories\Models\accounting;

use App\Repositories\Models\Products\Product;
use App\Repositories\Models\User;
use App\Services\Products\Traits\HasProducts;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasProducts;

    protected $table = 'orders';
    protected $fillable = ['customer_id', 'employee_id', 'prescription_id', 'code', 'amount',];

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function customer()
    {
        return $this->belongsTo(User::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity');
    }
}
