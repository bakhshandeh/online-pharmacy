<?php

namespace App\Repositories\Models\ProvincesCities;

use App\Repositories\Models\Products\manufacturers\Manufacturer;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Repositories\Models\Users\Address_User;
use Illuminate\Database\Eloquent\Model;

class Quarter extends Model
{
    protected $fillable = [
        'province_id', 'city_id', 'name', 'amar_code',
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function manufacturers()
    {
        return $this->hasMany(Manufacturer::class);
    }

    public function sellers()
    {
        return $this->hasMany(Seller::class);
    }

    public function addressUsers()
    {
        return $this->hasMany(Address_User::class);
    }

}
