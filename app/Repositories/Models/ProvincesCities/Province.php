<?php

namespace App\Repositories\Models\ProvincesCities;

use App\Repositories\Models\Products\manufacturers\Manufacturer;
use App\Repositories\Models\Products\Sellers\Seller;
use App\Repositories\Models\Users\Address_User;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'name', 'amar_code',
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }

    public function quarters()
    {
        return $this->hasMany(Quarter::class);
    }

    public function manufacturers()
    {
        return $this->hasMany(Manufacturer::class);
    }

    public function sellers()
    {
        return $this->hasMany(Seller::class);
    }

    public function addressUsers()
    {
        return $this->hasMany(Address_User::class);
    }
}
