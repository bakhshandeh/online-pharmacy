<?php

namespace App\Repositories\Models\Users;

use App\Repositories\Models\ProvincesCities\City;
use App\Repositories\Models\ProvincesCities\Province;
use App\Repositories\Models\ProvincesCities\Quarter;
use Illuminate\Database\Eloquent\Model;

class Address_User extends Model
{
    protected $table = 'address_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'province', 'city', 'quarter', 'street_alley', 'postal_code', 'no', 'floor',
        'unit', 'recipient_first_name', 'recipient_last_name', 'recipient_national_code',
        'recipient_phone_number',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_number_verified_at' => 'datetime',
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function quarter()
    {
        return $this->belongsTo(Quarter::class);
    }
}
