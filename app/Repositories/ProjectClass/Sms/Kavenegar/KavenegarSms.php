<?php


namespace App\Repositories\ProjectClass\Sms\Kavenegar;


use App\Repositories\Models\User;
use App\Repositories\ProjectClass\Sms\Sms;
use Kavenegar;

class KavenegarSms implements Sms
{
    public function sendmessage($number, string $_message)
    {
        try{
            $sender = "10004346";
            $message = $_message;
            $receptor = $number;
            $result = Kavenegar::Send($sender,$receptor,$message);
            if($result){
                foreach($result as $r){
                    echo "messageid = $r->messageid";
                    echo "message = $r->message";
                    echo "status = $r->status";
                    echo "statustext = $r->statustext";
                    echo "sender = $r->sender";
                    echo "receptor = $r->receptor";
                    echo "date = $r->date";
                    echo "cost = $r->cost";
                }
            }
        }
        catch(\Kavenegar\Exceptions\ApiException $e){
            // در صورتی که خروجی وب سرویس 200 نباشد این خطا رخ می دهد
            echo $e->errorMessage();
        }
        catch(\Kavenegar\Exceptions\HttpException $e){
            // در زمانی که مشکلی در برقرای ارتباط با وب سرویس وجود داشته باشد این خطا رخ می دهد
            echo $e->errorMessage();
        }
    }
}
