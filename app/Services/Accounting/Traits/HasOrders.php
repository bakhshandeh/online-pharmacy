<?php


namespace App\Services\Accounting\Traits;


use App\Repositories\Models\accounting\Order;
use Illuminate\Support\Arr;

trait HasOrders
{
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }


//    public function giveOrdersTo(... $orders)
//    {
//        $orders = $this->getAllOrders($orders);
//
//        if ($orders->isEmpty()) return $this;
//
//        $this->orders()->syncWithoutDetaching($orders);
//
//        return $this;
//    }
//
//    public function withDrawOrders(... $orders)
//    {
//        $orders = $this->getAllOrders($orders);
//        $this->orders()->detach($orders);
//
//        return $this;
//    }
//
//    public function refreshOrders(... $orders)
//    {
//        $orders = $this->getAllOrders($orders);
//        $this->orders()->sync($orders);
//
//        return $this;
//    }
//
//    public function hasOrder(string $order)
//    {
//        return $this->orders->contains('name', $order);
//    }
//
//    protected function getAllOrders(array $orders)
//    {
//        return Order::whereIn('name',Arr::flatten($orders))->get();
//    }
}
