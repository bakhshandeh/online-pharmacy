<?php


namespace App\Services\Uploader;


use App\Exceptions\FileHasExistsException;
use App\Repositories\Models\Products\Product_File;
use App\Repositories\Models\Prescriptions\Prescription_File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class Uploader
{
    private $request;
    private $storageManager;
    private $file;
    private $ffmpeg;

//    public function __construct(Request $request, StorageManager $storageManager, FFMpegService $ffmpeg)
    public function __construct( StorageManager $storageManager, FFMpegService $ffmpeg)
    {
//        $this->request = $request;
        $this->storageManager = $storageManager;
//        $this->file = $request->file;
        $this->ffmpeg = $ffmpeg;
    }

//    public function upload()
//    {
//        if ($this->isFileExists()) throw new FileHasExistsException('File has already uploaded');
//        $this->putFileInStorage();
//        $this->saveIntoDatabase();
//
//    }
    public function upload(Request $request)
    {
        $this->request = $request;

        foreach ($request->file() ['file'] as $file)
        {
            $this->file = $file;
            if ($this->isFileExists()) throw new FileHasExistsException('File has already uploaded');
            $this->putFileInStorage();
            $this->saveIntoDatabase();
        }
    }

    public function uploadFor(Request $request, Model $model, $modelTableFile)
    {
        $this->request = $request;

        if (is_array($request->file() ['file']))
        {
            foreach ($request->file() ['file'] as $file)
            {
                $this->file = $file;
                if ($this->isFileExists()) throw new FileHasExistsException('File has already uploaded');
                $this->putFileInStorage();
                $_file = $this->saveIntoDatabase($modelTableFile);
                $model->files()->attach($_file->id);
            }
        }else
        {
            $this->file = $request->file() ['file'];
            if ($this->isFileExists()) throw new FileHasExistsException('File has already uploaded');
            $this->putFileInStorage();
            $_file = $this->saveIntoDatabase($modelTableFile);
            $model->files()->attach($_file->id);
        }

    }

    private function saveIntoDatabase($modelTableFile)
    {

        $file = new $modelTableFile([
             'name' => Carbon::now('Asia/Tehran')->toDateTimeString(). "_" .$this->file->getClientOriginalName(),
             'size' => $this->file->getSize(),
             'type' => $this->getType(),
             'is_private' => $this->isPrivate()
         ]);
         $file->time = $this->getTime($file);
         $file->save();

        return $modelTableFile::all()->last();

    }

    private function getTime($file)
    {
        if (!$file->isMedia()) return null;
        return (int) $this->ffmpeg->durationOf($file->absolutePath());
    }

    public function putFileInStorage()
    {
        $method = $this->isPrivate() ? 'putFileAsPrivate' : 'putFileAsPublic' ;
        $this->storageManager->$method(Carbon::now('Asia/Tehran')->toDateTimeString(). "_" .$this->file->getClientOriginalName(), $this->file, $this->getType());
    }

    private function isPrivate()
    {
        return $this->request->has('is_private');
    }

    private function isFileExists()
    {
        return $this->storageManager->isFileExists($this->file->getClientOriginalName(), $this->getType(), $this->isPrivate());
    }

    public function getType()
    {
        return [
            'video/avi' => 'video',
            'video/mpeg' => 'video',
            'video/mp4' => 'video',
            'image/jpeg' => 'image',
            'image/jpg' => 'image',
            'application/pdf' => 'pdf',
            'application/zip' => 'zip',
        ][$this->file->getClientMimeType()];
    }
}
