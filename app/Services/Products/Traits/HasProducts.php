<?php


namespace App\Services\Products\Traits;


use App\Repositories\Models\Products\Product;
use Illuminate\Support\Arr;

trait HasProducts
{
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }


//    public function giveProductsTo(... $products)
//    {
//        $products = $this->getAllProducts($products);
//
//        if ($products->isEmpty()) return $this;
//
//        $this->products()->syncWithoutDetaching($products);
//
//        return $this;
//    }
//
//    public function withDrawProducts(... $products)
//    {
//        $products = $this->getAllProducts($products);
//        $this->products()->detach($products);
//
//        return $this;
//    }
//
//    public function refreshProducts(... $products)
//    {
//        $products = $this->getAllProducts($products);
//        $this->products()->sync($products);
//
//        return $this;
//    }
//
//    public function hasProduct(string $product)
//    {
//        return $this->products->contains('id', $product);
//    }
//
//    protected function getAllProducts(array $products)
//    {
//        return Product::whereIn('id',Arr::flatten($products))->get();
//    }
}
