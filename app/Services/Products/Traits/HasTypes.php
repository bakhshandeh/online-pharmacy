<?php


namespace App\Services\Products\Traits;


use App\Repositories\Models\Products\Type;
use Illuminate\Support\Arr;

trait HasTypes
{
    public function types()
    {
        return $this->belongsToMany(Type::class);
    }


    public function giveTypesTo(... $types)
    {
        $types = $this->getAllTypes($types);

        if ($types->isEmpty()) return $this;

        $this->types()->syncWithoutDetaching($types);

        return $this;
    }

    public function withDrawTypes(... $types)
    {
        $types = $this->getAllTypes($types);
        $this->types()->detach($types);

        return $this;
    }

    public function refreshTypes(... $types)
    {
        $types = $this->getAllTypes($types);
        $this->types()->sync($types);

        return $this;
    }

    public function hasType(string $type)
    {
        return $this->types->contains('id', $type);
    }

    protected function getAllTypes(array $types)
    {
        return Type::whereIn('id',Arr::flatten($types))->get();
    }
}
