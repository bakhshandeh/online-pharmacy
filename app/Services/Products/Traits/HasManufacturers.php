<?php


namespace App\Services\Products\Traits;


use App\Repositories\Models\Products\manufacturers\Manufacturer;
use Illuminate\Support\Arr;

trait HasManufacturers
{
    public function manufacturers()
    {
        return $this->belongsToMany(Manufacturer::class);
    }


    public function giveManufacturersTo(... $manufacturers)
    {
        $manufacturers = $this->getAllManufacturers($manufacturers);

        if ($manufacturers->isEmpty()) return $this;

        $this->manufacturers()->syncWithoutDetaching($manufacturers);

        return $this;
    }

    public function withDrawManufacturers(... $manufacturers)
    {
        $manufacturers = $this->getAllManufacturers($manufacturers);
        $this->manufacturers()->detach($manufacturers);

        return $this;
    }

    public function refreshManufacturers(... $manufacturers)
    {
        $manufacturers = $this->getAllManufacturers($manufacturers);
        $this->manufacturers()->sync($manufacturers);

        return $this;
    }

    public function hasManufacturer(string $manufacturer)
    {
        return $this->manufacturers->contains('id', $manufacturer);
    }

    protected function getAllManufacturers(array $manufacturers)
    {
        return Manufacturer::whereIn('id',Arr::flatten($manufacturers))->get();
    }
}
