<?php


namespace App\Services\Products\Traits;


use App\Repositories\Models\Products\Sellers\Seller;
use Illuminate\Support\Arr;

trait HasSellers
{
    public function sellers()
    {
        return $this->belongsToMany(Seller::class)->withPivot('number','seller_price','seller_purchase_price', 'profit');

    }


//    public function giveSellersTo(... $sellers)
//    {
//        $sellers = $this->getAllSellers($sellers);
//
//        if ($sellers->isEmpty()) return $this;
//
//        $this->sellers()->syncWithoutDetaching($sellers);
//
//        return $this;
//    }
//
//    public function withDrawSellers(... $sellers)
//    {
//        $sellers = $this->getAllSellers($sellers);
//        $this->sellers()->detach($sellers);
//
//        return $this;
//    }
//
//    public function refreshSellers(... $sellers)
//    {
//        $sellers = $this->getAllSellers($sellers);
//        $this->sellers()->sync($sellers);
//
//        return $this;
//    }
//
//    public function hasSellers(string $sellers)
//    {
//        return $this->sellers->contains('id', $sellers);
//    }
//
//    protected function getAllSellers(array $sellers)
//    {
//        return Seller::whereIn('id',Arr::flatten($sellers))->get();
//    }
}
