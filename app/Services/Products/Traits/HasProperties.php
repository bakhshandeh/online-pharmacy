<?php


namespace App\Services\Products\Traits;



use App\Repositories\Models\Products\Property;
use Illuminate\Support\Arr;

trait HasProperties
{
    public function properties()
    {
        return $this->belongsToMany(Property::class)->withPivot('value');

    }


    public function givePropertiesTo(... $properties)
    {
        $properties = $this->getAllProperties($properties);

        if ($properties->isEmpty()) return $this;

        $this->properties()->syncWithoutDetaching($properties);

        return $this;
    }

    public function withDrawProperties(... $properties)
    {
        $properties = $this->getAllProperties($properties);
        $this->properties()->detach($properties);

        return $this;
    }

    public function refreshProperties(... $properties)
    {
        $properties = $this->getAllProperties($properties);
        $this->properties()->sync($properties);

        return $this;
    }

//    public function hasProperty(string $property)
//    {
//        return $this->properties->contains('name', $property);
//    }

    public function hasPermission(Property $property)
    {
        return $this->hasPropertyThroughType($property) || $this->properties->contains('id', $property);
    }

    protected function hasPropertyThroughType(Property $property)
    {
        foreach ($property->types as $type)
        {
            if ($this->types->contains($type)) return true;
        }
        return false;
    }

    protected function getAllProperties(array $properties)
    {
        return Property::whereIn('id',Arr::flatten($properties))->get();
    }
}
