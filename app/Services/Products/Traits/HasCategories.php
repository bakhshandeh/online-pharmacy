<?php


namespace App\Services\Products\Traits;


use App\Repositories\Models\Products\Category;
use Illuminate\Support\Arr;

trait HasCategories
{
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }


    public function giveCategoriesTo(... $categories)
    {
        $categories = $this->getAllCategories($categories);

        if ($categories->isEmpty()) return $this;

        $this->categories()->syncWithoutDetaching($categories);

        return $this;
    }

    public function withDrawCategories(... $categories)
    {
        $categories = $this->getAllCategories($categories);
        $this->categories()->detach($categories);

        return $this;
    }

    public function refreshCategories(... $categories)
    {
        $categories = $this->getAllCategories($categories);
        $this->categories()->sync($categories);

        return $this;
    }

    public function hasCategory(string $category)
    {
        return $this->categories->contains('id', $category);
    }

    protected function getAllCategories(array $categories)
    {
        return Category::whereIn('id',Arr::flatten($categories))->get();
    }
}
