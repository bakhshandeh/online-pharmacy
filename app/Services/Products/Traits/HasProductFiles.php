<?php


namespace App\Services\Products\Traits;


use App\Repositories\Models\Products\Product_File;
use Illuminate\Support\Arr;

trait HasProductFiles
{
    public function files()
    {
        return $this->belongsToMany(Product_File::class, 'file_product', 'product_id', 'file_id');

//        return $this->belongsToMany(Product_File::class);
    }


    public function giveFilesTo(... $files)
    {
        $files = $this->getAllFiles($files);

        if ($files->isEmpty()) return $this;

        $this->files()->syncWithoutDetaching($files);

        return $this;
    }
//
//    public function withDrawFiles(... $files)
//    {
//        $files = $this->getAllFiles($files);
//        $this->files()->detach($files);
//
//        return $this;
//    }
//
//    public function refreshFiles(... $files)
//    {
//        $files = $this->getAllFiles($files);
//        $this->files()->sync($files);
//
//        return $this;
//    }
//
    public function hasFile(string $file)
    {
        return $this->files->contains('id', $file);
    }

    protected function getAllFiles(array $files)
    {
        return Product_File::whereIn('id',Arr::flatten($files))->get();
    }
}
