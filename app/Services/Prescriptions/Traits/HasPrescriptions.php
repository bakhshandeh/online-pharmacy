<?php


namespace App\Services\Prescriptions\Traits;


use App\Repositories\Models\Prescriptions\Prescription;
use Illuminate\Support\Arr;

trait HasPrescriptions
{
    public function prescriptions()
    {
        return $this->belongsToMany(Prescription::class,'prescription_product')->withPivot(['seller_id','quantity','price','id']);
    }


//    public function givePrescriptionsTo(... $prescriptions)
//    {
//        $prescriptions = $this->getAllPrescriptions($prescriptions);
//
//        if ($prescriptions->isEmpty()) return $this;
//
//        $this->prescriptions()->syncWithoutDetaching($prescriptions);
//
//        return $this;
//    }
//
//    public function withDrawPrescriptions(... $prescriptions)
//    {
//        $prescriptions = $this->getAllPrescriptions($prescriptions);
//        $this->prescriptions()->detach($prescriptions);
//
//        return $this;
//    }
//
//    public function refreshPrescriptions(... $prescriptions)
//    {
//        $prescriptions = $this->getAllPrescriptions($prescriptions);
//        $this->prescriptions()->sync($prescriptions);
//
//        return $this;
//    }
//
//    public function hasPrescription(string $role)
//    {
//        return $this->prescriptions->contains('name', $role);
//    }
//
//    protected function getAllPrescriptions(array $prescriptions)
//    {
//        return Prescription::whereIn('name',Arr::flatten($prescriptions))->get();
//    }
}
