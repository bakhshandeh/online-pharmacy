<?php


namespace App\Services\Notification\Constants;



use App\Mail\User\Auth\ForgetPassword;
use App\Mail\User\Auth\UserRegister;

class MailTypes
{
    const USER_REGISTERED = 1;
    const FORGET_PASSWORD = 2;

    public static function toString()
    {
        return [
            self::USER_REGISTERED => 'ثبت نام کاربر',
            self::FORGET_PASSWORD => 'فراموش کردن رمز عبور',
        ];
    }

    public static function toMail($type)
    {
        try {
            $types = [
                self::USER_REGISTERED => UserRegister::class,
                self::FORGET_PASSWORD => ForgetPassword::class,
            ];

            return $types[$type];
        }catch (\Throwable $th){
            throw new \InvalidArgumentException('Mailable class dose not exist');
        }
    }
}
