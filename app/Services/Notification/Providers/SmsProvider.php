<?php


namespace App\Services\Notification\Providers;


use App\Repositories\Models\User;
use App\Repositories\ProjectClass\Sms\Kavenegar\KavenegarSms;
use App\Services\Notification\Exceptions\UserDoesNotHavePhoneNumber;
use App\Services\Notification\Providers\Contracts\Provider;
//use GuzzleHttp\Client;

class SmsProvider implements Provider
{
    private $user;
    private $message;

    public function __construct(User $user,string $message)
    {
        $this->user = $user;
        $this->message = $message;
    }

    public function send()
    {
        $this->havePhoneNumber();

        $number = $this->user->phone_number;
        $KavenegarSms = new KavenegarSms();
        $KavenegarSms->sendmessage($number, $this->message);


//        $client = new Client();
//        $response = $client->post(config('services.sms.uri'), $this->prepareDataForSms($this->user, $this->message));
//        return $response->getBody();
    }

//    public function prepareDataForSms(User $user,string $message)
//    {
//        $number = $user->phone_number;
//
//        $data = array_merge(
//            config('services.sms.auth'),
//            [
//                'op' => 'send',
//                'message' => $message,
//                'to' => [$number],
//            ]
//        );
//
//        $options = [
//            'json' => $data
//        ];
//
//        return $options;
//    }

    /**
     * @return string
     */
    public function havePhoneNumber()
    {
        if (is_null($this->user->phone_number) || empty($this->user->phone_number))
        {
            throw new UserDoesNotHavePhoneNumber();
        }

    }
}
