<?php


namespace App\Services\Notification;


use App\Services\Notification\Providers\Contracts\Provider;

/**
 * Class Notification
 * @package App\Services\Notification
 * @method sendMail(App\Repositories\Models\User $user, Illuminate\Mail\Mailable $mailable)
 * @method sendSms(App\Repositories\Models\User $user,string $message)
 */
class Notification
{
//    public function sendMail(User $user,Mailable $mailable)
//    {
//        $mailProvider = new MailProvider();
//        return $mailProvider->send($user, $mailable);
//    }
//
//    public function sendSMS(User $user,string $message)
//    {
//        $smsProvider = new SmsProvider();
//        return $smsProvider->send($user, $message);
//    }

    public function __call($method, $arguments)
    {
        $providerPath = __NAMESPACE__ . '\Providers\\' . substr($method, 4) . 'Provider';
        if (!class_exists($providerPath))
        {
            throw new \Exception("Class dose not exist");
        }

        $providerInstance = new $providerPath(...$arguments);
        if (!is_subclass_of($providerInstance, Provider::class))
        {
            throw new \Exception("Class must implements \App\Services\Notification\Providers\Contracts\Provider");
        }

        return $providerInstance->send();
    }
}
