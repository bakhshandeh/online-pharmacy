<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $usersNamespace = 'App\Http\Controllers\Manegment\Users';
    protected $usersAdminNamespace = 'App\Http\Controllers\Manegment\UsersAdmin';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';
    public const DASHBOARDUSER = 'dashboard/';

    protected $prefixRoutusersAdmin = '__@M*uA&__';
    public const DASHBOARDUSERADMIN = '__@M*uA&__/dashboard';


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));

        Route::middleware(['web','auth'])
            ->namespace($this->usersNamespace)
            ->group(base_path('routes/management/users.php'));

        Route::middleware(['web','auth'])
            ->prefix($this->prefixRoutusersAdmin)
            ->namespace($this->usersAdminNamespace)
            ->group(base_path('routes/management/usersAdmin.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
